<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function clear_cache() {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function get_type_name_by_id($type, $type_id = '', $field = 'name') {
        return $this->db->get_where($type, array($type . '_id' => $type_id))->row()->$field;
    }
//////////ACCOUNTS/////////////
    function money($number) {
        if (strlen($number) <= 0)
            return '';

        return number_format((float)$number, 2, '.', '');
    }
    function get_paid_amount($a1_id){
        $total_paid = 0;
        $paid_amt = $this->db->get_where('a8', array( 'a1_id' => $a1_id ))->result_array();

        foreach($paid_amt as $amt):
            $total_paid += floatval($amt['amount']); 
        endforeach;
        return $total_paid;

    }
    function getfullmonthname($param1) {
        return date('F', mktime(0, 0, 0, $param1, 1, date('Y')));
    }
    function getmethodname($param1) {
        if ($param1 == 1)
            return get_phrase('cash');
        if ($param1 == 2)
            return get_phrase('check');
        if ($param1 == 3)
            return get_phrase('card');
        if ($param1 == 'paypal')
            return get_phrase('paypal');
        return '';
    }
    function mysqldate($param1) {
        if (strlen(($param1) > 0)) {
            $_date = explode("/", $param1);
            return date('Y-m-d', strtotime($_date[2] . '-' . $_date[0] . '-' . $_date[1]));
        } else {
            return '';
        }
    }
    function get_table($acc_id, $type) {
        $_tid = $acc_id;
        $_tt = $type;
        if ($_tt == 'a2') {
            $_tid = $this->db->get_where('a2', array('a2_id' => $_tid))->result_array()[0]['a3_id'];
            $_tt = 'a3';
        }
        if ($_tt == 'a3') {
            $_tid = $this->db->get_where('a3', array('a3_id' => $_tid))->result_array()[0]['a4_id'];
        }
        if (intval($_tid) == 1 || intval($_tid) == 4)
            return 'a1';

        if (intval($_tid) == 2 || intval($_tid) == 3)
            return 'a2';

        return '';
    }
    function get_total_due_from_database($course_id="",$batch_id="",$student_id=""){
     
        $sql = "SELECT SUM(t.amount) as due_amount FROM (SELECT  a1.course_id, a1.batch_id, a1.a1_id , a6.a2_id, a6.amount FROM a1 RIGHT JOIN a6 ON a1.a1_id = a6.a1_id WHERE a1.course_id = ".$course_id." AND a1.batch_id = ".$batch_id.") as t WHERE t.a2_id = 1200";
        $q=$this->db->query($sql);
        $row=$q->row();
        $total=$row->due_amount; 
        
        return $total;
    }
    function get_total_fee_from_database($course_id="",$batch_id="",$student_id=""){

        $sql = "SELECT SUM(t.amount) as fee_total FROM (SELECT  a1.course_id, a1.batch_id, a1.a1_id , a6.a2_id, a6.amount FROM a1 RIGHT JOIN a6 ON a1.a1_id = a6.a1_id WHERE a1.course_id = ".$course_id." AND a1.batch_id = ".$batch_id.") as t WHERE t.a2_id = 1303";
        $q=$this->db->query($sql);
        // echo $this->db->last_query
        $row=$q->row();
        $total=$row->fee_total; 
        
        return $total;
    }
    function get_total_paid_from_database($course_id="",$batch_id="",$student_id=""){
        $sql = "SELECT SUM(t.amount) as paid_amount FROM (SELECT  a1.course_id, a1.batch_id, a1.a1_id , a6.a2_id, a6.amount FROM a1 RIGHT JOIN a6 ON a1.a1_id = a6.a1_id WHERE a1.course_id = ".$course_id." AND a1.batch_id = ".$batch_id.") as t WHERE t.a2_id = 1065";
        $q=$this->db->query($sql);
        $row=$q->row();
        $total=$row->paid_amount; 
        return $total;
    }
    function get_individual_paid_student($a1_id){
       $sql = "SELECT SUM(amount) as paid_amount FROM a8 where a1_id = ".$a1_id."";
       $q=$this->db->query($sql);
       $row=$q->row();
       $total=$row->paid_amount;    
       return $total;
   }

        ////////STUDENT/////////////
   function get_students($course_id) {
    $query = $this->db->get_where('student', array('course_id' => $course_id));
    return $query->result_array();
}

function get_student_info($student_id) {
    $query = $this->db->get_where('student', array('student_id' => $student_id));
    return $query->result_array();
}
function get_student_name($student_id) {
    $query = $this->db->get_where('student', array('student_id' => $student_id));
    $res = $query->result_array();
    foreach ($res as $row)
        return $row['name'];
}
function __gettext($input) {
    if (!empty($input))
        if (strlen(trim($input)) > 0)
            return $input;

        return 'N/A';
    }

    function __getx($input_text, $font, $font_size, $container_width) {
        if (!empty($input_text) && !empty($font) && $font_size > 0 && $container_width > 0) {
            if (strlen(trim($input_text)) > 0) {
                $_size_name = imagettfbbox($font_size, 0, $font, $input_text);
                $_l = $_size_name[2];
                $_x = ($container_width - $_l) / 4;
                return $_x;
            }
        }
        return 0;
    }


    function __getnewx($input_text, $font, $font_size, $container_width) {
        if (!empty($input_text) && !empty($font) && $font_size > 0 && $container_width > 0) {
            if (strlen(trim($input_text)) > 0) {
                $container_center = $container_width / 2;                   
                $_size_name = imagettfbbox($font_size, 0, $font, $input_text);
                $text_center = $_size_name[2] / 2;
                        // echo $container_center.'-'.$text_center;

                        //$_l = $_size_name[2];
                $_x = $container_center - $text_center;
                        // echo '-'.$_x;
                        //  die();
                return $_x;
            }
        }
        return 0;
    }

    function __getlength($input_text, $font, $font_size) {
        if (!empty($input_text) && !empty($font) && $font_size > 0) {
            if (strlen(trim($input_text)) > 0) {
                $_size_name = imagettfbbox($font_size, 0, $font, $input_text);
                $_l = $_size_name[2];
                return $_l;
            }
        }
        return 0;
    }

    function __getcx($input_text, $font, $font_size, $ref_x) {
        if (!empty($input_text) && !empty($font) && $font_size > 0 && $ref_x > 0) {
            if (strlen(trim($input_text)) > 0) {
                $_size_name = imagettfbbox($font_size, 0, $font, $input_text);
                $_l = $_size_name[2];
                return $ref_x - $_l;
            }
        }
        return 0;
    }

    function generate_student_id_card($student_id) {
        $_validity_date = $this->db->get_where('settings' , array('type' => 'id_card_validity'))->row()->description;
        $_student = $this->db->get_where('student' , array('student_id' => $student_id))->row();
        $_prefix = $this->db->get_where('settings' , array('type' => 'student_id_card_prefix'))->row()->description;

    if (empty($_validity_date) || empty($_student)/* || empty($_prefix)*/) {
        return false;
        // echo "WTF";
        // die();
    }

    $_id_date = date('dmY', strtotime($_validity_date));
    $_textdate = date('d-M-Y', strtotime($_validity_date));
    $_birthdate = date('d-M-Y', strtotime($_student->birthday));
    $_image_url = base_url() . 'uploads/student_id_cards/' . $_id_date . $student_id . '.jpg';


    if (file_exists(base_url() .'uploads/student_id_cards/' . $_id_date . $student_id . '.jpg')) {
        return $_image_url;
    } else {
        $_card_back = imagecreatefromjpeg(base_url() ."uploads/STUDENT_ID_CARD_BACK.jpg");
        //  echo $_card_back;
        // die();
    

        if (file_exists(base_url() .'uploads/student_image/' . $student_id . '.jpg')) {
            $_student_image_file = base_url() .'uploads/student_image/' . $student_id . '.jpg';
        } else {
            $_student_image_file = base_url() .'uploads/user.jpg';
        }

        switch (exif_imagetype($_student_image_file)) {
            case 1:
            $_student_image = imagecreatefromgif($_student_image_file);
            break;
            case 2:
            $_student_image = imagecreatefromjpeg($_student_image_file);
            break;
            case 3:
            $_student_image = imagecreatefrompng($_student_image_file);
            break;
            case 15:
            $_student_image = imagecreatefromwbmp($_student_image_file);
            break;
            default:
            $_student_image = imagecreatefrompng('uploads/user.jpg');
            break;
        }

        $_width = imagesx($_student_image);
        $_height = imagesy($_student_image);
        $_card_width = imagesx($_card_back);
            //$_card_height = imagesy($_card_back);

        $_font = "assets/fonts/OpenSans-Bold.ttf";
        $_font1 = "assets/fonts/OpenSans-Regular.ttf";

        $_roll_lebel = 'Roll';
        $_course_lebel = 'Course';
        $id_label = 'ID:';
        $_birthdate_lebel = 'Birthdate';
        $_emergency_lebel = 'Emergency';
        $_blood_group_lebel = 'Blood Group';
        $_separator = ':';

        $_roll = $this->__gettext($_student->roll);
        $_course_name = $this->__gettext($this->db->get_where('course' , array('course_id' => $_student->course_id))->row()->name);
        $_teacher_id = $this->__gettext($this->db->get_where('batch' , array('batch_id' => $_student->batch_id))->row()->teacher_id);
        $_start_date = strtotime($this->__gettext($this->db->get_where('batch' , array('batch_id' => $_student->batch_id))->row()->start_date));
        $_end_date = strtotime($this->__gettext($this->db->get_where('batch' , array('batch_id' => $_student->batch_id))->row()->end_date));
        $total_hrs = $this->__gettext($this->db->get_where('course' , array('course_id' => $_student->course_id))->row()->total_hrs);
        $_teacher_id_f = $this->db->get_where('teacher' , array('teacher_id' => $_teacher_id))->row()->name;
        $_blood_group = $this->__gettext($_student->blood_group);

        $_nx = $this->__getnewx($_student->name, $_font, 35, $_card_width);

        $_mx = $this->__getx($_separator, $_font, 8, $_card_width);

        $_rlx = $this->__getcx($_roll_lebel, $_font, 8, $_mx);
        $_clx = $this->__getcx($_course_lebel, $_font, 8, $_mx);
        $_slx = $this->__getcx($_batch_lebel, $_font, 8, $_mx);
        $_bdlx = $this->__getcx($_birthdate_lebel, $_font, 8, $_mx);
        $_bglx = $this->__getcx($_blood_group_lebel, $_font, 8, $_mx);
        $_glx = $this->__getcx($_emergency_lebel, $_font, 8, $_mx);

        $_top = 655;
        $_ry = $_top;
        $_cy = $_top + 100;
        $_sy = $_top + 100;
        $_bdy = $_top + 100;
        $_bgy = $_top + 100;
        $_gy = $_top + 100;
        $_sep_space = 5;

        $_cname_pos = $this->__getnewx($_course_name, $_font, 35, $_card_width);
            // echo $_mx;
            // die();

            // imagecopyresampled($_card_back, $_student_image, 69, 103, 0, 0, 67, 65, $_width, $_height);

        imagefttext($_card_back, 35, 0, $_nx, 595, 2, $_font, strtoupper($_student->name));
        imagefttext($_card_back, 30, 0, 100, 100, 1, $_font, $id_label);


        imagefttext($_card_back, 35, 0, 681, 663, 1, $_font, $total_hrs);
        imagefttext($_card_back, 35, 0, $_cname_pos, 720, 1, $_font, strtoupper($_course_name));
        imagefttext($_card_back, 35, 0, 650, 790, 1, $_font1, date('d F Y',$_start_date).' - ');
        imagefttext($_card_back, 35, 0, 1090, 790, 1, $_font1, date('d F Y',$_end_date));
        imagefttext($_card_back, 30, 0, 200, 100, 1, $_font, $_student->student_id);
        imagefttext($_card_back, 20, 0, 1000, 1090, 1, $_font1, $_teacher_id_f);


        imagejpeg($_card_back, 'uploads/student_id_cards/' . $_id_date . $student_id . '.jpg', 99);
        imagedestroy($_card_back);

        return $_image_url;
    }
}

    /////////TEACHER/////////////
function get_teachers() {
    $query = $this->db->get('teacher');
    return $query->result_array();
}

function get_teacher_name($teacher_id) {
    $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
    $res = $query->result_array();
    foreach ($res as $row)
        return $row['name'];
}

function get_teacher_info($teacher_id) {
    $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
    return $query->result_array();
}

    //////////SUBJECT/////////////
function get_subjects() {
    $query = $this->db->get('subject');
    return $query->result_array();
}

function get_subject_info($subject_id) {
    $query = $this->db->get_where('subject', array('subject_id' => $subject_id));
    return $query->result_array();
}

function get_subjects_by_course($course_id) {
    $query = $this->db->get_where('subject', array('course_id' => $course_id));
    return $query->result_array();
}

function get_subject_name_by_id($subject_id) {
    $query = $this->db->get_where('subject', array('subject_id' => $subject_id))->row();
    return $query->name;
}
    ///////////////////batchS////////////////////
function get_batchs_by_course($course_id) {
    $query = $this->db->get_where('batch', array('course_id' => $course_id));
    return $query->result_array();
}
    ////////////CLASS///////////
function get_course_name($course_id) {
    $query = $this->db->get_where('course', array('course_id' => $course_id));
    $res = $query->result_array();
    foreach ($res as $row)
        return $row['name'];
}

function get_course_name_numeric($course_id) {
    $query = $this->db->get_where('course', array('course_id' => $course_id));
    $res = $query->result_array();
    foreach ($res as $row)
        return $row['name_numeric'];
}

function get_courses() {
    $query = $this->db->get('course');
    return $query->result_array();
}

function get_course_info($course_id) {
    $query = $this->db->get_where('course', array('course_id' => $course_id));
    return $query->result_array();
}

    //////////EXAMS/////////////
function get_exams() {
    $query = $this->db->get('exam');
    return $query->result_array();
}

function get_exam_info($exam_id) {
    $query = $this->db->get_where('exam', array('exam_id' => $exam_id));
    return $query->result_array();
}

    ////////////////INCOME CATEGORY NAME//////////////////
function get_income_category_by_id($income_category_id){
  $query = $this->db->get_where('income_category', array('income_category_id' => $income_category_id));
  return $query->result_array();
}
    //////////GRADES/////////////
function get_grades() {
    $query = $this->db->get('grade');
    return $query->result_array();
}

function get_grade_info($grade_id) {
    $query = $this->db->get_where('grade', array('grade_id' => $grade_id));
    return $query->result_array();
}

function get_grade($mark_obtained,$full_mark) {
    if(!empty($full_mark)){
        $this->db->where('for_marks',$full_mark);
        $query = $this->db->get('grade');
        $grades = $query->result_array();
        foreach ($grades as $row) {
            if ($mark_obtained >= $row['mark_from'] && $mark_obtained <= $row['mark_upto'])
                return $row;
        }
    }
    else{
        $query = $this->db->get('grade');
        $grades = $query->result_array();
        foreach ($grades as $row) {
            if ($mark_obtained >= $row['mark_from'] && $mark_obtained <= $row['mark_upto'])
                return $row;
        }
    }
}

function create_log($data) {
    $data['timestamp'] = strtotime(date('Y-m-d') . ' ' . date('H:i:s'));
    $data['ip'] = $_SERVER["REMOTE_ADDR"];
    $location = new SimpleXMLElement(file_get_contents('http://freegeoip.net/xml/' . $_SERVER["REMOTE_ADDR"]));
    $data['location'] = $location->City . ' , ' . $location->CountryName;
    $this->db->insert('log', $data);
}

function get_system_settings() {
    $query = $this->db->get('settings');
    return $query->result_array();
}

    ////////BACKUP RESTORE/////////
function create_backup($type) {
    $this->load->dbutil();


    $options = array(
            'format' => 'txt', // gzip, zip, txt
            'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE, // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );


    if ($type == 'all') {
        $tables = array('');
        $file_name = 'system_backup';
    } else {
        $tables = array('tables' => array($type));
        $file_name = 'backup_' . $type;
    }

    $backup = & $this->dbutil->backup(array_merge($options, $tables));


    $this->load->helper('download');
    force_download($file_name . '.sql', $backup);
}

    /////////RESTORE TOTAL DB/ DB TABLE FROM UPLOADED BACKUP SQL FILE//////////
function restore_backup() {
    move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/backup.sql');
    $this->load->dbutil();


    $prefs = array(
        'filepath' => 'uploads/backup.sql',
        'delete_after_upload' => TRUE,
        'delimiter' => ';'
    );
    $restore = & $this->dbutil->restore($prefs);
    unlink($prefs['filepath']);
}

    /////////DELETE DATA FROM TABLES///////////////
function truncate($type) {
    if ($type == 'all') {
        $this->db->truncate('student');
        $this->db->truncate('mark');
        $this->db->truncate('teacher');
        $this->db->truncate('subject');
        $this->db->truncate('course');
        $this->db->truncate('exam');
        $this->db->truncate('grade');
    } else {
        $this->db->truncate($type);
    }
}

    ////////IMAGE URL//////////
function get_image_url($type = '', $id = '') {
    if (file_exists('uploads/' . $type . '_image/' . $id . '.jpg'))
        $image_url = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
    else
        $image_url = base_url() . 'uploads/user.jpg';

    return $image_url;
}
    ////////////////////////////ALBUM iMAGE URL///////////////////////
function get_album_url($type = '', $albumid ='' , $imageid ='' ){
    if (file_exists('uploads/' . $type . '_image/' . $albumid .'-'.$imageid.'.jpg'))
        $image_url = base_url() . 'uploads/' . $type . '_image/' .$albumid.'-'. $imageid. '.jpg';
    return $image_url;
}
//
//////////////////
    ////////STUDY MATERIAL//////////
function save_study_material_info()
{
    $data['timestamp']      = strtotime($this->input->post('timestamp'));
    $data['title'] 		= $this->input->post('title');
    $data['description']    = $this->input->post('description');
    $data['file_name'] 	= $_FILES["file_name"]["name"];
    $data['file_type'] 	= $this->input->post('file_type');
    $data['course_id'] 	= $this->input->post('course_id');

    $this->db->insert('document',$data);

    $document_id            = $this->db->insert_id();
    move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/document/" . $_FILES["file_name"]["name"]);
}

function select_study_material_info()
{
    $this->db->order_by("timestamp", "desc");
    return $this->db->get('document')->result_array(); 
}

function select_study_material_info_for_student()
{
    $student_id = $this->session->userdata('student_id');
    $course_id   = $this->db->get_where('student', array('student_id' => $student_id))->row()->course_id;
    $this->db->order_by("timestamp", "desc");
    return $this->db->get_where('document', array('course_id' => $course_id))->result_array();
}

function update_study_material_info($document_id)
{
    $data['timestamp']      = strtotime($this->input->post('timestamp'));
    $data['title'] 		= $this->input->post('title');
    $data['description']    = $this->input->post('description');
    $data['course_id'] 	= $this->input->post('course_id');

    $this->db->where('document_id',$document_id);
    $this->db->update('document',$data);
}

function delete_study_material_info($document_id)
{
    $this->db->where('document_id',$document_id);
    $this->db->delete('document');
}
     ////////FORM//////////
function save_form_info()
{
    $data['timestamp']      = strtotime($this->input->post('timestamp'));
    $data['file_name'] 	= $_FILES["file_name"]["name"];
    $data['file_type'] 	= $this->input->post('file_type');

    $this->db->insert('form',$data);

    $form_id            = $this->db->insert_id();
    move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/form/" . $_FILES["file_name"]["name"]);
}

function select_form_info()
{
    $this->db->order_by("timestamp", "desc");
    return $this->db->get('form')->result_array(); 
}


function update_form_info($form_id)
{
    $data['timestamp']      = strtotime($this->input->post('timestamp'));
    move_uploaded_file($_FILES['file_name']['tmp_name'], 'uploads/form/' . $form_id.'.pdf');
    $this->db->where('form_id',$form_id);
    $this->db->update('form',$data);
}

function delete_form_info($form_id)
{
    $this->db->where('form_id',$form_id);
    $this->db->delete('form');
}
    ////////private message//////
function send_new_private_message() {
    $message    = $this->input->post('message');
    $timestamp  = strtotime(date("Y-m-d H:i:s"));

    $reciever   = $this->input->post('reciever');
    $sender     = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');

        //check if the thread between those 2 users exists, if not create new thread
    $num1 = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->num_rows();
    $num2 = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->num_rows();

    if ($num1 == 0 && $num2 == 0) {
        $message_thread_code                        = substr(md5(rand(100000000, 20000000000)), 0, 15);
        $data_message_thread['message_thread_code'] = $message_thread_code;
        $data_message_thread['sender']              = $sender;
        $data_message_thread['reciever']            = $reciever;
        $this->db->insert('message_thread', $data_message_thread);
    }
    if ($num1 > 0)
        $message_thread_code = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->row()->message_thread_code;
    if ($num2 > 0)
        $message_thread_code = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->row()->message_thread_code;


    $data_message['message_thread_code']    = $message_thread_code;
    $data_message['message']                = $message;
    $data_message['sender']                 = $sender;
    $data_message['timestamp']              = $timestamp;
    $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());

    return $message_thread_code;
}

function send_reply_message($message_thread_code) {
    $message    = $this->input->post('message');
    $timestamp  = strtotime(date("Y-m-d H:i:s"));
    $sender     = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');


    $data_message['message_thread_code']    = $message_thread_code;
    $data_message['message']                = $message;
    $data_message['sender']                 = $sender;
    $data_message['timestamp']              = $timestamp;
    $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());
}

function mark_thread_messages_read($message_thread_code) {
        // mark read only the oponnent messages of this thread, not currently logged in user's sent messages
    $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
    $this->db->where('sender !=', $current_user);
    $this->db->where('message_thread_code', $message_thread_code);
    $this->db->update('message', array('read_status' => 1));
}

function count_unread_message_of_thread($message_thread_code) {
    $unread_message_counter = 0;
    $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
    $messages = $this->db->get_where('message', array('message_thread_code' => $message_thread_code))->result_array();
    foreach ($messages as $row) {
        if ($row['sender'] != $current_user && $row['read_status'] == '0')
            $unread_message_counter++;
    }
    return $unread_message_counter;
}

}
