<?php 
    $child_of_parent = $this->db->get_where('student' , array(
        'student_id' => $student_id
    ))->result_array();
    foreach ($child_of_parent as $row):
?>
<hr />
<div class="label label-primary pull-right" style="font-size: 14px;">
    <i class="entypo-user"></i> <?php echo $row['name'];?>    
</div>
<div class="label label-primary" style="font-size: 14px;">
    <?php echo get_phrase ('Attendance_::')?> <?php  $attendence = $this->db->get_where('attendance' , array('student_id' => $student_id , 'date' => date('Y-m-d')))->row()->status;?>
       <label for="">
           <?php 
               if($attendence == 1)
             echo  get_phrase('Present');
                 if($attendence == 2)
       echo get_phrase('Absent');
                    if($attendence == 0)
       echo get_phrase('Not_yet_taken');    
            ?>
       </label>
     
  
</div>

<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('course_routine_list');?>
                    	</a></li>
		</ul>

    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane active" id="list">
				<div class="panel-group joined" id="accordion-test-2">
                	
                        
                
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                		<h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapse<?php echo $row['course_id'];?>">
                                        <i class="entypo-rss"></i> Course <?php echo $row['course_id'];?>
                                    </a>
                                    </h4>
                                </div>
                
                                <div id="collapse" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <table cellpadding="0" cellspacing="0" border="0"  class="table table-bordered">
                                            <tbody>
                                                <?php 
                                                for($d=1;$d<=7;$d++):
                                                
                                                if($d==1)$day='sunday';
                                                else if($d==2)$day='monday';
                                                else if($d==3)$day='tuesday';
                                                else if($d==4)$day='wednesday';
                                                else if($d==5)$day='thursday';
                                                else if($d==6)$day='friday';
                                                else if($d==7)$day='saturday';
                                                ?>
                                                <tr class="gradeA">
                                                    <td width="100"><?php echo strtoupper($day);?></td>
                                                    <td>
                                                    	<?php
														$this->db->order_by("time_start", "asc");
														$this->db->where('day' , $day);
														$this->db->where('course_id' , $row['course_id']);
														$routines	=	$this->db->get('course_routine')->result_array();
														foreach($routines as $row2):
														?>
    															<button class="btn btn-primary" >
                                                         <?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?>
																<?php echo '('.$row2['time_start'].'-'.$row2['time_end'].')';?>
                                                            </button>
														<?php endforeach;?>

                                                    </td>
                                                </tr>
                                                <?php endfor;?>
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
  				</div>
			</div>
            <!----TABLE LISTING ENDS-->
            
            
			
            
		</div>
	</div>
</div>
<?php endforeach;?>
