<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('text_in_home_page'); ?>
                </a></li>
            <?php
            $mis = count($this->db->get('homepage_text')->result_array());
            if ($mis == 0) {
                ?><li>
                    <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                <?php echo get_phrase('add_home_page_text'); ?></a></li>
                <?php
            } else {
                ?>
                <li>
                    <a href="#edit" data-toggle="tab"><i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_home_page_text'); ?></a></li>
                        <?php
            }
            ?>
        </ul>
        <!------CONTROL TABS END------>


        <div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="">
                    <thead>
                        <tr>
                            <th><div><?php echo get_phrase('homepage_text'); ?></div></th>                   		
                    </tr>
                    </thead>
                    <tbody>
                       <?php $homepage = $this->db->get('homepage_text')->result_array(); ?>
                        <?php foreach ($homepage as $row): ?>
                            <tr>                            						
                                <td class="span5"><?php echo $row['homepage_details']; ?></td>							
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url() . 'index.php?admin/homepagetext/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                    <div class="form-group">
                        <label class="col-sm-1 control-label"><?php echo get_phrase('add_home_page_text'); ?></label>
                        <div class="col-sm-10">
                            <div class="box closable-chat-box">
                                <div class="box-content padded">
                                    <div class="chat-message-box">
                                        <textarea name="homepage_details" id="elm1" rows="5" placeholder="<?php echo get_phrase('add_homepage_text'); ?>" class="form-control"><?php echo $row['homepage_details']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('add'); ?></button>
                        </div>
                    </div>
                    </form>                
                </div>                
            </div>
            <!----CREATION FORM ENDS-->

            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="edit" style="padding: 5px">
                <div class="box-content">
                    <?php foreach ($homepage as $row): ?>
                        <?php echo form_open(base_url() . 'index.php?admin/homepagetext/do_update/' . $row['homepage_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                        <div class="form-group">
                            <label class="col-sm-1 control-label"><?php echo get_phrase('home_page_text_edit'); ?></label>
                            <div class="col-sm-11">
                                <div class="box closable-chat-box">
                                    <div class="box-content padded">
                                        <div class="chat-message-box">
                                            <textarea name="homepage_details" id="elm3" rows="5" placeholder="<?php echo get_phrase('edit'); ?>" class="form-control"><?php echo $row['homepage_details'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>              
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-1">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('edit'); ?></button>
                            </div>
                        </div>
                        </form>  
                    <?php endforeach; ?>
                </div>                
            </div>
            <!----CREATION FORM ENDS-->
        </div>
    </div>
</div><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

