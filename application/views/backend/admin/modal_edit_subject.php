<?php 
$edit_data		=	$this->db->get_where('subject' , array('subject_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_subject');?>
            	</div>
            </div>
			<div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/subject/do_update/'.$row['subject_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('course');?></label>
                    <div class="col-sm-5 controls">
                        <select name="course_id" class="form-control">
                            <?php 
                            $courses = $this->db->get('course')->result_array();
                            foreach($courses as $row2):
                            ?>
                                <option value="<?php echo $row2['course_id'];?>"
                                    <?php if($row['course_id'] == $row2['course_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('category');?></label>
                    <div class="col-sm-5 controls">
                        <select name="subject_category_id" class="form-control">
                            <?php
                            $subject_categories = $this->db->get('subject_category')->result_array();
                            foreach($subject_categories as $row2):
                                ?>
                                <option value="<?php echo $row2['subject_category_id'];?>"
                                    <?php if($row['subject_category_id'] == $row2['subject_category_id'])echo 'selected';?>>
                                    <?php echo $row2['name'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('trainer');?></label>
                    <div class="col-sm-5 controls">
                        <select name="teacher_id" class="form-control">
                            <option value=""></option>
                            <?php 
                            $teachers = $this->db->get('teacher')->result_array();
                            foreach($teachers as $row2):
                            ?>
                                <option value="<?php echo $row2['teacher_id'];?>"
                                    <?php if($row['teacher_id'] == $row2['teacher_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('fullmark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="fullmark" value="<?php echo $row['fullmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('passmark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="passmark" value="<?php echo $row['passmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('theory_full_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="theoryfullmark" value="<?php echo $row['theoryfullmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('theory_pass_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="theorypassmark" value="<?php echo $row['theorypassmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('subjective_full_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="subjectivefullmark" value="<?php echo $row['subjectivefullmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('subjective_pass_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="subjectivepassmark" value="<?php echo $row['subjectivepassmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('objective_full_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="objectivefullmark" value="<?php echo $row['objectivefullmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('objective_pass_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="objectivepassmark" value="<?php echo $row['objectivepassmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('practical_full_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="practicalfullmark" value="<?php echo $row['practicalfullmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('practical_pass_mark');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="practicalpassmark" value="<?php echo $row['practicalpassmark'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_subject');?></button>
                    </div>
                 </div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>



