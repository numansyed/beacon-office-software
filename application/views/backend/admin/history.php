<div class="row">
    <div class="col-md-12">
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('history'); ?>
                </a></li>
            <li>
                <a href="#edit" data-toggle="tab"><i class="entypo-pencil"></i>
                    <?php echo get_phrase('edit_history'); ?></a>
            </li> 
        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table class="table table-bordered " id="">
                    <thead>
                        <tr>
                          
                    <th><div><?php echo get_phrase('history'); ?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $histories = $this->db->get('history')->result_array(); ?>
                        <?php foreach ($histories as $row): ?>
                            <tr>
                              
                                <td class="span5"><?php echo $row['history']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <!------EDIT TABS START------>
            <div class="tab-pane box" id="edit" style="padding: 0px">
                <div class="box-content">
                    <?php foreach ($histories as $row): ?>
                        <?php echo form_open(base_url() . 'index.php?admin/history_information/do_update/' . $row['history_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>                       
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><?php echo get_phrase('history'); ?></label>
                            <div class="col-sm-10">
                                <div class="box closable-chat-box">
                                    <div class="box-content padded">
                                        <div class="chat-message-box">
                                            <textarea name="history" id="elm2" rows="5" placeholder="<?php echo get_phrase('edit_history'); ?>" class="form-control "><?php echo $row['history']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('edit'); ?></button>
                            </div>
                        </div>
                        </form> 
                    <?php endforeach; ?>
                </div>                
            </div>
            <!----EDIT FORM ENDS-->
        </div>
    </div>
</div>