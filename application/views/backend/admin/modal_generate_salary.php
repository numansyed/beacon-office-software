<?php 
$edit_data		=	$this->db->get_where('teacher' , array('teacher_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i> 
					<?php echo get_phrase('generate_salary');?>
            	</div>
            </div>
			<div class="panel-body">
                    <?php echo form_open(base_url() . 'index.php?admin/teacher_salary/create/'.$row['teacher_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>
                        		
                          
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('for_month');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="datepicker form-control" name="datetime" data-start-view="1"/>
                                </div>
                            </div>
                           
                            
                          
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('no_of_class');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" name="no_of_class" />
                                </div>
                            </div>
                           
                            
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('genrate');?></button>
                            </div>
                        </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>
<script type="text/javascript">
    $(".datepicker").datepicker( {
    format: "mm-yyyy",
    startView: "months", 
    minViewMode: "months"
});
</script>