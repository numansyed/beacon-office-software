<?php
function cmpd($a, $b)
{
    if ($a->point == $b->point) {
        return 0;
    }
    return ($a->point < $b->point) ? 1 : -1;
}
function makenice($value) {
    if(strtolower($value) == 'a')
        return strtoupper($value);

    if (floatval($value) <= 0)
        return '-';

    return number_format((float)(floatval($value)), 2, '.', '');
}
?>
<style>
    #gradechart tr td th, #stats tr td th {
        padding: 2px 2px 2px 2px;
        font-size: 8pt !important;
    }
    #gradechart {
        width: 220px !important;
        border-style: solid;
        border-color: #999999;
    }
    #stats {
        border-style: solid;
        border-color: #999999;
        width: 250px !important;
    }
    #gradechart th, #stats th { font-weight: bold !important;}
    #main tr td th {
        padding: 2px 2px 2px 2px;
        font-size: 10pt !important;
    }
    #main {
        color: black !important;
        background-color: white !important;
        font-family: 'MS Sans Serif', sans-serif, Verdana, Arial !important;
        border-color: black !important;
        border-style: solid;
        border-width: 1px;
    }
    #tabulationsheet_container {
        margin: 0 0 0 0 !important;
        padding: 10px 10px 10px 10px !important;
        border: 1px solid black !important;
        overflow: auto;
    }
    #mainheader, #tabledata, #footer {background-color: white; color: black; width: 99%;}
</style>
<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('view_tabulationsheets');?>
                </a></li>
            <li>
        </ul>
        <!------CONTROL TABS END------>

        <!----TABLE LISTING STARTS-->
        <div class="tab-content">
            <div class="tab-pane active" id="list">
                <?php echo form_open(base_url() . 'index.php?admin/student_tabulationsheet' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td><?php echo get_phrase('select_session');?></td>
                        <td><?php echo get_phrase('select_exam');?></td>
                        <td><?php echo get_phrase('select_course');?></td>
                        <td><?php echo get_phrase('option');?></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="session" name="session" class="form-control"  style="float:left;" onchange="enable_submit(this);">
                                <option value=""><?php echo get_phrase('select_session');?></option>
                                <?php
                                $year = date('Y');
                                for($i=-5; $i < 5; $i++):?>
                                    <option value="<?php echo ($year + $i); ?>"
                                        <?php if(($year + $i) == $session)echo 'selected';?>>
                                        <?php echo ($year + $i); ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                            <select id="exam_id" name="exam_id" class="form-control"  style="float:left;" onchange="enable_submit();">
                                <option value=""><?php echo get_phrase('select_exam');?></option>
                                <?php
                                $exams = $this->db->get('exam')->result_array();
                                foreach($exams as $row):
                                    ?>
                                    <option id="<?php echo $row['session'];?>" value="<?php echo $row['exam_id'];?>"
                                        <?php if($session != $row['session'])echo 'style="display: none;"';?>
                                        <?php if($exam_id == $row['exam_id'])echo 'selected';?>>
                                        <?php echo get_phrase('exam');?> <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <select id="course_id" name="course_id" class="form-control"  style="float:left;" onchange="enable_submit();">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($course_id == $row['course_id'])echo 'selected';?>>
                                        Course <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <input id="tabulationsheet" type="submit" value="<?php echo get_phrase('get_tabulationsheets');?>" class="btn btn-info"
                                <?php if(empty($course_id))echo 'disabled';?> />
                        </td>
                    </tr>
                </table>
                <?php echo form_close();?>
                <?php if (count($result) > 0): ?>
                    <table width="100%" border="0" class="table table-bordered">
                        <tr>
                            <td align="center">
                                <a id="download" href="#" target="_blank"
                                   class="btn btn-info btn-sm"><i class="entypo-download"></i> Download Tabulationsheet</a>
                            </td>
                        </tr>
                    </table>
                    <div id="tabulationsheet_container">
                        <div id="mainheader">
                            <table width="99%" id="top_header" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <table border="1" cellpadding="2" cellspacing="1" id="gradechart">
                                        <tr>
                                            <th colspan="3">&nbsp;Grading System</th>
                                        </tr>
                                        <tr>
                                            <td align="center">Course Interval</td>
                                            <td align="center">Letter Grade</td>
                                            <td align="center">Grade Point</td>
                                        </tr>
                                        <?php
                                        //usort($grades, 'cmpd');
                                        foreach($grades as $grade):?>
                                            <tr>
                                                <td align="center"><?php echo $grade['mark_from'] . '-' . $grade['mark_upto']; ?></td>
                                                <td align="center"><?php echo $grade['name']; ?></td>
                                                <td align="center"><?php echo $grade['grade_point']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                                <td align="center">
                                    <table id="heading" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center"><img src="uploads/logo.png" class="img-responsive" style="max-height:35px"/></td></tr>
                                        <tr><td align="center" style="font-size: 14pt;"><strong><?php echo $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description; ?></strong></td></tr>
                                        <tr><td align="center" style="font-size: 11pt;"><strong>Tabulation Sheet</strong></td></tr>
                                        <tr><td align="center" style="font-size: 11pt;"><strong><?php echo ucwords($this->crud_model->get_type_name_by_id('exam', $exam_id)); ?> Exam of Course <?php echo ucwords($this->crud_model->get_type_name_by_id('course', $course_id)); ?></strong></td></tr>
                                        <tr><td align="center" style="font-size: 11pt;"><strong>Held in <?php echo date('F-Y', strtotime($this->crud_model->get_type_name_by_id('exam', $exam_id, 'date'))); ?></strong></td></tr>
                                    </table>
                                </td>
                                <td style="vertical-align: top;" align="right">
                                    <table id="stats" border="1" cellpadding="2" cellspacing="1">
                                        <tr>
                                            <th colspan="5" align="center">Statistics of Result</th>
                                        </tr>
                                        <tr>
                                            <td align="center">Total</td>
                                            <td align="center">Absent</td>
                                            <td align="center">Appeared</td>
                                            <td align="center">Passed</td>
                                            <td align="center">(%) of Passes</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><strong><?php echo count($result); ?></strong></td>
                                            <td align="center"><strong><?php
                                                    $pr = 0;
                                                    $ab = 0;
                                                    $ps = 0;
                                                    $pr = 0;
                                                    foreach($result as $row) {
                                                        if ($row['result_single'] == 'A')
                                                            $ab++;
                                                        if ($row['result_single'] != 'A')
                                                            $pr++;
                                                        if ($row['result_single'] == 'P')
                                                            $ps++;
                                                    }
                                                    echo $ab; ?></strong></td>
                                            <td align="center"><strong><?php echo $pr; ?></strong></td>
                                            <td align="center"><strong><?php echo $ps; ?></strong></td>
                                            <td align="center"><strong><?php echo number_format((float)round((float)(($ps * 100) / (int)$pr), 2), 2, '.', ''); ?></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </table>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div id="tabledata">
                        <table border="1" id="main" cellpadding="2" cellspacing="1" repeat_header="1" width="99%">
                            <thead>
                            <tr>
                                <th rowspan="2" align="center">Roll</th>
                                <th rowspan="2" align="center">Session</th>
                                <th rowspan="2" align="center">Department</th>
                                <th rowspan="2" align="center">batch</th>
                                <th rowspan="2" align="left">Name</th>
                                <?php foreach ($subjects as $sub): ?>
                                    <th colspan="2" align="center"><?php echo $sub['name']; ?></th>
                                <?php endforeach; ?>
                                <th rowspan="2" align="center">Total</th>
                                <?php if (intval($this->db->get_where('exam', array('exam_id' => $exam_id, 'session' => $session))->result_array()[0]['type']) == 3): ?>
                                    <th rowspan="2" align="center">Final Total</th>
                                <?php endif; ?>
                                <th rowspan="2" align="center">Grade</th>
                                <th rowspan="2" align="center">GPA</th>
                                <th rowspan="2" align="center">Result</th>
                                <th rowspan="2" align="center">Placement</th>
                                <th rowspan="2" align="center">Remark</th>
                            </tr>
                            <tr>
                                <?php foreach ($subjects as $sub): ?>
                                    <th align="center">Mark</th>
                                    <th align="center">Grade</th>
                                <?php endforeach; ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($result as $row):?>
                                <tr>
                                    <td align="center"><?php echo $row['roll']; ?></td>
                                    <td align="center"><?php echo $row['session']; ?></td>
                                    <td align="center"><?php if (intval($row['department_id']) > 0) {echo $this->crud_model->get_type_name_by_id('department', $row['department_id']);} else {echo '-';} ?></td>
                                    <td align="center"><?php if (intval($row['batch_id']) > 0) echo $this->crud_model->get_type_name_by_id('batch', $row['batch_id']); ?></td>
                                    <td align="left"><?php echo $row['name']; ?></td>
                                    <?php
                                    $_sqlmarks= "SELECT mark.*, subject.*, subject_category.name as category
                                                 FROM mark
                                                 JOIN subject ON subject.subject_id = mark.subject_id
                                                 LEFT JOIN subject_category ON subject_category.subject_category_id = subject.subject_category_id
                                                 WHERE mark.session = '$session'
                                                 AND mark.exam_id = '$exam_id'
                                                 AND mark.course_id = '$course_id'
                                                 AND mark.student_id = '" . $row['student_id'] . "'";
                                    $_marks = $this->db->query($_sqlmarks)->result_array();
                                    foreach($subjects as $sub):?><?php
                                        $_total = 0;
                                        $_grade = '-';
                                        $show_failed = false;
                                        foreach($_marks as $mark) {
                                            if ($sub['name'] == $mark['name']) {
                                                if (floatval($mark['subjectivemark']) < floatval($mark['subjectivepassmark']))
                                                    $show_failed = true;
                                                if (floatval($mark['objectivemark']) < floatval($mark['objectivepassmark']))
                                                    $show_failed = true;
                                                if (floatval($mark['theorymark']) < floatval($mark['theorypassmark']))
                                                    $show_failed = true;
                                                if (floatval($mark['practicalmark']) < floatval($mark['practicalmarkpassmark']))
                                                    $show_failed = true;
                                                if (floatval($mark['mark']) < floatval($mark['passmark']))
                                                    $show_failed = true;

                                                $_grade = $mark['grade'];
                                                $_total = $mark['total'];
                                                break;
                                            }
                                        }
                                        ?><td align="center"><?php
                                        if ($show_failed) echo '<u>';
                                        echo makenice($_total);
                                        if ($show_failed) echo '</u>';?></td>
                                        <td align="center"><?php echo $_grade; ?></td>
                                    <?php endforeach; ?>
                                    <td align="center"><?php echo $row['total']; ?></td>
                                    <?php if (intval($this->db->get_where('exam', array('exam_id' => $exam_id, 'session' => $session))->result_array()[0]['type']) == 3): ?>
                                        <td align="center"><?php echo $row['avg_total']; ?></td>
                                    <?php endif; ?>
                                    <td align="center"><?php echo $row['grade']; ?></td>
                                    <td align="center"><?php echo $row['gpa']; ?></td>
                                    <td align="center"><?php echo $row['result']; ?></td>
                                    <td align="center"><?php echo $row['position']; ?></td>
                                    <td align="center"><?php echo $row['remark']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </div>
                        <br />
                        <br />
                        <div id="footer">
                            <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50%" align="left">Date of Issue:&nbsp;<strong><?php echo date('d-M-Y', strtotime($row['gen_datetime'])); ?></strong></td>
                                    <td width="50%" align="right">Principal:&nbsp;______________________</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <!----TABLE LISTING ENDS-->
        </div>
    </div>
</div>

<script type="text/javascript">
    function enable_submit(obj = null) {
        var _f1 = ($("#course_id").val().length > 0);
        var _f2 = ($("#exam_id").val().length > 0);
        var _f3 = ($("#session").val().length > 0);

        $("#tabulationsheet").prop('disabled', !(_f1 && _f2 && _f3));

        if ($(obj).attr('id') == 'session') {
            var session = $(obj).val();
            $("#exam_id option").each(function() {
                if($(this).attr('id') == session)
                    $(this).css('display', 'block');
                else if($(this).val() != '')
                    $(this).css('display', 'none');
            });

            if ($(obj).val() == '')
                $("#exam_id").val('');
        }
    }

    $("#download").click(function (e) {
        e.preventDefault();

        var pdfheader = $("#mainheader").html();
        var pdfbody = $("#tabledata").html();
        var pdffooter = $("#footer").html();

        var styledata = '<style media="print">' +
            '#gradechart tr td th, #stats tr td th {padding: 2px 2px 2px 2px;}' +
            '#gradechart {font-size: 9pt; width: 60%;font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#stats {font-size: 9pt; width: 70%; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#heading {width: 60%; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;}' +
            '#main {width: 16in; color: black !important;background-color: white !important;font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important;border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#top_header {width: 16in;}' +
            '#main tr td th {padding: 2px 2px 2px 2px;font-size: 10pt !important;}' +
            '</style>';

        $.ajax({
            async: false,
            url: '<?php echo base_url() . 'index.php?admin/gettabulationsheetpdf'; ?>',
            type: 'POST',
            data: { style: styledata,
                header: pdfheader,
                body: pdfbody,
                footer: pdffooter,
                pagesize: 'A3-L',
                filename: 'TabulationSheet - <?php echo ucwords($this->crud_model->get_type_name_by_id('exam', $exam_id)) . ' Exam of Course ' . ucwords($this->crud_model->get_type_name_by_id('course', $course_id)) . ' - ' . date('F-Y', strtotime($this->crud_model->get_type_name_by_id('exam', $exam_id, 'date')));?>'},
            timeout: 10000,
            success: function(result) {
                if (result.length > 0) {
                    var link = $("#download");
                    link.attr("href", result);
                    window.open(link.attr("href"));
                }
            }
        });
    });
</script> 