<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
	<thead>
		<tr>
			<th>Type</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<form method="post" action="<?php echo base_url(); ?>index.php?admin/id_card" class="form">
			<tr class="gradeA">
				<td>
					<div class="form-group">

						<div class="col-sm-12" id="courses">
							<select name="course_id"  class="form-control" required="required" onchange="return get_batch_name(this.value)">
								<option value="" >Select Course</option>
								<?php
								$courses = $this->db->get('course')->result_array();
								foreach ($courses as $row):
									?>
									<option value="<?php echo $row['course_id']; ?>"
										<?php if (isset($course_id) && $course_id == $row['course_id']) echo 'selected="selected"'; ?>>
										<?php echo $row['name']; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-12" id="batches">
							<select name="batch_id" class="form-control" required  id ="batch_selector_holder" onchange="return get_student_list(this.value)">
								<option value="">Select Course First</option>
							</select>
						</div>
						<div class="col-sm-12" id="students">
							<select name="student_id" class="form-control" id ="student_selector_holder">
								<option value="">Select Batch First</option>
							</select>
						</div>
					</div>
				</td>
				<td><input type="submit" value="<?php echo get_phrase('view'); ?>" class="btn btn-info"/>
					<br><input type="button" value="<?php echo get_phrase('clear_cache'); ?>" class="btn btn-danger" onclick="clear_cache()"/>
				</td>
			</tr>
		</form>
	</tbody>
</table>
<table class="table table-bordered datatable" id="table_export">
	<thead>
		<tr>
			<th width="80"><div><?php echo get_phrase('id'); ?></div></th>
			<th><div><?php echo get_phrase('name'); ?></div></th>
			<th><div><?php echo get_phrase('ID_card'); ?></div></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$_count = 1;
		foreach ($data as $row):
			?>
			<tr>
				<td><?php echo $_count; ?></td>
				<td><?php echo $row['name']; ?></td>
				<td align="center">
					<div id="print_certificate" ><img style="width:100%;" class="img-responsive" src="<?php echo $this->crud_model->generate_student_id_card($row['student_id']); ?>" /></div>
					
					<a onclick="PrintElem()" class="btn btn-primary">print</a>
				</td>
			</tr>
			<?php $_count++; ?>
		<?php endforeach; ?>
	</tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function ($)
	{
		var datatable = $("#table_export").dataTable({
			"sPaginationType": "bootstrap",
			"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			"oTableTools": {
				"aButtons": [
				{
					"sExtends": "xls",
					"mColumns": [2]
				},
				{
					"sExtends": "pdf",
					"mColumns": [2]
				},
				{
					"sExtends": "print",
					"fnSetText": "Press 'esc' to return",
					"fnClick": function (nButton, oConfig) {
						datatable.fnSetColumnVis(0, false);
						datatable.fnSetColumnVis(1, false);
						this.fnPrint(true, oConfig);
						window.print();
						$(window).keyup(function (e) {
							if (e.which == 27) {
								datatable.fnSetColumnVis(0, true);
								datatable.fnSetColumnVis(1, true);
							}
						});
					},
				},
				]
			},
		});
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
	function show_batches(type) {
		try {
			document.getElementById('courses').style.visibility = 'hidden';
		}
		catch (err) {
		}

		if (type == 'student') {
			document.getElementById('courses').style.visibility = 'visible';
		}
	}
	function clear_cache() {
		$.ajax({
			async: false,
			url: '<?php echo base_url() . 'index.php?admin/clear_id_cards_cache'; ?>',
			type: 'POST',
			data: {clear_cache: 'yes'},
			timeout: 1000,
			success: function (result) {
				alert(result);
				if (result.length > 0) {
					window.location.href = result;
				}
			}
		});
	}
	function get_batch_name(course_id) {
		$.ajax({
			url: '<?php echo base_url();?>index.php?admin/get_batch_list/' + course_id ,
			success: function(response)
			{
				jQuery('#batch_selector_holder').html(response);

			}
		});

	}
	function get_student_list(batch_id) {

		$.ajax({
			url: '<?php echo base_url();?>index.php?admin/get_student_list/' + batch_id ,
			success: function(response)
			{
				jQuery('#student_selector_holder').html(response);
			}
		});

	}

    // print invoice function
    function PrintElem()
    {
    	var img_source = $("#print_certificate").html();
        Popup(img_source);
    }

    function Popup(data)
    {
        var mywindow = window.open('', '', 'height=400,width=600');
        mywindow.document.write('<html><head>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css" type="text/css" />');
        mywindow.document.write('<style>@page { size: auto;  margin: 0mm; }</style></head><body style="margin:0;padding:0;">');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>