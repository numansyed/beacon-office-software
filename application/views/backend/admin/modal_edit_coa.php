<?php
$edit_data		= array();
if ($param2 == 'master')
    $edit_data		=	$this->db->get_where('a2' , array('a2_id' => $param3) )->result_array();
if ($param2 == 'type')
    $edit_data		=	$this->db->get_where('a3' , array('a3_id' => $param3) )->result_array();
/*if ($param2 == 'course')
    $edit_data		=	$this->db->get_where('a4' , array('a4_id' => $param3) )->result_array();*/

foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-mastering">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
                    <?php
                    if ($param2 == 'master') echo get_phrase('edit_master_account');
                    if ($param2 == 'type') echo get_phrase('edit_type');
                    /*if ($param2 == 'course') echo get_phrase('edit_course');*/
                    ?>
            	</div>
            </div>
			<div class="panel-body">
                <?php if ($param2 == 'master'):?>
                    <?php echo form_open(base_url() . 'index.php?admin/coa/edit/master/' . $row['a2_id'] , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('code');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="a2_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" oldvalue="<?php echo $row['a2_id'];?>" value="<?php echo $row['a2_id'];?>" autofocus onchange="check_availble_id(this, 'a2')">
                            <span id="notice" style="color: darkred;display: none;">ID Not Available</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('type'); ?></label>
                        <div class="col-sm-5">
                            <select name="a3_id" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <?php
                                $types = $this->db->get('a3')->result_array();
                                foreach ($types as $row2):?>
                                    <option value="<?php echo $row2['a3_id']; ?>"
                                        <?php if($row2['a3_id'] == $row['a3_id'])echo 'selected';?>>
                                        <?php echo $row2['a3_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row2['name']; ?>
                                    </option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('active');?></label>
                        <div class="col-sm-5">
                            <select name="inactive" class="form-control" required>
                                <option value="0" <?php if(intval($row['inactive']) == 0)echo 'selected';?>><?php echo get_phrase('yes');?></option>
                                <option value="1" <?php if(intval($row['inactive']) == 1)echo 'selected';?>><?php echo get_phrase('no');?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="comment" value="<?php echo $row['comment'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_master_account');?></button>
                        </div>
                    </div>
                    <?php echo form_close();?>
                <?php endif; ?>
                <?php if ($param2 == 'type'):?>
                    <?php echo form_open(base_url() . 'index.php?admin/coa/edit/type/' . $row['a3_id'] , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('code');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="a3_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" oldvalue="<?php echo $row['a3_id'];?>" value="<?php echo $row['a3_id'];?>" autofocus onchange="check_availble_id(this, 'a3')">
                            <span id="notice" style="color: darkred;display: none;">ID Not Available</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('course'); ?></label>
                        <div class="col-sm-5">
                            <select name="a4_id" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <?php
                                $courses = $this->db->get('a4')->result_array();
                                foreach ($courses as $row2):
                                    ?>
                                    <option value="<?php echo $row2['a4_id']; ?>"
                                        <?php if($row2['a4_id'] == $row['a4_id'])echo 'selected';?>>
                                        <?php echo $row2['a4_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row2['name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('parent'); ?></label>
                        <div class="col-sm-5">
                            <select name="parent_id" class="form-control">
                                <option value="0"><?php echo get_phrase('none');?></option>
                                <?php
                                $types = $this->db->get('a3')->result_array();
                                foreach ($types as $row2):
                                if (intval($row['inactive']) == 0):
                                    ?>
                                    <option value="<?php echo $row2['a3_id']; ?>"
                                        <?php if($row2['a3_id'] == $row['parent_id'])echo 'selected';?>>
                                        <?php echo $row2['a3_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row2['name']; ?>
                                    </option>
                                <?php
                                endif;
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('active');?></label>
                        <div class="col-sm-5">
                            <select name="inactive" class="form-control" required>
                                <option value="0" <?php if(intval($row['inactive']) == 0)echo 'selected';?>><?php echo get_phrase('yes');?></option>
                                <option value="1" <?php if(intval($row['inactive']) == 1)echo 'selected';?>><?php echo get_phrase('no');?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="comment" value="<?php echo $row['comment'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_type');?></button>
                        </div>
                    </div>
                    <?php echo form_close();?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
    <?php
endforeach;
?>
<script type="text/javascript">
    function check_availble_id(obj, type) {
        var id = $(obj).val();
        var oid = $(obj).attr("oldvalue");
        var _reply = 0;

        $('button[type="submit"]').each(function() {
            $(this).prop('disabled', true);
        });
        $("#notice").css("display", "none");

        if (id.length > 0) {
            $.ajax({
                async: false,
                timeout: 4000,
                url: '<?php echo base_url();?>index.php?admin/checkid/' + type + '/' + id,
                success: function (reply) {
                    _reply = reply;
                    if (reply == 0) {
                        if (oid != id) {
                            $('button[type="submit"]').each(function () {
                                $(this).prop('disabled', true);
                            });
                            $("#notice").css("display", "block");
                        } else {
                            $('button[type="submit"]').each(function () {
                                $(this).prop('disabled', false);
                            });
                            _reply = 1;
                        }
                    } else if (reply == 1) {
                        $('button[type="submit"]').each(function() {
                            $(this).prop('disabled', false);
                        });
                        $("#notice").css("display", "none");
                    }
                }
            });
        }
        return (_reply == 1);
    }

    var element = document.querySelector("form");
    element.addEventListener("submit", function(event) {
        event.preventDefault();

        var obj = null;
        var type = "";

        $('input[type="text"]').each(function () {
            var _name = $(this).prop("name");
            if (_name.indexOf('id') > 0) {
                obj = this;
                var _type = _name.split("_");
                type = _type[2];
                return false;
            }
        });

        if (obj && type.length > 0) {
            if (check_availble_id(obj, type)) {
                $("form").submit();
            }
            else {
                return false;
            }
        }
    });
</script>

