<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading"> 
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('Information_form'); ?>
                </div>
            </div>
            <div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/tentative_student/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('name'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="student_name"  data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                        </div>
                    </div>

                 
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('course'); ?></label>
                        <div class="col-sm-10">
                           
                            <?php
                            $courses = $this->db->get('course')->result_array();
                            foreach ($courses as $row2):
                                ?>
                                <input type="checkbox" name="preferred_course[]" value="<?php echo $row2['course_id']; ?>" />
                                    <?php echo $row2['name']; ?>
                               
                                <?php
                            endforeach;
                            ?>
                    </div> 
                </div>
                  
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Area'); ?></label>
                        <div class="col-sm-10">
                            <select name="m_area" class="form-control select2" data-validate="required" id="mArea"
                            data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $m_areas = $this->db->get('m_area')->result_array();
                            foreach ($m_areas as $row5):
                                ?>
                                <option value="<?php echo $row5['name']; ?>">
                                    <?php echo $row5['name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
               

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('phone'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="contact_no" placeholder="insert with leading 88 only e.g 8801234567891" value="" >
                        </div>
                    </div>
               
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-10">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('add'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function get_course_batchs(course_id) {
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
            success: function (response)
            {
                jQuery('#batch_selector_holder').html(response);
            }
        });
    }
    function no_parents(parent_id) {
        if (parent_id == false) {
            $("#no_parents").show();
        }
        else {
            $("#no_parents").hide();
        }

    }
</script>