<br><br>
<center>
    <a onClick="PrintElem('#balancesheet_print')" class="btn btn-default btn-icon icon-left hidden-print pull-right">
        Print Balancesheet
        <i class="entypo-print"></i>
    </a>
</center>
<br />
<br />
<br />
<br />

<table class="table table-bordered" id="balancesheet_print">
    <tr>
        <td colspan="2" style="font-size: 20px; text-align:center;"><?php echo $system_name; ?>&nbsp;<br /><?php echo $page_title?></td>
        
    </tr>
    <tr>
        <td>Income</td>
        <td>Expense</td>
    </tr>
    <tr>
          <td>
        <div class="col-md-12">
            <table class="table table-bordered" id="">
                <thead>
                    <tr>
                        <th><div>#</div></th>
                <th><div>Date</div></th>
                <th><div>Income</div></th>
                </tr>
                </thead>
                <tfoot>
                <td></td>
                <td>Total Income</td>
                <td><?php
                    //	$length = $this->db->count_all('payment');
                    $this->db->where('payment_type', 'income');
                    $this->db->order_by('timestamp', 'desc');
                    $result = $this->db->get('payment')->result_array();
                    $total = 0;
                    foreach ($result as $row):
                        if (date('M,Y', $row['timestamp']) == date('M,Y', $monthfrom)):
                            $total = $total + $row['amount'];
                        endif;
                    endforeach;

                    echo $total;
                    ?></td>
                </tfoot>
                <tbody>
<?php
$count = 1;
$this->db->where('payment_type', 'income');
$this->db->order_by('timestamp', 'desc');
$payments = $this->db->get('payment')->result_array();
foreach ($payments as $row):
    if (date('M,Y', $row['timestamp']) == date('M,Y', $monthfrom)):
        ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo date('M,Y', $row['timestamp']); ?></td>  
                                <td><?php echo $row['amount']; ?></td>
                            </tr>
        <?php
    endif;
endforeach;
?>
                </tbody>
            </table>
        </div>
    </td>
    <td>
        <div class="col-md-12">
            <table class="table table-bordered " id="">
                <thead>
                    <tr>
                        <th><div>#</div></th>
                <th><div>Date</div></th>
                <th><div>Expense</div></th>
                </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>Total Expense</td>
                        <td><?php
                    //	$length = $this->db->count_all('payment');
                    $this->db->where('payment_type', 'expense');
                    $this->db->order_by('timestamp', 'desc');
                    $result = $this->db->get('payment')->result_array();
                    $etotal = 0;
                    foreach ($result as $row):
                        if (date('M,Y', $row['timestamp']) == date('M,Y', $monthfrom)):
                            $etotal = $etotal + $row['amount'];
                        endif;
                    endforeach;

                    echo $etotal;
?></td>
                    </tr>
                    
                </tfoot>
                <tbody>
                            <?php
                            $count = 1;
                            $this->db->where('payment_type', 'expense');
                            $this->db->order_by('timestamp', 'desc');
                            $payments = $this->db->get('payment')->result_array();
                            foreach ($payments as $row):
                                if (date('M,Y', $row['timestamp']) == date('M,Y', $monthfrom)):
                                    ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo date('M,Y', $row['timestamp']); ?></td>  
                                <td><?php echo $row['amount']; ?></td>
                            </tr>
        <?php
    endif;
endforeach;
?>
                </tbody>
            </table>
        </div>
    </td>
    </tr >
  <tr>                       
                        <td>Balance</td>
                        <td><p style="text-align: right; padding-right: 77px;"><?php
                            $btotal = $total - $etotal;
                            echo $btotal;
?></p></td>
                    </tr>
</table>





<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    // print invoice function
    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'Balancesheet', 'height=800,width=1500');
        mywindow.document.write('<html><head><title>Balance Sheet</title>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write('<div class="col-lg-12">');
        mywindow.document.write('<table style="width: 100%;" class="table table-bordered"><td colspan="2">');
        mywindow.document.write(data);
        mywindow.document.write('</td></table><br><br><p>Authorized Signature</p></div>');
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>

