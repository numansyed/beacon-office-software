<hr />
<div class="row">
    <table class="col-md-12">

        <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->

            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title"><?php echo get_phrase('send_marks'); ?> via SMS</div>
            </div>


            <!-- panel body -->
            <div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/exam_marks_sms/send_sms', array('target' => '_top')); ?>

                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr><td>
                        <select id="session" name="session" class="form-control"  style="float:left;" onchange="show_exams(this);">
                            <option value=""><?php echo get_phrase('select_session');?></option>
                            <?php
                            $year = date('Y');
                            for($i=-5; $i < 5; $i++):?>
                                <option value="<?php echo ($year + $i); ?>"
                                    <?php if(($year + $i) == $session)echo 'selected';?>>
                                    <?php echo ($year + $i); ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>

                <td>
                        <select id="exam_id" name="exam_id" class="form-control" required>
                            <option value=""><?php echo get_phrase('select_exam'); ?></option>
                            <?php
                            $exams = $this->db->get('exam')->result_array();
                            foreach ($exams as $row):
                                ?>
                                <option id="<?php echo $row['session'];?>" value="<?php echo $row['exam_id'];?>"
                                    <?php if($session != $row['session'])echo 'style="display: none;"';?>
                                    <?php if($exam_id == $row['exam_id'])echo 'selected';?>><?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>

                <td>
                        <select name="course_id" class="form-control" required>
                            <option value=""><?php echo get_phrase('select_course'); ?></option>
                            <?php
                            $courses = $this->db->get('course')->result_array();
                            foreach ($courses as $row):
                                ?>
                                <option value="<?php echo $row['course_id']; ?>"><?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>

                <td>
                        <select name="receiver" class="form-control" required>
                            <option value=""><?php echo get_phrase('select_receiver'); ?></option>
                            <option value="student"><?php echo get_phrase('students'); ?></option>
                            <option value="parent"><?php echo get_phrase('parents'); ?></option>
                        </select></td>

                <td>
                    <button type="submit" class="btn btn-primary"><?php echo get_phrase('send_marks'); ?> via SMS</button>
                    </td>
                        </tr>


        </table>
                <?php echo form_close(); ?>
            </div>

        </div>

    </div>
</div>

<script type="text/javascript">
    function show_exams(obj = null) {
            var session = $(obj).val();
            $("#exam_id option").each(function() {
                if($(this).attr('id') == session)
                    $(this).css('display', 'block');
                else if($(this).val() != '')
                    $(this).css('display', 'none');
            });

            if ($(obj).val() == '')
                $("#exam_id").val('');
    }
</script>