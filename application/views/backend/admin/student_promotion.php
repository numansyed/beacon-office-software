<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li <?php if(!$individual){echo 'course="active"';} ?>>
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('general_promotion'); ?>
                </a>
            </li>
            <li <?php if($individual){echo 'course="active"';} ?>>
                <a href="#edit" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('individual_promotion'); ?></a>
            </li>
        </ul>
        <!------CONTROL TABS END------>
        
	
            <!----TABLE LISTING STARTS-->
        <div class="tab-content">
            <div class="tab-pane <?php if(!$individual){echo 'active';}else{echo 'box';} ?>" id="list">
                <?php echo form_open(base_url() . 'index.php?admin/student_promotion/promote' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                	<tr>
                        <td><?php echo get_phrase('select_session');?></td>
                        <td><?php echo get_phrase('from_course_&_batch');?></td>
                        <td><?php echo get_phrase('to_course_&_batch');?></td>
                        <td><?php echo get_phrase('option');?></td>
                	</tr>
                	<tr>
                        <td>
                            <select id="session" name="session" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_session');?></option>
                                <?php
                                $year = date('Y');
                                for($i=-5; $i < 5; $i++):?>
                                    <option value="<?php echo ($year + $i); ?>"
                                        <?php if(($year + $i) == $session)echo 'selected';?>>
                                        <?php echo ($year + $i); ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                        	<select id="from_course_id" name="from_course_id" class="form-control"  style="float:left;" onchange="getbatchs(this.value, '#from_batch_id')">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php 
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($from_course_id == $row['course_id'])echo 'selected';?>>
                                            Course <?php echo $row['name'];?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                            <select id="from_batch_id" name="from_batch_id" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_course_first');?></option>
                                <?php if (!empty($from_batch_id)) {
                                    $batchs = $this->db->get_where('batch', array(
                                        'course_id' => $from_course_id
                                    ))->result_array();
                                    foreach ($batchs as $row) {
                                        echo '<option ' . ($from_batch_id == $row['batch_id'] ? 'selected' : '') . ' value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
                                    }
                                }?>
                            </select>
                        </td>
                        <td>
                            <select id="to_course_id" name="to_course_id" class="form-control"  style="float:left;" onchange="getbatchs(this.value, '#to_batch_id')">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($to_course_id == $row['course_id'])echo 'selected';?>>
                                        Course <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                            <select id="to_batch_id" name="to_batch_id" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_course_first');?></option>
                                <?php if (!empty($to_batch_id)) {
                                    $batchs = $this->db->get_where('batch', array(
                                        'course_id' => $to_course_id
                                    ))->result_array();
                                    foreach ($batchs as $row) {
                                        echo '<option ' . ($to_batch_id == $row['batch_id'] ? 'selected' : '') . ' value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
                                    }
                                }?>
                            </select>
                        </td>
                        <td>
                            <input id="promote" type="submit" value="<?php echo get_phrase('promote');?>" class="btn btn-info" />
                        </td>
                	</tr>
                </table>
                </form>
                <?php if (count($student_promoted) > 0):?>
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th width="80">ID</th>
                        <th><?php echo get_phrase('name'); ?></th>
                        <th width="80"><div><?php echo get_phrase('result'); ?></th>
                        <th><?php echo get_phrase('promoted_to_course'); ?></th>
                        <th width="80">Old Roll</th>
                        <th width="80">New Roll</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($student_promoted as $row):?>
                        <tr>
                            <td><?php echo $row['student_id']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['result']; ?></td>
                            <td><?php echo $this->crud_model->get_course_name($row['course_id']); ?></td>
                            <td><?php echo $row['oldroll']; ?></td>
                            <td><?php echo $row['roll']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
			</div>
        <!----TABLE LISTING ENDS-->
        <div class="tab-pane <?php if($individual){echo 'active';}else{echo 'box';} ?>" id="edit" style="padding: 5px">
                <?php echo form_open(base_url() . 'index.php?admin/student_promotion/individual' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td><?php echo get_phrase('select_session');?></td>
                        <td><?php echo get_phrase('from_course _&_batch');?></td>
                        <td><?php echo get_phrase('to_course_&_batch');?></td>
                        <td><?php echo get_phrase('option');?></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="i_session" name="i_session" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_session');?></option>
                                <?php
                                $year = date('Y');
                                for($i=-5; $i < 5; $i++):?>
                                    <option value="<?php echo ($year + $i); ?>"
                                        <?php if(($year + $i) == $i_session)echo 'selected';?>>
                                        <?php echo ($year + $i); ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                            <select id="i_course_id" name="i_course_id" class="form-control"  style="float:left;" onchange="getbatchs(this.value, '#i_from_batch_id')">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($i_course_id == $row['course_id'])echo 'selected';?>>
                                        Course <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                            <select id="i_from_batch_id" name="i_from_batch_id" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_course_first');?></option>
                                <?php if (!empty($i_from_batch_id)) {
                                    $batchs = $this->db->get_where('batch', array(
                                        'course_id' => $i_course_id
                                    ))->result_array();
                                    foreach ($batchs as $row) {
                                        echo '<option ' . ($i_from_batch_id == $row['batch_id'] ? 'selected' : '') . ' value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
                                    }
                                }?>
                            </select>
                        </td>
                        <td>
                            <select id="i_to_course_id" name="i_to_course_id" class="form-control"  style="float:left;" onchange="getbatchs(this.value, '#i_to_batch_id')">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($i_to_course_id == $row['course_id'])echo 'selected';?>>
                                        Course <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                            <select id="i_to_batch_id" name="i_to_batch_id" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_course_first');?></option>
                                <?php if (!empty($i_to_batch_id)) {
                                    $batchs = $this->db->get_where('batch', array(
                                        'course_id' => $i_to_course_id
                                    ))->result_array();
                                    foreach ($batchs as $row) {
                                        echo '<option ' . ($i_to_batch_id == $row['batch_id'] ? 'selected' : '') . ' value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
                                    }
                                }?>
                            </select>
                        </td>
                        <td>
                            <input type="submit" value="<?php echo get_phrase('filter');?>" class="btn btn-info" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php if (count($student) > 0): ?>
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th width="80">ID</th>
                        <th width="80">Roll</th>
                        <th><?php echo get_phrase('name'); ?></th>
                        <th width="80"><div><?php echo get_phrase('result'); ?></th>
                        <th><?php echo get_phrase('details'); ?></th>
                        <th><?php echo get_phrase('option'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($student as $row):?>
                        <tr>
                            <td><?php echo $row['student_id']; ?></td>
                            <td><?php echo $row['roll']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['result']; ?></td>
                            <td><?php echo $row['details']; ?></td>
                            <td>
                                <input type="button" value="<?php echo get_phrase('promote');?>" onclick="promote(this, <?php echo $row['student_id']; ?>);" class="btn btn-info" />
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
	</div>
    </div>

<script type="text/javascript">
  function enable_promote() {
      var _f1 = ($("#to_course_id").val().length > 0);
      var _f2 = ($("#to_batch_id").val().length > 0);
      var _f3 = ($("#from_course_id").val().length > 0);
      var _f4 = ($("#from_batch_id").val().length > 0);
      var _f5 = ($("#session").val().length > 0);
      var _f6 = ($("#to_course_id").val() != $("#from_course_id").val());
      //$("#promote").prop('disabled', !(_f1 && _f2 && _f3 && _f4 && _f5 && _f6));
  }

  function getbatchs(course_id, batch_holder) {
      $.ajax({
          url: '<?php echo base_url();?>index.php?admin/get_batch_list/' + course_id ,
          success: function(response)
          {
              $(batch_holder).html(response);
          }
      });

      enable_promote();
  }

/*  function enable_exam(obj, exam) {
      if (obj) {
          var session = $(obj).val();
          $(exam + " option").each(function() {
              if($(this).attr('id') == session)
                  $(this).css('display', 'block');
              else if($(this).val() != '')
                  $(this).css('display', 'none');
          });

          if ($(obj).val() == '')
              $(exam).val('');
      }

      if ($(obj).attr('id') == 'session') {
          enable_promote();
      }
  }*/

  function promote(obj, student_id) {
      var _from_course = $("#i_course_id").val();
      var _to_course = $("#i_to_course_id").val();
      var _to_batch = $("#i_to_batch_id").val();
      var _from_batch = $("#i_from_batch_id").val();
      var _session = $("#i_session").val();

      var _f1 = (_from_course.length > 0);
      var _f2 = (_to_course.length > 0);
      var _f3 = (_to_batch.length > 0);
      var _f4 = (_session.length > 0);
      var _f5 = (_from_course != _to_course);
      var _f6 = (_from_batch.length > 0);

      if (!(_f1 && _f2 && _f3 && _f4 && _f5 && _f6)) {
          return;
      }

      $.ajax({
          async: false,
          url: '<?php echo base_url() . 'index.php?admin/student_promotion/individual/promote'; ?>',
          type: 'POST',
          data: { i_course_id: _from_course,
              i_from_batch_id: _from_batch,
              i_to_course_id: _to_course,
              i_to_batch_id: _to_batch,
              i_session: _session,
              i_student_id: student_id},
          timeout: 4000,
          success: function(result) {
              if (result.trim() == 'ok') {
                  $(obj).closest('tr').remove();
              } else {
                  //alert(result);
                  //window.location.href = result;
              }
          }
      });
  }
</script> 