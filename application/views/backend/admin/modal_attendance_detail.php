<?php
$this->db->select('*');
$this->db->from('employee_attendance');
$this->db->where(array('date' => $param5 . '-' . $param3 . '-' . $param4, 'employee_id' => $param6));
$this->db->join('teacher', 'teacher.teacher_id = employee_attendance.employee_id');
$data = $this->db->get()->result_array();

$_name = $data[0]['name'];
?>
<div class="row">
    <div class="col-md-16">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-right-circled"></i>
                    <?php echo get_phrase('Attendance Details of ') . $_name;?>
                </div>
            </div>
            <div class="panel-body">
                    <table class="table table-bordered datatable" id="table_export_modal">
                        <thead>
                        <tr>
                            <th width="80"><div><?php echo get_phrase('id'); ?></div></th>
                            <th><div><?php echo get_phrase('date'); ?></div></th>
                            <th><div><?php echo get_phrase('status'); ?></div></th>
                            <th><div><?php echo get_phrase('time'); ?></div></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $_count = 1;
                        foreach ($data as $row):
                        ?>
                            <tr>
                                <td><?php echo $_count; ?></td>
                                <td><?php echo date('m/d/Y', strtotime($row['date'])); ?></td>
                                <td align="center"><?php
                                    if ($row['status'] == 0) {
                                        echo '<span class="badge badge-danger">' . get_phrase('absent') . '</span></td><td>N/A</td>';
                                    }
                                    else if ($row['status'] == 1) {
                                        echo '<span class="badge badge-success">' . get_phrase('In') . '</span></td><td>' . date('g:i a',strtotime($row['time']));
                                    }
                                    else if ($row['status'] == 2) {
                                        echo '<span class="badge badge-danger">' . get_phrase('Out') . '</span></td><td>' . date('g:i a',strtotime($row['time']));
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php $_count++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
			</div>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        /*var datatable = $("#table_export_modal").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            //datatable.fnSetColumnVis(3, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    //datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        */
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>