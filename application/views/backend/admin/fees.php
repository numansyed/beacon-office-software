<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('fees'); ?>
                </a></li>
            <li>
                <a href="#edit" data-toggle="tab"><i class="entypo-pencil"></i>
                    <?php echo get_phrase('edit_fees'); ?></a>
            </li> ;
        </ul>
        <!------CONTROL TABS END------>


        <div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="">
                    <thead>
                        <tr>
                    <th><div><?php echo get_phrase('fees'); ?></div></th>

                    
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($feess as $row): ?>
                            <tr>
                                <td class="span5"><?php echo $row['fees']; ?></td>
                            </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="edit" style="padding: 5px">
                <div class="box-content">
                  <?php  foreach ($feess as $row): ?>
<?php echo form_open(base_url() . 'index.php?admin/fees_information/do_update/' . $row['fees_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
;
                    <div class="form-group">
                        <label class="col-sm-1 control-label"><?php echo get_phrase('fees'); ?></label>
                        <div class="col-sm-11">
                            <div class="box closable-chat-box">
                                <div class="box-content padded">
                                    <div class="chat-message-box">
                                        <textarea name="fees" id="elm2" rows="5" placeholder="<?php echo get_phrase('edit_fees'); ?>" class="form-control"><?php echo $row['fees']?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>              
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-1 pull-right">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('edit'); ?></button>
                    </div>
                </div>
                </form>  
                <?php endforeach;?>
            </div>                
        </div>
        <!----CREATION FORM ENDS-->

    </div>
</div>
</div>
