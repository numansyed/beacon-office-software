<?php

$edit_data = $this->db->get_where('a1', array('a1_id' => $param2))->result_array();
foreach ($edit_data as $row):
    $paid = 0;
    $due = 0;

    $this->db->order_by('date', 'desc');
    $payments = $this->db->get_where('a8', array(
        'a1_id' => $param2
    ))->result_array();
    $date = $payments[0]['date'];

    foreach ($payments as $row2) {
        $paid += floatval($row2['amount']);
    }

    $due = floatval($row['total_amount']) - $paid;
    ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Send_SMS_to: &nbsp;::&nbsp;<?php   echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;?>  <br /> Contact# <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->phone;?></h3>
                </div>
            </div>

            <div class="panel-body">
                <?php echo form_open(base_url().'index.php?admin/sms/student/'.$row['student_id'], array('class' => 'form-horizontal form-groups-bordered validate'));?>
                   <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo get_phrase('message');?></label>
                                <div class="col-sm-10">
                                    <div class="box closable-chat-box">
                                        <div class="box-content padded">
                                                <div class="chat-message-box">
                                                    <textarea name="message" id="ttt" rows="8" class="form-control" ><?php
                                                        echo "Your Payment is Successfull!";
                                                        echo "\nInvoice ID:: " . $row['a1_id'];
                                                        echo "\nTotal:: " . $row['total_amount'] . "TK.";
                                                        echo "\nPaid:: $paid TK";
                                                        echo "\nDue:: $due TK.";
                                                        echo "\nPayment Date:: $date";
                                                        ?></textarea>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="col-sm-2 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Send">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
<?php endforeach; ?>