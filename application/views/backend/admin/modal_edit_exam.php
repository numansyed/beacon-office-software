<?php 
$edit_data		=	$this->db->get_where('exam' , array('exam_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_exam');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(base_url() . 'index.php?admin/exam/edit/do_update/'.$row['exam_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('session');?></label>
                    <div class="col-sm-5">
                        <select id="session" name="session" class="form-control"  style="float:left;" onchange="enable_promote();">
                            <?php
                            $year = date('Y');
                            for($i=-5; $i < 5; $i++):?>
                                <option value="<?php echo ($year + $i); ?>"
                                    <?php if(($year + $i) == $row['session'])echo 'selected';?>>
                                    <?php echo ($year + $i); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('date');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="datepicker form-control" name="date" value="<?php echo $row['date'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('course_start_date');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="datepicker form-control" name="startdate" value="<?php echo $row['startdate'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('course_end_date');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="datepicker form-control" name="enddate" value="<?php echo $row['enddate'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('total_working_days');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="workingdays" value="<?php echo $row['workingdays'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('type');?></label>
                    <div class="col-sm-5">
                        <select  name="type" class="form-control"  style="float:left;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                            <option value="0" <?php if (intval($row['type']) == 0) echo 'selected'; ?>>General</option>
                            <option value="1" <?php if (intval($row['type']) == 1) echo 'selected'; ?>>First of 3-Part Exam</option>
                            <option value="2" <?php if (intval($row['type']) == 2) echo 'selected'; ?>>Second of 3-Part Exam</option>
                            <option value="3" <?php if (intval($row['type']) == 3) echo 'selected'; ?>>Final of 3-Part Exam</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="comment" value="<?php echo $row['comment'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_exam');?></button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>





