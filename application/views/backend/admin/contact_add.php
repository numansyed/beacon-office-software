<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_contact_person'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/contact/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_name'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contact_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('contact_address'); ?></label>

                    <div class="col-sm-5">
                        <textarea type="text" class="form-control autogrow" name="contact_address" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus></textarea>
                    </div> 
                </div>
                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('contact_phone'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contact_phone" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus >
                    </div> 
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_email'); ?></label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" name="contact_email"data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('add_contact_person'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function get_course_batchs(course_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
            success: function (response)
            {
                jQuery('#batch_selector_holder').html(response);
            }
        });

    }

</script>