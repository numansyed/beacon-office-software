<?php
function cmpd($a, $b)
{
    if ($a->point == $b->point) {
        return 0;
    }
    return ($a->point < $b->point) ? 1 : -1;
}

function makenice($value)
{
    if (strtolower($value) == 'a')
        return strtoupper($value);

    if (floatval($value) <= 0)
        return '-';

    return number_format((float)(floatval($value)), 2, '.', '');
}

?>
<style>
    #gradechart tr td, #stats tr td {
        padding: 2px 2px 2px 2px;
        font-size: 12pt !important;
    }

    #gradechart {
        width: 210px !important;
        border-style: solid;
        border-color: #999999;
    }

    #stats {
        border-style: solid;
        border-color: #999999;
        width: 210px !important;
    }

    #gradechart th, #stats th {
        font-weight: bold !important;
        font-size: 12pt !important;
    }

    #markstable tr td {
        padding: 2px 2px 2px 2px;
        font-size: 14pt !important;
    }

    #markstable {
        border-style: solid;
        border-color: black;
    }

    #marksheet_container {
        margin: 0 0 0 0 !important;
        padding: 10px 0 10px 0 !important;
        border: 1px solid black !important;
    }

    #header, #tabledata, #footer {background-color: white; color: black; width: 99%;}
</style>
<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('view_marksheets'); ?>
                </a></li>
            <li>
        </ul>
        <!------CONTROL TABS END------>

        <!----TABLE LISTING STARTS-->
        <div class="tab-content">
            <div class="tab-pane active" id="list">
                <?php echo form_open(base_url() . 'index.php?admin/student_marksheet', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td><?php echo get_phrase('select_session'); ?></td>
                        <td><?php echo get_phrase('select_exam'); ?></td>
                        <td><?php echo get_phrase('select_course'); ?></td>
                        <td><?php echo get_phrase('option'); ?></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="session" name="session" class="form-control" style="float:left;"
                                    onchange="enable_submit(this);">
                                <option value=""><?php echo get_phrase('select_session'); ?></option>
                                <?php
                                $year = date('Y');
                                for ($i = -5; $i < 5; $i++):?>
                                    <option value="<?php echo($year + $i); ?>"
                                        <?php if (($year + $i) == $session) echo 'selected'; ?>>
                                        <?php echo($year + $i); ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                            <select id="exam_id" name="exam_id" class="form-control" style="float:left;"
                                    onchange="enable_submit();">
                                <option value=""><?php echo get_phrase('select_exam'); ?></option>
                                <?php
                                $exams = $this->db->get('exam')->result_array();
                                foreach ($exams as $row):
                                    ?>
                                    <option id="<?php echo $row['session']; ?>" value="<?php echo $row['exam_id']; ?>"
                                        <?php if ($session != $row['session']) echo 'style="display: none;"'; ?>
                                        <?php if ($exam_id == $row['exam_id']) echo 'selected'; ?>>
                                        <?php echo get_phrase('exam'); ?><?php echo $row['name']; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <select id="course_id" name="course_id" class="form-control" style="float:left;"
                                    onchange="enable_submit();">
                                <option value=""><?php echo get_phrase('select_course'); ?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach ($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id']; ?>"
                                        <?php if ($course_id == $row['course_id']) echo 'selected'; ?>>
                                        Course <?php echo $row['name']; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <input id="marksheet" type="submit" value="<?php echo get_phrase('get_marksheets'); ?>"
                                   class="btn btn-info"
                                <?php if (empty($course_id)) echo 'disabled'; ?> />
                        </td>
                    </tr>
                </table>
                <?php echo form_close(); ?>
                <?php if (count($result) > 0): ?>
                <table width="100%" border="0" class="table table-bordered">
                    <tr>
                        <td align="center">
                            <a id="download" href="#" target="_blank"
                               class="btn btn-info btn-sm"><i class="entypo-download"></i> Download Marksheet</a>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td><?php echo get_phrase('marksheets'); ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $serial = 1;
                    foreach ($result as $row): ?>
                    <tr>
                        <td><?php echo $serial; ?></td>
                        <td align="center">
                            <div id="marksheet_container">
                                <div id="header">
                                    <table width="99%" id="top_header" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">
                                                <table border="1" cellpadding="0" cellspacing="0" id="gradechart">
                                                    <tr>
                                                        <th colspan="3">&nbsp;Grading System</th>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">Course Interval</td>
                                                        <td align="center">Letter Grade</td>
                                                        <td align="center">Grade Point</td>
                                                    </tr>
                                                    <?php
                                                   // usort($grades, 'cmpd');
                                                    foreach ($grades as $grade):?>
                                                        <tr>
                                                            <td align="center"><?php echo $grade['mark_from'] . '-' . $grade['mark_upto']; ?></td>
                                                            <td align="center"><?php echo $grade['name']; ?></td>
                                                            <td align="center"><?php echo $grade['grade_point']; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <table id="heading" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="center"><img src="uploads/logo.png"
                                                                                class="img-responsive"
                                                                                style="max-height:35px"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 13pt;"><strong><?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 10pt;"><strong>Mark Sheet</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 10pt;"><strong><?php echo ucwords($this->crud_model->get_type_name_by_id('exam', $exam_id)); ?> Exam of Course <?php echo ucwords($this->crud_model->get_type_name_by_id('course', $course_id)); ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 10pt;">&nbsp;<strong>Held
                                                                in <?php echo date('F-Y', strtotime($this->crud_model->get_type_name_by_id('exam', $exam_id, 'date'))); ?></strong>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="vertical-align: top;" align="right">
                                                <table id="stats" border="1" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <th colspan="4" align="center">Statistics of Attendance</th>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">Total Working Days</td>
                                                        <td align="center">Present</td>
                                                        <td align="center">Absent</td>
                                                        <td align="center">&nbsp;%&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">&nbsp;
                                                  <!--          <strong><?php //$wd = intval($this->crud_model->get_type_name_by_id('exam', $exam_id, 'workingdays'));
                                                                echo $wd; ?></strong> --></td>
                                                        <td align="center">&nbsp; <!--   <strong><?php
                                                 //               $sd = $this->crud_model->get_type_name_by_id('exam', $exam_id, 'startdate');
                                                  //              $ed = $this->crud_model->get_type_name_by_id('exam', $exam_id, 'enddate');
                                             //                   $tp = count($this->db->get_where('attendance', array('status' => 1, 'date >=' => $sd, 'date <=' => $ed, 'student_id' => $row['name'])));
                                                               // echo $tp; ?></strong> --></td>
                                                        <td align="center"> &nbsp;<!--   <strong><?php// echo($wd - $tp); ?></strong>-->
                                                        </td>
                                                        <td align="center"> &nbsp;
                                                          <!--      <strong><?php// echo number_format((float)round((float)(($tp * 100) / (int)$wd), 2), 2, '.', ''); ?></strong> -->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <br />
                                <div id="tabledata">
                                    <?php
                                    $_sqlsubs = "SELECT mark.*, subject.*, subject_category.name as category
                                                            FROM mark
                                                            JOIN subject ON subject.subject_id = mark.subject_id
                                                            LEFT JOIN subject_category ON subject_category.subject_category_id = subject.subject_category_id
                                                            WHERE mark.session = '$session'
                                                            AND mark.exam_id = '$exam_id'
                                                            AND mark.course_id = '$course_id'
                                                            AND mark.student_id = '" . $row['student_id'] . "'";
                                    $_allsubs = $this->db->query($_sqlsubs)->result_array(); ?>
                                    <table width="99%" border="0" cellpadding="0" cellspacing="0" id="subheading">
                                        <tr>
                                            <td align="left">Student
                                                Name:&nbsp;<strong><?php echo $row['name']; ?></strong></td>
                                            <?php if (intval($row['department_id']) > 0) echo '<td align="center">Department:&nbsp;<strong>' . $this->crud_model->get_type_name_by_id('department', $row['department_id']) . '</strong></td>'; ?>
                                            <?php if (intval($row['batch_id']) > 0) echo '<td align="center">batch:&nbsp;<strong>' . $this->crud_model->get_type_name_by_id('batch', $row['batch_id']) . '</strong></td>'; ?>
                                            <td align="right">Roll:&nbsp;<strong><?php echo $row['roll']; ?></strong>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="99%" id="markstable" border="1" cellpadding="0" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <td rowspan="3">&nbsp;Subject</td>
                                            <td colspan="5" align="center">Mark</td>
                                            <td rowspan="3" align="center">Letter Grade</td>
                                            <td rowspan="3" align="center">Grade Point</td>
                                            <td rowspan="3" align="center">Highest Mark</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">Theory</td>
                                            <td rowspan="2" align="center">Practical</td>
                                            <td rowspan="2" align="center">Total</td>
                                        </tr>
                                        <tr>
                                            <td align="center">Subjective</td>
                                            <td align="center">Objective</td>
                                            <td align="center">Total</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($_allsubs as $subs): ?>
                                            <tr>
                                                <td>&nbsp;<?php echo $subs['name']; ?></td>
                                                <td align="center"><?php if (floatval($subs['subjectivemark']) < floatval($subs['subjectivepassmark'])) echo '<u>';
                                                    echo makenice($subs['subjectivemark']);
                                                    if (floatval($subs['subjectivemark']) < floatval($subs['subjectivepassmark'])) echo '</u>';
                                                    ?></td>
                                                <td align="center"><?php if (floatval($subs['objectivemark']) < floatval($subs['objectivepassmark'])) echo '<u>';
                                                    echo makenice($subs['objectivemark']);
                                                    if (floatval($subs['objectivemark']) < floatval($subs['objectivepassmark'])) echo '</u>';
                                                    ?></td>
                                                <td align="center"><?php if (floatval($subs['theorymark']) < floatval($subs['theorypassmark'])) echo '<u>';
                                                    echo makenice($subs['theorymark']);
                                                    if (floatval($subs['theorymark']) < floatval($subs['theorypassmark'])) echo '</u>';
                                                    ?></td>
                                                <td align="center"><?php if (floatval($subs['practicalmark']) < floatval($subs['practicalmarkpassmark'])) echo '<u>';
                                                    echo makenice($subs['practicalmark']);
                                                    if (floatval($subs['practicalmark']) < floatval($subs['practicalmarkpassmark'])) echo '</u>';
                                                    ?></td>
                                                <td align="center"><?php if (floatval($subs['mark']) < floatval($subs['passmark'])) echo '<u>';
                                                    echo makenice($subs['total']);
                                                    if (floatval($subs['mark']) < floatval($subs['passmark'])) echo '</u>'; ?></td>
                                                <td align="center"><?php echo $subs['grade']; ?></td>
                                                <td align="center"><?php echo $subs['point']; ?></td>
                                                <td align="center"><?php echo makenice($subs['highestmark']); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td colspan="5">&nbsp;Total</td>
                                            <td align="center"><?php echo $row['total']; ?></td>
                                            <td align="center">-</td>
                                            <td align="center"><?php echo $row['points']; ?></td>
                                            <td align="center">-</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <?php if (intval($this->db->get_where('exam', array('exam_id' => $exam_id, 'session' => $session))->result_array()[0]['type']) == 3): ?>
                                        <?php
                                        $_allexams = $this->db->get_where('exam', array('session' => $session))->result_array();
                                        $first = 0;
                                        $second = 0;
                                        foreach ($_allexams as $_exam) {
                                            if ($_exam['type'] == '1')
                                                $first = $_exam['exam_id'];
                                            if ($_exam['type'] == '2')
                                                $second = $_exam['exam_id'];
                                        }
                                        ?>
                                        <br />
                                        <br />
                                        <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left"><?php echo $this->crud_model->get_type_name_by_id('exam', $first); ?>:&nbsp;<strong><?php echo $this->db->get_where('result', array('session' => $session, 'course_id' => $course_id, 'exam_id' => $first, 'student_id' => $row['student_id']))->result_array()[0]['total']; ?></strong>
                                                </td>
                                                <td align="center"><?php echo $this->crud_model->get_type_name_by_id('exam', $second); ?>:&nbsp;<strong><?php echo $this->db->get_where('result', array('session' => $session, 'course_id' => $course_id, 'exam_id' => $second, 'student_id' => $row['student_id']))->result_array()[0]['total']; ?></strong>
                                                </td>
                                                <td align="right"><?php echo $this->crud_model->get_type_name_by_id('exam', $exam_id); ?>:&nbsp;<strong><?php echo $row['avg_total']; ?></strong></td>
                                            </tr>
                                        </table>
                                    <?php endif; ?>
                                    <br />
                                    <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">Result:&nbsp;<strong><?php echo $row['result']; ?></strong></td>
                                            <td align="center">Grade:&nbsp;<strong><?php echo $row['grade']; ?></strong></td>
                                            <td align="center">GPA:&nbsp;<strong><?php echo $row['gpa']; ?></strong></td>
                                            <td align="right">Placement:&nbsp;<strong><?php echo $row['position']; ?></strong>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php if ($serial < count($result)) echo '<pagebreak />'; ?>
                                </div>
                                <br />
                                <br />
                                <br />
                                <div id="footer">
                                    <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">Date of Issue:&nbsp;<strong><?php echo date('d-M-Y', strtotime($row['gen_datetime'])); ?></strong>
                                            </td>
                                            <td align="right">Principal:&nbsp;______________________</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $serial++;
                            endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
    <!----TABLE LISTING ENDS-->
</div>
</div>
</div>

<script type="text/javascript">
    $(function ($) {
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

    function enable_submit(obj = null) {
        var _f1 = ($("#course_id").val().length > 0);
        var _f2 = ($("#exam_id").val().length > 0);
        var _f3 = ($("#session").val().length > 0);

        $("#marksheet").prop('disabled', !(_f1 && _f2 && _f3));

        if ($(obj).attr('id') == 'session') {
            var session = $(obj).val();
            $("#exam_id option").each(function () {
                if ($(this).attr('id') == session)
                    $(this).css('display', 'block');
                else if ($(this).val() != '')
                    $(this).css('display', 'none');
            });

            if ($(obj).val() == '')
                $("#exam_id").val('');
        }
    }

    $("#download").click(function (e) {
        e.preventDefault();
        var pdfbody = "";
        var pdfheader = "";
        var pdffooter = "";

        $('div[id="tabledata"]').each(function () {
            pdfbody += $(this).html();
        });

        $("#header").each(function () {
            pdfheader = $(this).html();
        });
        $("#footer").each(function () {
            pdffooter = $(this).html();
        });

        var styledata = '<style media="print">' +
            '#gradechart tr td th, #stats tr td th {padding: 2px 2px 2px 2px;}' +
            '#gradechart {font-size: 12pt; width: 210px;font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#stats {font-size: 12pt;width: 210px;font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#markstable tr td th {padding: 2px 2px 2px 2px;font-size: 13pt !important;}' +
            '#heading {width: "360px"; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;}' +
            '#markstable {width: 8.5in; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important; border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '#subheading {width: 8.5in; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important; border-style: none;border-color: white;border-width: 0px;border-collapse: collapse;}' +
            '#top_header {width: 8.5in;font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important;}' +
            '</style>';

        $.ajax({
            async: false,
            url: '<?php echo base_url() . 'index.php?admin/getmarksheetpdf' ?>',
            type: 'POST',
            data: {
                header: pdfheader,
                footer: pdffooter,
                style: styledata,
                body: pdfbody,
                pagesize: 'A5',
                filename: 'MarkSheet - <?php echo ucwords($this->crud_model->get_type_name_by_id('exam', $exam_id)) . ' Exam of Course ' . ucwords($this->crud_model->get_type_name_by_id('course', $course_id)) . ' - ' . date('F-Y', strtotime($this->crud_model->get_type_name_by_id('exam', $exam_id, 'date')));?>'
            },
            timeout: 1000000000,
            success: function (result) {
                if (result.length > 0) {
                    var link = $("#download");
                    link.attr("href", result);
                    window.open(link.attr("href"));
                }
            }
        });
    });
</script> 