<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('manage_marks');?>
                    	</a></li>
            <li>
                <a href="#edit" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('generate_result');?>
                </a></li>
		</ul>
    	<!------CONTROL TABS END------>
	
            <!----TABLE LISTING STARTS-->
        <div class="tab-content">
            <div class="tab-pane active" id="list">
                <?php echo form_open(base_url() . 'index.php?admin/get_subjects' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                	<tr>
                        <td><?php echo get_phrase('select_session');?></td>
                        <td><?php echo get_phrase('select_exam');?></td>
                        <td><?php echo get_phrase('select_course');?></td>
                        <td><?php echo get_phrase('select_subject');?></td>
                        <td><?php echo get_phrase('option');?></td>
                	</tr>
                	<tr>
                        <td>
                            <select id="session" name="session" class="form-control"  style="float:left;" onchange="enable_upload(this);">
                                <option value=""><?php echo get_phrase('select_session');?></option>
                                <?php
                                $year = date('Y');
                                for($i=-5; $i < 5; $i++):?>
                                    <option value="<?php echo ($year + $i); ?>"
                                    <?php if(($year + $i) == $session)echo 'selected';?>>
                                    <?php echo ($year + $i); ?></option>
                                    <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                        	<select id="exam_id" name="exam_id" class="form-control"  style="float:left;" onchange="enable_upload();">
                                <option value=""><?php echo get_phrase('select_exam');?></option>
                                <?php 
                                $exams = $this->db->get('exam')->result_array();
                                foreach($exams as $row):
                                ?>
                                    <option id="<?php echo $row['session'];?>" value="<?php echo $row['exam_id'];?>"
                                        <?php if($session != $row['session'])echo 'style="display: none;"';?>
                                        <?php if($exam_id == $row['exam_id'])echo 'selected';?>>
                                            <?php echo get_phrase('exam');?> <?php echo $row['name'];?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                        	<select id="course_id" name="course_id" class="form-control"  style="float:left;" onchange="enable_upload(this);">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php 
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                ?>
                                    <option value="<?php echo $row['course_id'];?>"
                                        <?php if($course_id == $row['course_id'])echo 'selected';?>>
                                            Course <?php echo $row['name'];?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <select id="subject_id" name="subject_id" class="form-control"  style="float:left;" onchange="enable_upload();">
                                <option value=""><?php echo get_phrase('select_course_first');?></option>
                                <?php
                                $subs = $this->db->get('subject')->result_array();
                                foreach($subs as $row):
                                    ?>
                                    <option id="<?php echo $row['course_id'];?>" value="<?php echo $row['subject_id'];?>"
                                        <?php if($course_id != $row['course_id'])echo 'style="display: none;"';?>
                                        <?php if($subject_id == $row['subject_id'])echo 'selected';?>>
                                        <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <input id="manage_marks" name="manage_marks" type="submit" value="<?php echo get_phrase('manage_marks');?>" class="btn btn-info"
                                <?php if(empty($subject_id))echo 'disabled';?> />
                        </td>
                	</tr>
                </table>
                <?php echo form_close();?>
                <?php if (count($students) > 0): ?>
                <?php echo form_open(base_url() . 'index.php?admin/save_marks' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    <input name="exam_id" type="hidden" value="<?php echo $exam_id; ?>" readonly />
                    <input name="course_id" type="hidden" value="<?php echo $course_id; ?>" readonly />
                    <input name="session" type="hidden" value="<?php echo $session; ?>" readonly />
                    <input name="subject_id" type="hidden" value="<?php echo $subject_id; ?>" readonly />
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td><?php echo get_phrase('roll');?></td>
                        <td><?php echo get_phrase('name');?></td>
                        <td><?php echo get_phrase('mark');?></td>
                        <td><?php echo get_phrase('theory_mark');?></td>
                        <td><?php echo get_phrase('subjective_mark');?></td>
                        <td><?php echo get_phrase('objective_mark');?></td>
                        <td><?php echo get_phrase('practical_mark');?></td>
                    </tr>
                    <?php foreach($students as $row):?>
                    <?php foreach($subject as $row2):?>
                    <tr>
                        <td>
                            <input name="student_id[]" type="hidden" value="<?php echo $row['student_id']; ?>" />
                            <input name="batch_id[]" type="hidden" value="<?php echo $row['batch_id']; ?>" />
                            <span><?php echo $row['roll']; ?></span>
                        </td>
                        <td>
                            <span><?php echo $row['name']; ?></span>
                        </td>
                        <td>
                            <?php if (intval($row2['passmark']) > 0): ?>
                                <input name="mark[]" type="text" value="<?php echo $row['mark']; ?>" class="validate[required]" />
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (intval($row2['theorypassmark']) > 0): ?>
                                <input name="theorymark[]" type="text" value="<?php echo $row['theorymark']; ?>" class="validate[required]" />
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (intval($row2['subjectivepassmark']) > 0): ?>
                                <input name="subjectivemark[]" type="text" value="<?php echo $row['subjectivemark']; ?>" class="validate[required]" />
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (intval($row2['objectivepassmark']) > 0): ?>
                                <input name="objectivemark[]" type="text" value="<?php echo $row['objectivemark']; ?>" class="validate[required]" />
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (intval($row2['practicalpassmark']) > 0): ?>
                                <input name="practicalmark[]" type="text" value="<?php echo $row['practicalmark']; ?>" class="validate[required]" />
                            <?php endif; ?>
                        </td>
                    </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="7" align="center">
                            <table>
                                <tr>
                                    <td id="unlocktd" style="display: none">
                                        <input id="unlock" type="button" value="<?php echo get_phrase('edit_marks');?>" class="btn btn-danger" />&nbsp;
                                    </td>
                                    <td>
                                        &nbsp;<input type="submit" value="<?php echo get_phrase('save_marks');?>" class="btn btn-info" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <?php echo form_close();?>
                <?php endif; ?>
			</div>
            <!----TABLE LISTING ENDS-->
            <div class="tab-pane box" id="edit" style="padding: 5px">
                <?php echo form_open(base_url() . 'index.php?admin/generate_result' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td><?php echo get_phrase('select_session');?></td>
                        <td><?php echo get_phrase('select_exam');?></td>
                        <td><?php echo get_phrase('select_course');?></td>
                        <td><?php echo get_phrase('option');?></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="session_result" name="session_result" class="form-control"  style="float:left;" onchange="show_exams(this)">
                                <option value=""><?php echo get_phrase('select_session');?></option>
                                <?php
                                $year = date('Y');
                                for($i=-5; $i < 5; $i++):?>
                                    <option value="<?php echo ($year + $i); ?>">
                                        <?php echo ($year + $i); ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                            <select id="exam_id_result" name="exam_id_result" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_exam');?></option>
                                <?php
                                $exams = $this->db->get('exam')->result_array();
                                foreach($exams as $row):
                                    ?>
                                    <option id="<?php echo $row['session'];?>" value="<?php echo $row['exam_id'];?>"
                                        <?php if($session_result != $row['session'])echo 'style="display: none;"';?>>
                                        <?php echo get_phrase('exam');?> <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <select id="course_id_result" name="course_id_result" class="form-control"  style="float:left;">
                                <option value=""><?php echo get_phrase('select_course');?></option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id'];?>">
                                        Course <?php echo $row['name'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="submit" value="<?php echo get_phrase('generate_result');?>" class="btn btn-info" />
                        </td>
                    </tr>
                </table>
                <?php echo form_close();?>
            </div>
            </div>
		</div>
	</div>

<script type="text/javascript">
  function enable_upload(obj = null) {
      var _f1 = ($("#course_id").val().length > 0);
      var _f2 = ($("#exam_id").val().length > 0);
      var _f3 = ($("#session").val().length > 0);
      var _f4 = ($("#subject_id").val().length > 0);

      $("#manage_marks").prop('disabled', !(_f1 && _f2 && _f3 && _f4));

      if ($(obj).attr('id') == 'course_id') {
          var course_id = $(obj).val();
          $("#subject_id option").each(function() {
              if($(this).attr('id') == course_id)
                  $(this).css('display', 'block');
              else if($(this).val() != '')
                  $(this).css('display', 'none');
          });

          if ($(obj).val() == '')
              $("#subject_id").val('');
      }
      if ($(obj).attr('id') == 'session') {
          var session = $(obj).val();
          $("#exam_id option").each(function() {
              if($(this).attr('id') == session)
                  $(this).css('display', 'block');
              else if($(this).val() != '')
                  $(this).css('display', 'none');
          });

          if ($(obj).val() == '')
              $("#exam_id").val('');
      }
  }

  function show_exams(obj) {
          var session = $(obj).val();
          $("#exam_id_result option").each(function() {
              if($(this).attr('id') == session)
                  $(this).css('display', 'block');
              else if($(this).val() != '')
                  $(this).css('display', 'none');
          });
          if ($(obj).val() == '')
              $("#exam_id_result").val('');
  }


  $(function() {
      $("#unlocktd").css('display', 'none');

      $("input:text").each(function() {
          if ($(this).val().length > 0) {
              $("#unlocktd").css('display', 'block');
              $(this).prop('readonly', true);
          }
      });
  });

  $("#unlock").click(function () {
      $("input:text").each(function() {
          if ($(this).val().length > 0) {
              $(this).prop('readonly', false);
          }
      });
      $("#unlock").prop('disabled', true);
  });
</script> 