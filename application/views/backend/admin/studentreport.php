<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('Manage_student_report'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------>


        <!----TABLE LISTING STARTS-->
        <div class="tab-pane  <?php if (!isset($edit_data) && !isset($personal_profile) && !isset($academic_result)) echo 'active'; ?>" id="list">
            <center>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
                    <thead>
                        <tr>
                            <th><?php echo get_phrase('select_month'); ?></th>
                            <th><?php echo get_phrase('select_year'); ?></th>
                            <th><?php echo get_phrase('select_course'); ?></th>
                           
                            <th><?php echo get_phrase('select_student'); ?></th>
                            <th><?php echo get_phrase('option'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <form method="post" action="<?php echo base_url(); ?>index.php?admin/studentreport_selector" class="form">
                        <tr class="gradeA">
                            <td>
                                <select name="month" class="form-control">
                                    <?php
                                    for ($i = 1; $i <= 12; $i++):
                                        if ($i == 1)
                                            $m = 'Jan';
                                        else if ($i == 2)
                                            $m = 'Feb';
                                        else if ($i == 3)
                                            $m = 'Mar';
                                        else if ($i == 4)
                                            $m = 'Apr';
                                        else if ($i == 5)
                                            $m = 'May';
                                        else if ($i == 6)
                                            $m = 'Jun';
                                        else if ($i == 7)
                                            $m = 'Jul';
                                        else if ($i == 8)
                                            $m = 'Aug';
                                        else if ($i == 9)
                                            $m = 'Sep';
                                        else if ($i == 10)
                                            $m = 'Oct';
                                        else if ($i == 11)
                                            $m = 'Nov';
                                        else if ($i == 12)
                                            $m = 'Dec';
                                        ?>
                                        <option value="<?php echo $i; ?>"
                                                <?php if ($month == $i) echo 'selected="selected"'; ?>>
                                                    <?php echo $m; ?>
                                        </option>
                                        <?php
                                    endfor;
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="year" class="form-control">
                                    <?php for ($i = 2020; $i >= 2010; $i--): ?>
                                        <option value="<?php echo $i; ?>"
                                                <?php if (isset($year) && $year == $i) echo 'selected="selected"'; ?>>
                                                <?php echo $i; ?>
                                        </option>
                                    <?php endfor; ?>
                                </select>
                            </td>
                            <td>
                                <select name="course_id"  onchange="show_students(this.value)" class="form-control">
                                    <option value="">Select a class</option>
                                <?php
                                $courses = $this->db->get('course')->result_array();
                                foreach ($courses as $row):
                                    ?>
                                        <option value="<?php echo $row['course_id']; ?>"
    <?php if (isset($course_id) && $course_id == $row['course_id']) echo 'selected="selected"'; ?>>
    <?php echo $row['name']; ?>
                                        </option>
<?php endforeach; ?>
                                </select>

                            </td>
                            
                            <td>
                                <!-----SELECT STUDENTS ACCORDING TO SELECTED CLASS-------->
                                <?php
                                $courses = $this->crud_model->get_courses();
                                foreach ($courses as $row):
                                    ?>

                                    <select name="<?php if ($course_id == $row['course_id']) echo 'student_id';
                                    else echo 'temp'; ?>" 
                                            id="student_id_<?php echo $row['course_id']; ?>" 
                                            style="display:<?php if ($course_id == $row['course_id']) echo 'block';
                                    else echo 'none'; ?>;" class="form-control"  style="float:left;">

                                        <option value="">Student of class <?php echo $row['name']; ?></option>

    <?php
    $students = $this->crud_model->get_students($row['course_id']);
    foreach ($students as $row2):
        ?>
                                            <option value="<?php echo $row2['student_id']; ?>"
        <?php if (isset($student_id) && $student_id == $row2['student_id'])
            echo 'selected="selected"';
        ?>><?php echo $row2['name']; ?>
                                            </option>
                <?php endforeach; ?>


                                    </select> 
<?php endforeach; ?>


                                <select name="temp" id="student_id_0" 
                                        style="display:<?php if (isset($student_id) && $student_id > 0) echo 'none';
else echo 'block'; ?>;" class="form-control" style="float:left;">
                                    <option value="">Select a class first</option>
                                </select>
                            </td>
                            <td align="center"><input type="submit" value="<?php echo get_phrase('check_report'); ?>" class="btn btn-info"/></td>
                        </tr>
                    </form>
                    </tbody>
                </table>

            </center>


            <br /><br />



                    <?php if ($month != '' && $year != '' && $course_id != '' && $student_id != ''): ?>
                <hr/>  
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            <th><div>#</div></th>
                    <th><div><?php echo get_phrase('title'); ?></div></th>
                    <th><div><?php echo get_phrase('description'); ?></div></th>
                    <th><div><?php echo get_phrase('type'); ?></div></th>
                    <th><div><?php echo get_phrase('method'); ?></div></th>
                    <th><div><?php echo get_phrase('amount'); ?></div></th>
                    <th><div><?php echo get_phrase('date'); ?></div></th>
                    </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>Dues</td>
                    <td></td>
                    <td></td>
                    <td></td>
                        <td></td>
                    <td><?php
                        //	$length = $this->db->count_all('payment');
                        // $this->db->where('payment_type', 'income');
                        $this->db->order_by('creation_timestamp', 'desc');
                        $result = $this->db->get('invoice')->result_array();
                        $total = 0;
                        foreach ($result as $row):
                            if ((date('n', $row['creation_timestamp']) == $month) && (date('Y', $row['creation_timestamp']) == $year) && ($row['student_id'] == $student_id)):
                                $total = $total + $row['due'];
                               endif;
                        endforeach;

                        echo $total;
                        ?></td>
                    <td></td>
                
                        </tr>
                        <tr>
                            <td>Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                        <td></td>
                    <td><?php
                        //	$length = $this->db->count_all('payment');
                        $this->db->where('payment_type', 'income');
                        $this->db->order_by('timestamp', 'desc');
                        $result = $this->db->get('payment')->result_array();
                        $total = 0;
                        foreach ($result as $row):
                            if ((date('n', $row['timestamp']) == $month) && (date('Y', $row['timestamp']) == $year) && ($row['student_id'] == $student_id)):
                                $total = $total + $row['amount'];
                               endif;
                        endforeach;

                        echo $total;
                        ?></td>
                    <td><?php echo date('d,n,Y');?></td>
                
                        </tr>
                    
                    </tfoot>
                    <tbody>
    <?php
    $count = 1;
    $this->db->where('payment_type', 'income');
    $this->db->order_by('timestamp', 'desc');
    $expenses = $this->db->get('payment')->result_array();
    foreach ($expenses as $row):
        if ((date('n', $row['timestamp']) == $month) && (date('Y', $row['timestamp']) == $year) && ($row['student_id'] == $student_id)):
            ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row['title']; ?></td>
                                    <td><?php echo $row['description']; ?></td>
                                    <td><?php if ($row['income_category_id'] != 0 || $row['income_category_id'] != '') echo $this->db->get_where('income_category' , array('income_category_id' => $row['income_category_id']))->row()->name;?></td>
                                    <td>
            <?php
            if ($row['method'] == 1)
                echo get_phrase('cash');
            if ($row['method'] == 2)
                echo get_phrase('check');
            if ($row['method'] == 3)
                echo get_phrase('card');
            if ($row['method'] == 'paypal')
                echo 'paypal';
            ?>
                                    </td>
                                    <td><?php echo $row['amount']; ?></td>
                                    <td><?php echo date('d M,Y', $row['timestamp']); ?></td>
                              
                                </tr>
        <?php endif; ?>       
    <?php endforeach; ?>
                    </tbody>

                </table>



                <!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
                <script type="text/javascript">

                    jQuery(document).ready(function ($)
                    {


                        var datatable = $("#table_export").dataTable({
                            "sPaginationType": "bootstrap",
                            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
                            "oTableTools": {
                                "aButtons": [
                                    {
                                        "sExtends": "xls",
                                        "mColumns": [1, 2, 3, 4, 5]
                                    },
                                    {
                                        "sExtends": "pdf",
                                        "mColumns": [1, 2, 3, 4, 5]
                                    },
                                    {
                                        "sExtends": "print",
                                        "fnSetText": "Press 'esc' to return",
                                        "fnClick": function (nButton, oConfig) {
                                            datatable.fnSetColumnVis(0, false);

                                            this.fnPrint(true, oConfig);

                                            window.print();

                                            $(window).keyup(function (e) {
                                                if (e.which == 27) {
                                                    datatable.fnSetColumnVis(0, true);
                                                }
                                            });
                                        },
                                    },
                                ]
                            },
                        });

                        $(".dataTables_wrapper select").select2({
                            minimumResultsForSearch: -1
                        });
                    });

                </script>



<?php endif; ?>
        </div>
        <!----TABLE LISTING ENDS-->

    </div>
</div>
</div>

<script type="text/javascript">
    function show_batchs(course_id)
    {
        for (i = 0; i <= 100; i++)
        {

            try
            {
                document.getElementById('batch_id_' + i).style.display = 'none';
                document.getElementById('batch_id_' + i).setAttribute("name", "temp");
            }
            catch (err) {
            }
        }
        document.getElementById('batch_id_' + course_id).style.display = 'block';
        document.getElementById('batch_id_' + course_id).setAttribute("name", "batch_id");
    }
    function show_students(course_id)
    {
        for (i = 0; i <= 100; i++)
        {

            try
            {
                document.getElementById('student_id_' + i).style.display = 'none';
                document.getElementById('student_id_' + i).setAttribute("name", "temp");
            }
            catch (err) {
            }
        }
        document.getElementById('student_id_' + course_id).style.display = 'block';
        document.getElementById('student_id_' + course_id).setAttribute("name", "student_id");
    }

</script> 