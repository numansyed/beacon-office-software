
<div class="row">
    <div class="col-md-12">
        
       	
        <div class="gallery-env">		
            <div class="row">			
                <div class="col-sm-12">					
                    <h3>
                        Album Title
                        &nbsp;
                       
                    </h3>					
                    <hr />					
                </div>			
            </div>
            <div class="row">
           <?php         ?>
            <?php foreach ($images as $row):?> 
                
            			
                <div class="col-sm-2 col-xs-4">					
                    <article class="image-thumb">						
                        <a href="#" class="image">
                            <img src="<?php echo $this->crud_model->get_album_url('album_admin',$row['album_id'],$row['image_id']);?>" />
                        </a>						
                        <div class="image-options">
                            <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_image_edit/<?php echo $row['image_id'];?>');" class="edit"><i class="entypo-pencil"></i></a>
                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/image/delete/<?php echo $row['album_id'];?>/<?php echo $row['image_id'];?>');" class="delete"><i class="entypo-cancel"></i></a>
                        </div>						
                    </article>				
                </div>			
            
            <?php endforeach;?>
                </div>
        </div>
        <hr />		
        <h3>
            Upload More Images
            <br />
            <small>The upload script will generate random response status (error or success), files are not uploaded.</small>
        </h3>	
            
        <br />	
       
        <?php echo form_open(base_url() . 'index.php?admin/image/upload/'.$current_album_id, array('class' => 'form-horizontal form-groups-bordered validate dropzone dz-min','id'=>'dropzone_example', 'enctype' => 'multipart/form-data')); ?>
        
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        <?php echo form_close(); ?>	
       
        <div id="dze_info" class="hidden">			
            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Dropzone Uploaded Images Info</div>
                </div>				
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="40%">File name</th>
                            <th width="15%">Size</th>
                            <th width="15%">Type</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>        
    
    </div>
</div>
