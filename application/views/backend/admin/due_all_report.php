<div class="row">
	<?php
	$courses = $this->db->get('course')->result_array();
	foreach ($courses as $row3): ?>
		<div class="col-md-12">
			<h2><?php echo $this->crud_model->get_type_name_by_id('course', $row3['course_id']);?></h2>	
			<?php
			$this->db->where(array(
				'course_id' => $row3['course_id']			
			));
			$batches = $this->db->get('batch')->result_array();
			foreach ($batches as $row2): ?>
				<div class="col-md-6">
					<h3><?php echo $this->crud_model->get_type_name_by_id('batch', $row2['batch_id']); ?></h3>
					<table  class="table table-bordered" id="">
						<thead>
							<tr>

								<th><div><?= get_phrase('ID');?></div></th>
								<th><div><?php echo get_phrase('Name'); ?></div></th>
								<th><div><?php echo get_phrase('Fee'); ?></div></th>
								<th><div><?php echo get_phrase('Paid'); ?></div></th>
								<th><div><?php echo get_phrase('Due'); ?></div></th>
							</tr>
						</thead>
						<tfoot>
							<td><p class="text text-success">Total</p></td>
							<td></td>
							<td><p class="text-success"><?php echo $this->crud_model->get_total_fee_from_database($row3['course_id'],$row2['batch_id'],$student_id);?></p></td>
							<td><p class="text-success"><?php echo $this->crud_model->get_total_paid_from_database($row3['course_id'],$row2['batch_id'],$student_id);?></p></td>
							<td><p class="text-success"><?php echo $this->crud_model->get_total_due_from_database($row3['course_id'],$row2['batch_id'],$student_id);?></p></td>
						</tfoot>
						<tbody>
							<?php
							$this->db->where(array(
								'course_id' => $row3['course_id'],
								'batch_id' => $row2['batch_id']
							));
							$courses = $this->db->get('a1')->result_array();
							foreach ($courses as $row): ?>
								<tr>
									<td><?php echo $row['student_id']; ?></td>
									<td><?php echo $this->crud_model->get_type_name_by_id('student', $row['student_id']); ?></td>
									<td><?php echo $row['total_amount']?></td>
									<td><?php $paid_amount = $this->crud_model->get_individual_paid_student($row['a1_id']); echo $paid_amount?></td>
									<td><?php echo $row['total_amount'] - $paid_amount; ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			<?php endforeach;?>
		</div>
	<?php endforeach;?>
</div>


