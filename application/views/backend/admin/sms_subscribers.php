<table class="table table-bordered datatable" id="table_export">
    <thead>
    <tr>
        <th><?php echo get_phrase('id'); ?></th>
        <th><?php echo get_phrase('name'); ?></th>
        <th><?php echo get_phrase('student name'); ?></th>
        <th><?php echo get_phrase('phone'); ?></th>
        <th><?php echo get_phrase('send') . ' SMS'; ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    function isValidPhone ($_phone)
    {
        $_phone = str_replace(' ', '', $_phone);
        $_phone = str_replace('+', '', $_phone);
        if ($_phone[0] == '0' && $_phone[1] == '8' && $_phone[2] == '8') {
            $_phone = substr($_phone, 3);
        }
        if ($_phone[0] == '0' && $_phone[1] == '0') {
            $_phone = substr($_phone, 2);
        }
        if ($_phone[0] == '8' && $_phone[1] == '8') {
            $_phone = substr($_phone, 2);
        }
        $_phone = intval($_phone);
        $_valid_phone = (strlen($_phone) == 10) && (intval($_phone) > 1000000000);

        return $_valid_phone;
    }

    $count = 1;
    foreach ($parent as $row):
        ?>
        <tr>
            <td><?php echo $count++; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['phone']; ?></td>
            <td><div class="btn-group">
                    <?php
                    if (isValidPhone($row['phone'])) {
                    if(!empty($row['sub_id'])) { ?>
                        <button type="button" class="badge badge-success" onclick="edit_list(<?php echo $row['parent_id']; ?>,this)">
                            Enabled
                        </button>
                    <?php } else { ?>
                        <button type="button" class="badge badge-danger" onclick="edit_list(<?php echo $row['parent_id']; ?>,this)">
                            Disabled
                        </button>
                    <?php } } else {?>
                        <span>Invalid Phone Number</span>
                    <?php }?>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

    function edit_list(parent_id, obj) {
        var _settings = $(obj).text().toLowerCase().trim();
        var settings = 'enable';

        if (_settings == 'enabled') {
            settings = 'disable';
        }

        $.ajax({
            async: false,
            url: '<?php echo base_url() . 'index.php?admin/sms_subscribers_edit_list'; ?>',
            type: 'POST',
            data: { param0: settings, param1: parent_id },
            timeout: 4000,
            success: function(result) {
                if (result.trim() == 'ok') {
                    if (settings == 'enable') {
                        $(obj).removeClass();
                        $(obj).addClass('badge badge-success');
                        $(obj).text('Enabled');
                    } else {
                        $(obj).removeClass();
                        $(obj).addClass('badge badge-danger');
                        $(obj).text('Disabled');
                    }
                } else {
                    //redirect to login page.
                    window.location.href = result;
                }
            }
        });
    }
</script>

