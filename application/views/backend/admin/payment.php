<div class="row">
    <div class="col-md-12">
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li <?php if ($active_page == 'add') echo 'course="active"'; ?>>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('receive_payment'); ?>
                </a></li>
            <li <?php if ($active_page == 'list') echo 'course="active"'; ?>>
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('memo_list'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">
            <!----CREATION FORM STARTS---->
            <div class="tab-pane box <?php if ($active_page == 'add') echo 'active'; ?>" id="add">
                <?php echo form_open(base_url() . 'index.php?admin/payment/add', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title"><?php echo get_phrase('payment_information'); ?></div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Type</label>
                                    <div class="col-sm-10">
                                        <select id="type" name="from_account_id" class="form-control" required onchange="showparty(this.value)">
                                            <option value=""><?php echo get_phrase('select'); ?></option>
                                            <?php
                                            $this->db->select('a2.*, a3.a3_id');
                                            $this->db->from('a2');
                                            $this->db->join('a3', 'a3.a3_id=a2.a3_id');
                                            $this->db->where(array('a2.inactive' => '0', 'a3.a4_id' => '3'));
                                            $accounts = $this->db->get()->result_array();
                                            $types = $this->db->get_where('a3', array('inactive' => '0', 'a3.a4_id' => '3'))->result_array();
                                            foreach($types as $type):?>
                                                <optgroup label="<?php echo $type['name']; ?>">
                                                    <?php foreach ($accounts as $account):?>
                                                        <?php if($account['a3_id'] == $type['a3_id']): ?>
                                                            <option value="<?php echo $account['a2_id']; ?>">
                                                                <?php echo $account['a2_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $account['name']; ?>
                                                            </option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </optgroup>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="student_details" style="display: none;">
                                    <div class="form-group" id="student_course">
                                        <label class="col-sm-2 control-label">Course</label>
                                        <div class="col-sm-10">
                                            <select id="course_id" name="course_id" class="form-control" style="float:left;"  onchange="getbatches(this.value);">
                                                <option value=""><?php echo get_phrase('select_course'); ?></option>
                                                <?php
                                                $courses = $this->db->get('course')->result_array();
                                                foreach ($courses as $row):?>
                                                    <option value="<?php echo $row['course_id']; ?>">
                                                        Course <?php echo $row['name']; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Batch</label>
                                        <div class="col-sm-10">
                                            <select name="batch_id" class="form-control" id="batch_list"  onchange="getstudents(this.value);">
                                                <option value="">select batch first</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Student</label>
                                        <div class="col-sm-10">
                                            <select name="student_id" class="form-control" id="student_list">
                                                <option value="">select course first</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="exam_details" style="display: none;">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Session</label>
                                        <div class="col-sm-10">
                                            <select id="session" name="session" class="form-control" style="float:left;"
                                                    onchange="getexams(this.value);">
                                                <option value=""><?php echo get_phrase('select_session'); ?></option>
                                                <?php
                                                $year = date('Y');
                                                for ($i = -5; $i < 5; $i++):?>
                                                    <option value="<?php echo($year + $i); ?>">
                                                        <?php echo($year + $i); ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Exam</label>
                                        <div class="col-sm-10">
                                            <select id="exam_list" name="exam_id" class="form-control" style="float:left;">
                                                <option value="">Select session first</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="month_details" style="display: none;">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Month</label>
                                        <div class="col-sm-10">
                                            <select name="month" class="form-control" style="float:left;">
                                                <?php for($m=1; $m <= 12; $m++):?>
                                                    <option value="<?php echo $m; ?>" <?php echo ($m == date('m')? 'selected':''); ?>><?php echo $this->crud_model->getfullmonthname($m); ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Account</label>
                                    <div class="col-sm-6">
                                        <select name="to_account_id" class="form-control" required onchange="getbalance(this.value)">
                                            <option value="1065"><?php echo '1065&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('petty_cash'); ?></option>
                                            <option value="1060"><?php echo '1060&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('checking_account_(Bank)'); ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <span id="balance" class="form-control"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Date</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="datepicker form-control" name="date" required value="<?php echo date('m/d/Y'); ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo get_phrase('description'); ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="memo" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!----CREATION FORM ENDS-->
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title"><?php echo get_phrase('amounts'); ?></div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('total'); ?></label>
                                    <div class="col-sm-9">
                                        <input id="total_amount" type="text" class="form-control" name="total_amount"
                                               placeholder="<?php echo get_phrase('enter_total_amount'); ?>" required onchange="checkpaid(0)"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('paid_amount'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="paid_amount"
                                               placeholder="<?php echo get_phrase('enter_paid_amount'); ?>" required onchange="checkpaid(this.value)"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                                    <div class="col-sm-9">
                                        <select id="paidstatus" name="status" class="form-control" required>
                                            <option value="2"><?php echo get_phrase('unpaid'); ?></option>
                                            <option value="1"><?php echo get_phrase('paid'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('method'); ?></label>
                                    <div class="col-sm-9">
                                        <select name="method" class="form-control" required onchange="showcardcheck(this.value)">
                                            <option value="1"><?php echo get_phrase('cash'); ?></option>
                                            <option value="2"><?php echo get_phrase('check'); ?></option>
                                            <option value="3"><?php echo get_phrase('card'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="check" style="display: none;">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('check_no'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="check_number"
                                               placeholder="<?php echo get_phrase('enter_check_number'); ?>" />
                                    </div>
                                </div>
                                <div class="form-group" id="card" style="display: none;">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('card_no'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="card_number"
                                               placeholder="<?php echo get_phrase('enter_card_number'); ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('save_payment'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="tab-pane box <?php if ($active_page == 'list') echo 'active'; ?>" id="list" style="padding: 5px">
                <?php echo form_open(base_url() . 'index.php?admin/payment/filter', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                <table  class="table table-bordered datatable">
                    <tr>
                        <td><?php echo get_phrase('from_date'); ?></td>
                        <td><?php echo get_phrase('to_date'); ?></td>
                        <td><?php echo get_phrase('options'); ?></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="datepicker form-control" name="fromdate" required value="<?php echo $fromdate; ?>"/>
                        </td>
                        <td>
                            <input type="text" class="datepicker form-control" name="todate" required value="<?php echo $todate; ?>"/>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('filter'); ?></button>
                        </td>
                    </tr>
                </table>
                <?php echo form_close(); ?>
                <table  class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th><div><?php echo get_phrase('ID'); ?></div></th>
                        <th><div><?php echo get_phrase('student'); ?></div></th>
                        <th><div><?php echo get_phrase('batch'); ?></div></th>
                        <th><div><?php echo get_phrase('course'); ?></div></th>
                        <th><div><?php echo get_phrase('fees'); ?></div></th>                       
                        <th><div><?php echo get_phrase('paid'); ?></div></th>                       
                        <th><div><?php echo get_phrase('due'); ?></div></th>                       
                        <th><div><?php echo get_phrase('status'); ?></div></th>
                        <th><div><?php echo get_phrase('last_paydate'); ?></div></th>
                        <th><div><?php echo get_phrase('options'); ?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($memos as $row): ?>
                        <tr>
                            <td><?php echo $row['a1_id']; ?></td>
                            <td><?php
                                if (intval($row['student_id']) > 0) {
                                    echo $this->crud_model->get_type_name_by_id('student', $row['student_id']);
                                } else {
                                    echo 'N/A';
                                }?></td>
                            <td><?php echo $this->crud_model->get_type_name_by_id('batch', $row['batch_id']); ?></td>
                            <td><?php
                                if (intval($row['course_id']) > 0) {
                                    echo $this->crud_model->get_type_name_by_id('course', $row['course_id']);
                                } else {
                                    echo 'N/A';
                                }?></td>
                            <td>
                                <p><span class="text-bold"><?php echo number_format((float)$row['total_amount'], 2, '.', '');?></span></p>
                            </td>
                            <td>
                                <p> <span class="text-bold"><?php 
                                $paid_amount = $this->crud_model->get_paid_amount($row['a1_id']);
                               echo number_format((float)$paid_amount, 2, '.', '');?></span></p>
                           </td><td>
                                <p> <span class="text-bold"><?php echo number_format((float)(floatval($row['total_amount']) - floatval($paid_amount)), 2, '.', ''); ?></span></p>
                            </td>
                            
                            <td><span class="label label-<?php echo (intval($row['status']) == 1 ? 'success' : 'danger'); ?>"><?php echo (intval($row['status']) == 1 ? 'Paid' : 'Has Due'); ?></span></td>
                            <td><?php echo date('d M, Y', strtotime($row['date'])); ?></td>
                            <td><div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                        <?php if (intval($row['status']) == 2): ?>
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_take_payment/<?php echo $row['a1_id']; ?>');">
                                                    <i class="entypo-bookmarks"></i>
                                                    <?php echo get_phrase('take_payment'); ?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                        <?php endif; ?>
                                        <!-- VIEWING LINK -->
                                        <li>
                                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_view_invoice/<?php echo $row['a1_id']; ?>');">
                                                <i class="entypo-credit-card"></i>
                                                <?php echo get_phrase('view_memo'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <!---- SENT SMS----->
                                        <?php
                                        $active_sms_service = $this->db->get_where('settings', array(
                                            'type' => 'active_sms_service'
                                        ))->row()->description;
                                        if (strlen($active_sms_service) > 0 && $active_sms_service != 'disabled'):?>
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_invoice_sms/<?php echo $row['a1_id'];?>/student');">
                                                    <i class="entypo-mail"></i>Send Payment Confirmation SMS</a>
                                            </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>

<script type="text/javascript">
    function checkpaid(amount) {
        var _total = parseFloat($("#total_amount").val());
        if (parseFloat(amount) >= _total) {
            $("#paidstatus").val(1);
        } else {
            $("#paidstatus").val(2);
        }
    }
    function showcardcheck(method) {
        $("#check").css('display', 'none');
        $("#card").css('display', 'none');
        $('#check input[type="text"]').each(function () {
            $(this).prop('required', false);
        });
        $('#card input[type="text"]').each(function () {
            $(this).prop('required', false);
        });

        if (method == 2) {
            $("#check").css('display', 'block');
            $('#check input[type="text"]').each(function () {
                $(this).prop('required', true);
            });
        }
        if (method == 3) {
            $("#card").css('display', 'block');
            $('#card input[type="text"]').each(function () {
                $(this).prop('required', true);
            });
        }
    }
   function showparty(party) {
       if (party > 1300 && party < 1308) {
           $("#student_details").css('display', 'block');
           $('#student_details input[type="text"]').each(function () {
              $(this).prop('required', true);
           });
           $('#student_details select').each(function () {
               $(this).prop('required', true);
           });
       } else {
           $("#student_details").css('display','none');
           $('#student_details input[type="text"]').each(function () {
               $(this).prop('required', false);
           });
           $('#student_details select').each(function () {
               $(this).prop('required', false);
           });
       }
       if (party == 1301) {
           $("#month_details").css('display','block');
           $('#month_details select').each(function () {
               $(this).prop('required', true);
           });
       } else {
           $("#month_details").css('display','none');
           $('#month_details select').each(function () {
               $(this).prop('required', false);
           });
       }
       if (party == 1302) {
           $("#exam_details").css('display','block');
           $('#exam_details input[type="text"]').each(function () {
               $(this).prop('required', true);
           });
           $('#exam_details select').each(function () {
               $(this).prop('required', true);
           });
       } else {
           $("#exam_details").css('display','none');
           $('#exam_details input[type="text"]').each(function () {
               $(this).prop('required', false);
           });
           $('#exam_details select').each(function () {
               $(this).prop('required', false);
           });
       }
   }
   function getbatches(course_id) {
       $.ajax({
           url: '<?php echo base_url();?>index.php?admin/get_batch_list/' + course_id ,
           success: function(response)
           {
               $('#batch_list').html(response);
           }
       });
   }
   function getstudents(batch_id) {
       $.ajax({
           url: '<?php echo base_url();?>index.php?admin/get_student_list/' + batch_id ,
           success: function(response)
           {
               $('#student_list').html(response);
           }
       });
   }
   function getexams(session) {
       $.ajax({
           url: '<?php echo base_url();?>index.php?admin/get_exam_list/' + session ,
           success: function(response)
           {
               $('#exam_list').html(response);
           }
       });
   }
    function getbalance(a2_id) {
        $.ajax({
            url: '<?php echo base_url();?>index.php?admin/getbalance/' + a2_id ,
            success: function(response)
            {
                if (response.indexOf('-') != -1 || parseFloat(response) <= 0)
                    $('#balance').css('color', 'red');
                else
                    $('#balance').css('color', 'black');

                $('#balance').html(response);
            }
        });
    }
    jQuery(document).ready(function ($) {
        getbalance(1065);
    });
</script>