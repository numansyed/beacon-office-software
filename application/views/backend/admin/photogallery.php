<hr />  
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/album_add/');" 
    class="btn btn-primary pull-right">
        <i class="entypo-plus-circled"></i>
        <?php echo get_phrase('add_new_album');?>
    </a> 
<hr />
<br />
<div class="row">
    <div class="col-md-12">

       
        <div class="gallery-env">

            <div class="row">
<?php foreach ($albums as $row):?>
                <div class="col-sm-4">

                    <article class="album">

                        <header>

                            <a href="<?php echo base_url(); ?>index.php?admin/image/view/<?php echo $row['album_id'];?>">
                                <img src="<?php echo $this->crud_model->get_album_url('album_admin',$row['album_id'],$row['album_name']);?>" />
                            </a>

                            <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_album_edit/<?php echo $row['album_id'];?>');" class="album-options">
                                <i class="entypo-cog"></i>
                                Change Cover
                            </a>
                        </header>

                        <section class="album-info">
                            <h3><a href="#"><?php echo $row['album_name']?></a></h3>

                            <p><?php echo $row['album_description']?> </p>
                        </section>

                        <footer>
                            <?php $countimage=count($this->db->get_where('image', array('album_id' => $row['album_id']))->result_array());?>
                            <div class="album-images-count">
                                <i class="entypo-picture"></i>
                              <?php echo $countimage;?>
                            </div>

                            <div class="album-options">
                                <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_album_edit/<?php echo $row['album_id'];?>');">
                                    <i class="entypo-cog"></i>
                                </a>

                                <a href="<?php echo base_url(); ?>index.php?admin/photogallery/delete/<?php echo $row['album_id'];?>">
                                    <i class="entypo-trash"></i>
                                </a>
                            </div>

                        </footer>

                    </article>

                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>