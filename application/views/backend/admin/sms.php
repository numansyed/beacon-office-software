<div class="row">
    <div class="col-md-12">    
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('sms_list'); ?>
                </a></li>
            </ul>
            <!------CONTROL TABS END------>

            <div class="tab-content">
                <!----TABLE LISTING STARTS-->
                <div class="tab-pane box active" id="list">				
                    <table class="table table-bordered datatable" id="table_export">
                        <thead>
                            <tr>
                                <th><div><?php echo get_phrase('Name'); ?></div></th>
                                <th><div><?php echo get_phrase('Receiver_type'); ?></div></th>
                                <th><div><?php echo get_phrase('Message'); ?></div></th>
                                <th><div><?php echo get_phrase('Date'); ?></div></th>
                                <th><div><?php echo get_phrase('options'); ?></div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $type= 0;
                            foreach ($smss as $row): ?>
                                <tr>
                                    <td>
                                        <?php
                                        if(($row['parent_id'] == 0) && ($row['teacher_id'] == 0)){ 
                                            $students = explode(',', $row['student_id']);
                                            if($students[0] == 'TS '){
                                               echo $this->db->get_where('tentative_student', array('tentative_student_id' => $students[1] ))->row()->student_name;
                                               $type = "tentative_student";

                                           }
                                           else{
                                            echo $this->db->get_where('student', array('student_id' => $row['student_id'] ))->row()->name;
                                            $type = 'student';
                                        }
                                    }
                                    elseif(($row['student_id'] == 0) && ($row['teacher_id'] == 0)){ 
                                        echo $this->db->get_where('parent', array('parent_id' => $row['parent_id'] ))->row()->name;
                                        $type = 'parent';
                                    }
                                    elseif(($row['student_id'] == 0)&& ($row['parent_id'] == 0)){ 
                                        echo $this->db->get_where('teacher', array('teacher_id' => $row['teacher_id'] ))->row()->name;
                                        $type = 'teacher';
                                    } 
                                    ?>
                                </td>
                                <td><?php echo $type;?></td>
                                <td><?php echo $row['message']; ?></td>
                                <td><?php echo date('d M,Y', $row['timestamp']);?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm ">
                                            <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/sms/delete/<?php echo $row['sms_id']; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </button>

                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


        </div>
    </div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {


        var datatable = $("#table_export").dataTable();

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>