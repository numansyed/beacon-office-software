<?php
    if (!strstr($fdate, '/')) {
        $fdate = date('m/d/Y', $fdate);
        $tdate = date('m/d/Y', $tdate);
    }
?>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th>From Date</th>
        <th>To Date</th>
        <th>Type</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    <form method="post" action="<?php echo base_url(); ?>index.php?admin/attendancereport_selector" class="form">
        <tr class="gradeA">
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="fdate" value="<?php echo $fdate; ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="tdate" value="<?php echo $tdate; ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <select name="type"  class="form-control" onchange="show_courses(this.value)">
                            <option value="student" <?php if ($type == 'student') echo 'selected="selected"'; ?>>Student</option>
                            <option value="employee" <?php if ($type == 'employee') echo 'selected="selected"'; ?>>Employee</option>
                        </select>
                    </div>
                    <div class="col-sm-12" id="courses" style="visibility: <?php if ($type == 'student') {echo 'visible';} else {echo 'hidden';} ?>">
                    <select name="course_id"  class="form-control">
                    <option value="all" <?php if (isset($course_id) && $course_id == 'all') echo 'selected="selected"'; ?>>All</option>
                    <?php
                    $courses = $this->db->get('course')->result_array();
                    foreach ($courses as $row):
                    ?>
                        <option value="<?php echo $row['course_id']; ?>"
                            <?php if (isset($course_id) && $course_id == $row['course_id']) echo 'selected="selected"'; ?>>
                            <?php echo $row['name']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                </div>
                </div>
            </td>
            <td><input type="submit" value="<?php echo get_phrase('View'); ?>" class="btn btn-info"/></td>
        </tr>
    </form>
    </tbody>
</table>
<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <?php if($type == 'student'): ?>
            <th width="80"><div>Roll</div></th>
            <?php endif; ?>
            <th width="80"><div>ID</div></th>
            <th><div><?php echo get_phrase('name'); ?></div></th>
            <?php if($type == 'student'): ?>
                <th width="80"><div><?php echo get_phrase('phone'); ?></div></th>
            <?php endif; ?>
            <th><div><?php echo get_phrase('total_working_days'); ?></div></th>
            <th><div><?php echo get_phrase('total_present'); ?></div></th>
            <th><div><?php echo get_phrase('total_absent'); ?></div></th>
            <th><div><?php echo get_phrase('percentage'); ?></div></th>
</tr>
</thead>
<tbody>
    <?php
	foreach ($data as $row):
        ?>
        <tr>
            <?php if($type == 'student'): ?>
                <td><?php echo $row['roll']; ?></td>
            <?php endif; ?>
            <td><?php if($type == 'employee') {echo $row['teacher_id'];}else {echo $row['student_id'];} ?></td>
            <td><?php echo $row['name']; ?></td>
            <?php if($type == 'student'): ?>
            <td><?php echo $this->crud_model->get_type_name_by_id('parent', $row['parent_id'], 'phone'); ?></td>
            <?php endif; ?>
            <td align="center"><?php echo $workingdays; ?></td>
            <?php
                $_fdate = explode('/',$fdate);
                $_tdate = explode('/',$tdate);
                $_sfdate = $_fdate[2] . '-' . $_fdate[0] . '-' . $_fdate[1];
                $_stdate = $_tdate[2] . '-' . $_tdate[0] . '-' . $_tdate[1];
                $_present = 0;

                if ($type == 'student') {
                    $_querystring = "SELECT DISTINCT date FROM attendance WHERE student_id = " . $row['student_id'] . " AND status = 1 AND date >= '$_sfdate' AND date <= '$_stdate'";
                    $_present = count($this->db->query($_querystring)->result_array());
                } else if ($type == 'employee') {
                    $_querystring = "SELECT DISTINCT date FROM employee_attendance WHERE employee_id = " . $row['teacher_id'] . " AND status = 1 AND date >= '$_sfdate' AND date <= '$_stdate'";
                    $_present = count($this->db->query($_querystring)->result_array());
                }
            ?>
            <td align="center"><?php echo $_present; ?></td>
            <td align="center"><?php echo ($workingdays - $_present); ?></td>
            <td style="text-align: center;">
                <?php
                $percentage_attendance =  round((float)(($_present * 100)/ (int)$workingdays), 2);
                if (!stristr($percentage_attendance, 'N')) {echo $percentage_attendance . '%';} else {echo 'N/A';};
                ?>
            </td>
        </tr>
<?php endforeach; ?>
</tbody>
</table>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3, 4<?php if ($type == 'student') echo ', 5, 6'; ?>]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3, 4<?php if ($type == 'student') echo ', 5, 6'; ?>]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            //datatable.fnSetColumnVis(3, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    //datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
    function show_courses(type) {
        try {
            document.getElementById('courses').style.visibility = 'hidden';
            //document.getElementById('student_id_' + i).setAttribute("name", "temp");
        }
        catch (err) {
        }

        if (type == 'student') {
            document.getElementById('courses').style.visibility = 'visible';
            //document.getElementById('courses').setAttribute("name", "student_id");
        }
    }
</script>