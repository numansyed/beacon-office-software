<style>
    #head {display: none;}
</style>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th>From Date</th>
        <th>To Date</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    <?php echo form_open(base_url() . 'index.php?admin/income_expense_report', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
        <tr class="gradeA">
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="fromdate" value="<?php echo date('m/d/Y', $fromdate); ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="todate" value="<?php echo date('m/d/Y', $todate); ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td><input type="submit" value="<?php echo get_phrase('view'); ?>" class="btn btn-info"/></td>
        </tr>
    <?php echo form_close(); ?>
    </tbody>
</table>
<?php if (count($income) > 0 || count($expense) > 0):?>
<table width="100%" border="0" class="table table-bordered">
    <tr>
        <td align="center">
            <a id="download" href="#" target="_blank"
               class="btn btn-info btn-sm"><i class="entypo-download"></i> Download Report</a>
        </td>
    </tr>
</table>
<center>
    <div style="border: 1px solid black; color: #000000; background-color: #ffffff;">
        <div id="head">
    <table id="heading" border="0" cellpadding="0" cellspacing="0" style="border: hidden;" width="99%">
        <tr>
            <td align="center"><img src="uploads/logo.png"
                                    class="img-responsive"
                                    style="max-height:35px"/></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 14pt;"><strong><?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?></strong></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 12pt;"><strong>Income/Expense Report</strong></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 10pt;"><strong><?php echo '(' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')'; ?></strong></td>
        </tr>
    </table>
        </div>
        <div id="data">
<table class="table table-bordered datatable" id="datatable" border="1" cellpadding="2" cellspacing="1" style="border-collapse: collapse;color: #000000;" width="99%">
    <thead>
    <tr>
        <th colspan="5" align="center">Income</th>
        <th>&nbsp;</th>
        <th colspan="5" align="center">Expense</th>
    </tr>
    <tr>
            <th><div>ID</div></th>
            <th><div><?php echo get_phrase('date'); ?></div></th>
            <th><div><?php echo get_phrase('account'); ?></div></th>
            <th><div><?php echo get_phrase('method'); ?></div></th>
            <th><div><?php echo get_phrase('amount'); ?></div></th>
            <th>&nbsp;</th>
            <th><div>ID</div></th>
            <th><div><?php echo get_phrase('date'); ?></div></th>
            <th><div><?php echo get_phrase('account'); ?></div></th>
            <th><div><?php echo get_phrase('method'); ?></div></th>
            <th><div><?php echo get_phrase('amount'); ?></div></th>
        </tr>
    </thead>
<tbody>
    <?php
    $total_payment = 0;
    $total_expense = 0;
    $length = count($income) > count($expense)? count($income):count($expense);
    $line = false;
    for ($i=0; $i<$length; $i++):?>
        <tr>
            <?php if ($i < count($income)) {?>
                <td><?php echo $income[$i]['a8_id']; ?></td>
                <td><?php echo date('m/d/Y', strtotime($income[$i]['date'])); ?></td>
                <td><?php echo $this->crud_model->get_type_name_by_id('a1', $income[$i]['a1_id'], 'a2_id') . '&nbsp;&nbsp;' . $this->crud_model->get_type_name_by_id('a2', $this->crud_model->get_type_name_by_id('a1', $income[$i]['a1_id'], 'a2_id')); ?></td>
                <td><?php echo $this->crud_model->getmethodname($income[$i]['method']); ?></td>
                <td align="right"><?php echo $this->crud_model->money($income[$i]['amount']); ?></td>
                <?php $total_payment += $income[$i]['amount'];?>
            <?php } else {?>
                <td colspan="5">&nbsp;</td>
            <?php }?>
            <?php if (!$line):
                $line = true;?>
                <td rowspan="<?php echo $length; ?>">&nbsp;</td>
            <?php endif; ?>
            <?php if ($i < count($expense)) {?>
                <td><?php echo $expense[$i]['a5_id']; ?></td>
                <td><?php echo date('m/d/Y', strtotime($expense[$i]['date'])); ?></td>
                <td><?php echo $this->crud_model->get_type_name_by_id('a1', $expense[$i]['a1_id'], 'a2_id') . '&nbsp;&nbsp;' . $this->crud_model->get_type_name_by_id('a2', $this->crud_model->get_type_name_by_id('a1', $expense[$i]['a1_id'], 'a2_id')); ?></td>
                <td><?php echo $this->crud_model->getmethodname($expense[$i]['method']); ?></td>
                <td align="right"><?php echo $this->crud_model->money($expense[$i]['amount']); ?></td>
                <?php $total_expense += $expense[$i]['amount']; ?>
            <?php } else {?>
                <td colspan="5">&nbsp;</td>
            <?php }?>
        </tr>
        <?php
    endfor;
    ?>
    <tr>
        <td colspan="4"><strong>Total</strong></td>
        <td align="right"><strong><?php echo $this->crud_model->money($total_payment); ?></strong></td>
        <td>&nbsp;</td>
        <td colspan="4"><strong>Total</strong></td>
        <td align="right"><strong><?php echo $this->crud_model->money($total_expense); ?></strong></td>
    </tr>
    <tr>
        <td colspan="10"><strong>Balance</strong></td>
        <td align="right"><strong><?php echo $this->crud_model->money(($total_payment - $total_expense)); ?></strong></td>
    </tr>
</tbody>
</table>
        </div>
        </div>
</center>
<?php endif; ?>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    $("#download").click(function (e) {
        e.preventDefault();

        var pdfbody = $("#data").html();
        var pdfheader = $("#head").html();
        var pdffooter = '<table width="99%" border="0" style="font-family:sans-serif; font-size:8pt; font-weight:bold; color:#999999;"><tr><td align="right">{PAGENO}</td></tr></table>';

        var styledata = '<style media="print">' +
            '#heading {width: 99%; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;}' +
            '#datatable {width: 8.5in; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important; border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '</style>';

        $.ajax({
            async: false,
            url: '<?php echo base_url() . 'index.php?admin/getreportpdf'; ?>',
            type: 'POST',
            data: {
                header: pdfheader,
                body: pdfbody,
                footer: pdffooter,
                style: styledata,
                pagesize: 'A4',
                filename: 'Income_Expense_Report__<?php echo date('m-d-Y', $fromdate) . '__' . date('m-d-Y', $todate); ?>'
            },
            timeout: 10000,
            success: function (result) {
                if (result.length > 0) {
                    var link = $("#download");
                    link.attr("href", result);
                    window.open(link.attr("href"));
                }
            }
        });
    });
</script>