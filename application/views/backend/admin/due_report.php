<div class="row">
    <div class="col-md-12">
        <hr>
        <br>
     <div class="col-md-12">
        <button class="btn btn-danger" onclick="get_all_due_report();">Search all due report till today</button>
    </div>
    <br>
    <!----TABLE LISTING STARTS-->
    <div class="row" id="list">

        <form id="search_form" action="<?= base_url()?>index.php?admin/ajax_due_report">
            <div class="col-md-12">
             <div class="col-md-4"  id="courses">
                <select name="course_id"  class="form-control" id="courseId" required="required" onchange="return get_batch_name(this.value)">
                    <option value="" >Select Course</option>
                    <?php
                    $courses = $this->db->get('course')->result_array();
                    foreach ($courses as $row):
                        ?>
                        <option value="<?php echo $row['course_id']; ?>"
                            <?php if (isset($course_id) && $course_id == $row['course_id']) echo 'selected="selected"'; ?>>
                            <?php echo $row['name']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-4"  id="batches">
                <select name="batch_id" class="form-control" required  id ="batch_selector_holder" onchange="return get_student_list(this.value)">
                    <option value="">Select Course First</option>
                </select>
            </div>
            <div class="col-md-4" id="students">
                <select name="student_id" class="form-control" id ="student_selector_holder">
                    <option value="">Select Batch First</option>
                </select>
            </div>
        </div>
        <br>
        <br>
        <br>

        <div class="col-md-12">
            <button class="btn btn-success pull-right" onclick="get_due_report(event)">Search</button>
        </div>

    </form>
</div>

<br>
<br>
<hr>
<div class="col-md-12" id="report_list">

</div>




</div>
<!----TABLE LISTING ENDS-->

</div>

<script type="text/javascript">

    function get_batch_name(course_id) {
        $.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_batch_list/' + course_id ,
            success: function(response)
            {
                jQuery('#batch_selector_holder').html(response);

            }
        });

    }
    function get_student_list(batch_id) {

        $.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_student_list/' + batch_id ,
            success: function(response)
            {
                jQuery('#student_selector_holder').html(response);
            }
        });

    }

    function get_due_report(ev){
        ev.preventDefault();
        var url = $("#search_form").attr('action');
        var courseID = $("#courseId").val();
        if(courseID == ""){
            alert('Please Select Course');
            return false;
        }
        var batchID = $("#batch_selector_holder").val();
        if(batchID == ""){
            alert('Please Select Batch');
            return false;
        }
        var studentID = $("#student_selector_holder").val();
    // alert(url+'/'+courseID+'/'+batchID);
    // return false;
    $("#report_list").html("");
    $("#report_list").load(url+'/'+courseID+'/'+batchID, {student_id: studentID});

    // alert(url);

} 
var base_url = "<?php  echo base_url();?>";
function get_all_due_report(){

    var all_report = "all";
    $("#report_list").html("");
    $("#report_list").load(base_url+'index.php?admin/ajax_due_report1/'+all_report);

}
</script> 