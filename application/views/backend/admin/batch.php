<hr />
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/batch_add/');" 
   class="btn btn-primary pull-right">
   <i class="entypo-plus-circled"></i>
   <?php echo get_phrase('add_new_batch'); ?>
</a> 
<br><br><br> 
<div class="row">
    <div class="col-md-12">
        <div class="tabs-vertical-env"  id="batch_list">
            <ul class="nav tabs-vertical" >
                <?php
                $courses = $this->db->get('course')->result_array();
                foreach ($courses as $row):
                    ?>
                    <li class="<?php if ($row['course_id'] == $course_id) echo 'active'; ?>">
                        <a href="#" onclick="show_batch_list_page(<?php echo $row['course_id']; ?>)">
                            <i class="entypo-dot"></i>
                            <?php echo get_phrase('course'); ?> <?php echo $row['name']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <table class="table table-bordered responsive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('batch_name'); ?></th>
                                <th><?php echo get_phrase('status'); ?></th>
                                <th><?php echo get_phrase('trainer'); ?></th>
                                <th><?php echo get_phrase('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $batchs = $this->db->get_where('batch', array(
                                'course_id' => $course_id
                            ))->result_array();
                            foreach ($batchs as $row):
                                ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><span class="label label-<?php echo (intval($row['status']) == 1 ? 'success' : 'danger'); ?>"><?php echo (intval($row['status']) == 1 ? 'Active' : 'Inactive'); ?></span></td>
                                    
                                    <td>
                                        <?php
                                        if ($row['teacher_id'] != '')
                                            echo $this->db->get_where('teacher', array('teacher_id' => $row['teacher_id']))->row()->name;
                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/batch_edit/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        <?php echo get_phrase('edit'); ?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <!--  SEND SMS BATCH WISE-->
                                                <!---- SEND SMS LINK----->
                                                <?php
                                                $active_sms_service = $this->db->get_where('settings', array(
                                                    'type' => 'active_sms_service'
                                                ))->row()->description;
                                                if (strlen($active_sms_service) > 0 && $active_sms_service != 'disabled'):?>
                                                    <li>
                                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_sms_batch/<?php echo $row['batch_id'];?>');">
                                                            <i class="entypo-mail"></i>
                                                            <?php echo get_phrase('Send_SMS');?>
                                                        </a>
                                                    </li>
                                                <?php endif;?>
                                                <li class="divider"></li>
                                                <!-- Activate or Deactivate Batch -->
                                                <li>
                                                    <?php if($row['status'] == 0){
                                                        ?>
                                                        <a href="#" onclick="confirm_modal1('<?php echo base_url(); ?>index.php?admin/batchs/update/<?php echo $row['batch_id']; ?>');">
                                                            <i class="entypo-bookmark"></i>
                                                            <?php echo get_phrase('activate'); ?>
                                                        </a>
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                        <a href="#" onclick="confirm_modal1('<?php echo base_url(); ?>index.php?admin/batchs/update/<?php echo $row['batch_id']; ?>');">
                                                            <i class="entypo-bookmark"></i>
                                                            <?php echo get_phrase('Deactivate'); ?>
                                                        </a>
                                                        <?php 
                                                    }?>
                                                </li>
                                                <li class="divider"></li>
                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/batchs/delete/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        <?php echo get_phrase('delete'); ?>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>	
    </div>
</div>
<script type="text/javascript">
    var base_url = "<?= base_url()?>";
    function show_batch_list_page(course_id){
        $("#batch_list").html("");
        $("#batch_list").load(base_url+'index.php?admin/batch_list/'+course_id);
    }
</script>