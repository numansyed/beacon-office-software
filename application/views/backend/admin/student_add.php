<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading"> 
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('addmission_form'); ?>
                </div>
            </div>
            <div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/student/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('name'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php if(isset($student_name)){ echo $student_name;}?>" autofocus>
                        </div>
                    </div>

<!--                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Admission_Type'); ?></label>
                        <div class="col-sm-10">
                            <select name="admission_type_id" class="form-control" data-validate="required" id="admission_type_id"
                                    data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $admission_type = $this->db->get('admission_type')->result_array();
                                foreach ($admission_type as $row):
                                    ?>
                                    <option value="<?php echo $row['id']; ?>">
                                        <?php echo $row['data']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('parent'); ?></label>
                        <div class="col-sm-10">
                            <select name="parent_id" class="form-control" id="parent_id"  onchange="return no_parents(this.value)">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $parents = $this->db->get('parent')->result_array();
                                foreach ($parents as $row):
                                    ?>
                                    <option value="<?php echo $row['parent_id']; ?>">
                                        <?php echo $row['name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                    </div>-->

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('course'); ?></label>
                        <div class="col-sm-10">
                            <select name="course_id" class="form-control" data-validate="required" id="course_id"
                            data-message-required="<?php echo get_phrase('value_required'); ?>"
                            onchange="return get_course_batchs(this.value)">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $courses = $this->db->get('course')->result_array();
                            foreach ($courses as $row):
                                ?>
                                <option value="<?php echo $row['course_id']; ?>">
                                    <?php echo $row['name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
<!--                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('department'); ?></label>
                        <div class="col-sm-10">
                            <select name="department_id" class="form-control">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $departments = $this->db->get('department')->result_array();
                                foreach ($departments as $row):
                                    ?>
                                    <option value="<?php echo $row['department_id']; ?>">
                                        <?php echo $row['name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('batch'); ?></label>
                        <div class="col-sm-10">
                            <select name="batch_id" class="form-control" id="batch_selector_holder">
                                <option value=""><?php echo get_phrase('select_course_first'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('roll'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="roll" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('birthday'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('admission_date'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker" name="admission_date" value="" data-start-view="3">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('gender'); ?></label>
                        <div class="col-sm-10">
                            <select name="sex" class="form-control">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <option value="male"><?php echo get_phrase('male'); ?></option>
                                <option value="female"><?php echo get_phrase('female'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('address'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="address" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Area'); ?></label>
                        <div class="col-sm-10">
                            <select name="m_area" class="form-control " data-validate="required" id="mArea"
                            data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $m_areas = $this->db->get('m_area')->result_array();
                            foreach ($m_areas as $row5):
                                ?>
                                <option value="<?php echo $row5['name']; ?>">
                                    <?php echo $row5['name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('father name'); ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="father_name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('mother name'); ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="mother_name" value="">
                    </div>
                </div>
<!--                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('father profession'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="father_profession" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('mother profession'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mother_profession" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('father monthly income'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="father_monthly_income" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('mother monthly income'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mother_monthly_income" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('father phone'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="father_phone" placeholder="insert with leading 88 only e.g 8801234567891" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('mother phone'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mother_phone" placeholder="insert with leading 88 only e.g 8801234567891" value="" >
                        </div>
                    </div>-->

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('phone'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="phone" placeholder="insert with leading 88 only e.g 8801234567891" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('email'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="email" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('father email'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="father_email" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('mother email'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mother_email" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Dormitory'); ?></label>
                        <div class="col-sm-10">
                            <select name="dormitory_id" class="form-control" id="admission_id">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $dormitory = $this->db->get('dormitory')->result_array();
                                foreach ($dormitory as $row):
                                    ?>
                                    <option value="<?php echo $row['dormitory_id']; ?>">
                                        <?php echo $row['name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('Dormitory Room Number'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="dormitory_room_number" value="" placeholder="leave blank if not applicable">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Transport'); ?></label>
                        <div class="col-sm-10">
                            <select name="transport_id" class="form-control" id="transport_id">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $transport = $this->db->get('transport')->result_array();
                                foreach ($transport as $row):
                                    ?>
                                    <option value="<?php echo $row['transport_id']; ?>">
                                        <?php echo $row['route_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>                    -->
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('password'); ?></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('photo'); ?></label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                    <img src="http://placehold.it/200x200" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="userfile" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
<!--                <div class="col-md-6" id="no_parents">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('parent_name'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="parent_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('Parent_Profession'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="parent_profession"  value="" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Address'); ?></label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="parent_address" value="" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('parent_Phone'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="parent_phone" placeholder="insert with leading 88 only e.g 8801234567891" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('parent_Email'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="parent_email" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Parent_password'); ?></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="parent_password" value="" >
                        </div>
                    </div>        

                </div>-->
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-10">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('admit_student'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function get_course_batchs(course_id) {
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
            success: function (response)
            {
                jQuery('#batch_selector_holder').html(response);
            }
        });
    }
    function no_parents(parent_id) {
        if (parent_id == false) {
            $("#no_parents").show();
        }
        else {
            $("#no_parents").hide();
        }

    }
</script>