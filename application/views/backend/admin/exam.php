<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('exam_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_exam');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <?php echo form_open(base_url() . 'index.php?admin/exam', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr><td>
                        <select id="session" name="session" class="form-control"  style="float:left;">
                            <option value=""><?php echo get_phrase('select_session');?></option>
                            <?php
                            $year = date('Y');
                            for($i=-5; $i < 5; $i++):?>
                                <option value="<?php echo ($year + $i); ?>"
                                    <?php if(($year + $i) == $session)echo 'selected';?>>
                                    <?php echo ($year + $i); ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>
                        <td>

                    <button type="submit" class="btn btn-primary"><?php echo get_phrase('get_exams'); ?></button>
                    </td>
                    </tr>

                </table>
                <?php echo form_close(); ?>
                <table  class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('exam_name');?></div></th>
                            <th><div><?php echo get_phrase('session');?></div></th>
                            <th><div><?php echo get_phrase('course_start');?></div></th>
                            <th><div><?php echo get_phrase('course_end');?></div></th>
                            <th><div><?php echo get_phrase('working_days');?></div></th>
                    		<th><div><?php echo get_phrase('exam_date');?></div></th>
                            <th><div><?php echo get_phrase('type');?></div></th>
                    		<th><div><?php echo get_phrase('comment');?></div></th>
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($exams as $row):?>
                        <tr>
							<td><?php echo $row['name'];?></td>
                            <td><?php echo $row['session'];?></td>
                            <td><?php echo $row['startdate'];?></td>
                            <td><?php echo $row['enddate'];?></td>
                            <td><?php echo $row['workingdays'];?></td>
                            <td><?php echo $row['date'];?></td>
                            <td><?php
                                if (intval($row['type']) == 1)
                                    echo 'First of 3-Part Exam';
                                else if (intval($row['type']) == 2)
                                    echo 'Second of 3-Part Exam';
                                else if (intval($row['type']) == 3)
                                    echo 'Final of 3-Part Exam';
                                else
                                    echo 'General';
                                ?></td>
							<td><?php echo $row['comment'];?></td>
							<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_exam/<?php echo $row['exam_id'];?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/exam/delete/<?php echo $row['exam_id'];?>');">
                                            <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(base_url() . 'index.php?admin/exam/create' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('session');?></label>
                                <div class="col-sm-5">
                                    <select  name="session" class="form-control"  style="float:left;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                        <?php
                                        $year = date('Y');
                                        for($i=-5; $i < 5; $i++):?>
                                            <option value="<?php echo ($year + $i); ?>"
                                                <?php if(($year + $i) == $year)echo 'selected';?>>
                                                <?php echo ($year + $i); ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('date');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="datepicker form-control" name="date" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('course_start_date');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="datepicker form-control" name="startdate" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('course_end_date');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="datepicker form-control" name="enddate" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('total_working_days');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="workingdays" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('type');?></label>
                                <div class="col-sm-5">
                                    <select  name="type" class="form-control"  style="float:left;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                        <option value="0">General</option>
                                        <option value="1">First of 3-Part Exam</option>
                                        <option value="2">Second of 3-Part Exam</option>
                                        <option value="3">Final of 3-Part Exam</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="comment"/>
                                </div>
                            </div>
                        		<div class="form-group">
                              	<div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo get_phrase('add_exam');?></button>
                              	</div>
								</div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS-->
            
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3,4,5,6,7]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3,4,5,6,7]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(8, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(8, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
		
</script>