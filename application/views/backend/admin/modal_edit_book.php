<?php 
$edit_data		=	$this->db->get_where('book' , array('book_id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
        <?php echo form_open(base_url() . 'index.php?admin/book/do_update/'.$row['book_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo 'ISBN';?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="isbn" value="<?php echo $row['isbn'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('author');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="author" value="<?php echo $row['author'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('publisher');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="publisher" value="<?php echo $row['publisher'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="description" value="<?php echo $row['description'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('price');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="price" value="<?php echo $row['price'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('course');?></label>
                    <div class="col-sm-5">
                        <select name="course_id" class="form-control">
                            <?php 
                            $courses = $this->db->get('course')->result_array();
                            foreach($courses as $row2):
                            ?>
                                <option value="<?php echo $row2['course_id'];?>"
                                    <?php if($row['course_id']==$row2['course_id'])echo 'selected';?>><?php echo $row2['name'];?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('category');?></label>
                    <div class="col-sm-5">
                        <select name="book_category_id" class="form-control">
                            <?php
                            $book_category = $this->db->get('book_category')->result_array();
                            foreach($book_category as $row3):
                                ?>
                                <option value="<?php echo $row3['book_category_id'];?>"
                                    <?php if($row['book_category_id']==$row3['book_category_id'])echo 'selected';?>><?php echo $row3['name'];?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('quantity');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="quantity" value="<?php echo $row['quantity'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('status');?></label>
                    <div class="col-sm-5">
                        <select name="locked" class="form-control">
                            <option value="0" <?php if($row['locked']==0)echo 'selected';?>><?php echo get_phrase('unlocked');?></option>
                            <option value="1" <?php if($row['locked']==1)echo 'selected';?>><?php echo get_phrase('locked');?></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-5">
                      <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_book');?></button>
                  </div>
                </div>
        </form>
        <?php endforeach;?>
    </div>
</div>