<div class="row">
    <div class="col-md-12">
        
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('principal'); ?>
                </a></li>
            <li>
                <a href="#edit" data-toggle="tab"><i class="entypo-pencil"></i>
                    <?php echo get_phrase('edit_principal'); ?></a>
            </li> 
        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
              
<br><br>
<table class="table table-bordered " id="">
    <thead>
        <tr>
            <th width="80"><div><?php echo get_phrase('photo'); ?></div></th>
            <th><div><?php echo get_phrase('name'); ?></div></th>
            <th><div><?php echo get_phrase('qualifications'); ?></div></th>
            <th><div><?php echo get_phrase('address'); ?></div></th>
            <th><div><?php echo get_phrase('message'); ?></div></th>
            <th><div><?php echo get_phrase('email'); ?></div></th>
           
      </tr>
</thead>
<tbody>
    <?php
    $principals = $this->db->get('principal')->result_array();
    foreach ($principals as $row):
        ?>
        <tr>
            <td><img src="<?php echo $this->crud_model->get_image_url('principal', $row['principal_id']); ?>" class="img-circle" width="50" /></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['qualifications']; ?></td>
            <td><?php echo $row['address']; ?></td>
            <td class="span5"><?php echo $row['message'];?></td>
            <td><?php echo $row['email']; ?></td>
        </tr>
<?php endforeach; ?>
</tbody>
</table>
 </div>
<div class="tab-pane box" id="edit" style="padding: 0px">
                <div class="box-content">
                    <?php foreach ($principals as $row): ?>
                        <?php echo form_open(base_url() . 'index.php?admin/principal/do_update/' . $row['principal_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>                       
                        <div class="form-group">
                                <label for="field-1" class="col-sm-1 control-label"><?php echo get_phrase('photo');?></label>
                                
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                            <img src="<?php echo $this->crud_model->get_image_url('principal' , $row['principal_id']);?>" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="form-group">
                                <label class="col-sm-1 control-label"><?php echo get_phrase('name');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>"/>
                                </div>
                            </div>
                    <div class="form-group">
                                <label class="col-sm-1 control-label"><?php echo get_phrase('qualifications');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="qualifications" value="<?php echo $row['qualifications'];?>"/>
                                </div>
                            </div>
                    <div class="form-group">
                                <label class="col-sm-1 control-label"><?php echo get_phrase('address');?></label>
                                <div class="col-sm-5">
                                    <textarea type="text" class="form-control" name="address"><?php echo $row['address'];?></textarea>
                                </div>
                            </div>
                   
                    <div class="form-group">
                                <label class="col-sm-1 control-label"><?php echo get_phrase('email');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>"/>
                                </div>
                            </div>
                    <div class="form-group">
                            <label class="col-sm-1 control-label"><?php echo get_phrase('Message'); ?></label>
                            <div class="col-sm-10">
                                <div class="box closable-chat-box">
                                    <div class="box-content padded">
                                        <div class="chat-message-box">
                                            <textarea name="message" id="elm2" rows="5" placeholder="<?php echo get_phrase('edit_message'); ?>" class="form-control "><?php echo $row['message']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <div class="form-group">
                                <label class="col-sm-1 control-label"><?php echo get_phrase('email');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>"/>
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('edit'); ?></button>
                            </div>
                        </div>
                        </form> 
                    <?php endforeach; ?>
                </div>                
            </div>

</div>
    </div>
</div>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function($)
    {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function(nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(3, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function(e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
