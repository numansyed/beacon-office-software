<div class="sidebar-menu">
    <header class="logo-env" >

        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="uploads/logo.png" class="img-responsive" style="max-height:35px"/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div style=""></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                <i class="entypo-gauge"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <!-- STUDENT -->
        <li class="<?php
        if ($page_name == 'student_add' ||
        $page_name == 'admissiontype' ||
        $page_name == 'tentative_student' ||
        $page_name == 'student_bulk_add' ||
        $page_name == 'student_information' ||
        $page_name == 'id_card' ||
        $page_name == 'student_promotion')
        echo 'opened active has-sub';
        ?> ">
        <a href="#">
            <i class="fa fa-group"></i>
            <span>Student</span>
        </a>
        <ul>
            <!-- STUDENT ADMISSION TYPE-->
<!--                <li class="<?php if ($page_name == 'admissiontype') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/admissiontype">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('Admission Type'); ?></span>
                    </a>
                </li>-->
                <!-- STUDENT ADMISSION -->
                <li class="<?php if ($page_name == 'tentative_student') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/tentative_student">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('tentative_student'); ?></span>
                    </a>
                </li>

                <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/student_add">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('admit_student'); ?></span>
                    </a>
                </li>

                <!-- ID CARDS -->
                <li class="<?php if ($page_name == 'id_card') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/id_card">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('certificates'); ?></span>
                    </a>
                </li>

                <!-- STUDENT ADMISSION -->
<!--                <li class="<?php if ($page_name == 'student_promotion') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/student_promotion">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('student_promotion'); ?></span>
                    </a>
                </li>-->

                <!-- STUDENT INFORMATION -->
                <li class="<?php if ($page_name == 'student_information') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('student_information'); ?></span>
                    </a>
                    <ul>
                        <?php
                        $courses = $this->db->get('course')->result_array();
                        foreach ($courses as $row):
                            ?>
                            <li class="<?php if ($page_name == 'student_information' && $course_id == $row['course_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin/student_information/<?php echo $row['course_id']; ?>">
                                    <span><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </li>
        <!-- TEACHER -->
        <li class="<?php
        if ($page_name == 'teacher' ||
        $page_name == 'posttype')
        echo 'opened active';
        ?> ">
        <a href="#">
            <i class="entypo-users"></i>
            <span>Trainers/Employee</span> 
        </a>
        <ul>
            <li class="<?php if ($page_name == 'posttype') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/posttype">
                    <span><i class="entypo-dot"></i> Post Type</span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'teacher') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/teacher">
                    <span><i class="entypo-dot"></i> Teachers/Employee</span>
                </a>
            </li>
        </ul>
    </li>

    <!-- PARENTS -->
<!--        <li class="<?php if ($page_name == 'parent') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/parent">
                <i class="entypo-user"></i>
                <span><?php echo get_phrase('parents'); ?></span>
            </a>
        </li>-->

        <!-- CLASS -->
        <li class="<?php
        if ($page_name == 'course' ||
        $page_name == 'batch' ||
        $page_name == 'department')
        echo 'opened active';
        ?> ">
        <a href="#">
            <i class="entypo-flow-tree"></i>
            <span>Courses</span>
        </a>
        <ul>
            <li class="<?php if ($page_name == 'course') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/courses">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_courses'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'batch') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/batch">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_batches'); ?></span>
                </a>
            </li>
<!--                <li class="<?php if ($page_name == 'department') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/department">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_departments'); ?></span>
                    </a>
                </li>-->
            </ul>
        </li>

<!--         SUBJECT 
        <li class="<?php if ($page_name == 'subject' || $page_name == 'subject_category') echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-docs"></i>
                <span>Subject</span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'subject_category') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/subject_category">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('subject_category'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('subject_list'); ?></span>
                    </a>
                    <ul>
                        <?php
                        $courses = $this->db->get('course')->result_array();
                        foreach ($courses as $row):
                            ?>
                            <li class="<?php if ($page_name == 'subject' && $course_id == $row['course_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin/subject/<?php echo $row['course_id']; ?>">
                                    <span>Course <?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </li>-->

        <!-- CLASS ROUTINE -->
        <li class="<?php if ($page_name == 'course_routine') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/course_routine">
                <i class="entypo-target"></i>
                <span><?php echo get_phrase('course_routine'); ?></span>
            </a>
        </li>

<!--         DAILY ATTENDANCE 
        <li class="<?php
        if ($page_name == 'daily_attendance' ||
            $page_name == 'attendance_report')
            echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-graduation-cap"></i>
                <span>Attendance</span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'daily_attendance') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/daily_attendance/student">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('daily_attendance'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'attendance_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/attendance_report">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('Attendance_Report'); ?></span>
                    </a>
                </li>
            </ul>
        </li>-->

<!--         EXAMS 
        <li class="<?php
        if ($page_name == 'exam' ||
            $page_name == 'student_marksheet' ||
            $page_name == 'marks' ||
            $page_name == 'exam_marks_sms' ||
            $page_name == 'student_tabulationsheet' ||
            $page_name == 'grade')
            echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-graduation-cap"></i>
                <span>Exam</span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'exam') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/exam">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_list'); ?> </span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'grade') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/grade">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_grades'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'marks') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/marks">
                        <span><i class="entypo-dot"></i> Manage Marks</span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'student_marksheet') echo 'opened active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/student_marksheet">
                        <span><i class="entypo-dot"></i> Mark Sheets</span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'student_tabulationsheet') echo 'opened active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/student_tabulationsheet">
                        <span><i class="entypo-dot"></i> Tabulation Sheets</span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'exam_marks_sms') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/exam_marks_sms">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('send_marks_by_SMS'); ?></span>
                    </a>
                </li>
            </ul>
        </li>-->

        <!-- ACCOUNTING -->
        <li class="<?php
        if ($page_name == 'payment' ||
        $page_name == 'expense' ||
        $page_name == 'coa' ||
        $page_name == 'income_expense_report' ||
        $page_name == 'salary_report' ||
        $page_name == 'student_payment_report' ||
        $page_name == 'journal_report' ||
        $page_name == 'gl_report' ||
        $page_name == 'balance_sheet'||
        $page_name == 'teacher_salary')
        echo 'opened active';
        ?> ">
        <a href="#">
            <i class="entypo-suitcase"></i>
            <span><?php echo get_phrase('accounts'); ?></span>
        </a>
        <ul>
            <li class="<?php if ($page_name == 'teacher_salary') echo 'opened active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/teacher_salary">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('teacher_salary'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'payment') echo 'opened active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/payment">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('receive_payment'); ?></span>
                </a>
            </li>

            <li class="<?php if ($page_name == 'expense') echo 'opened active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/expense">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('expense'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'coa') echo 'opened active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/coa">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('chart_of_accounts'); ?></span>
                </a>
            </li>
            <li class="<?php if ( $page_name == 'income_expense_report' ||
            $page_name == 'salary_report' ||
            $page_name == 'student_payment_report' ||
            $page_name == 'journal_report' ||
            $page_name == 'gl_report' ||
            $page_name == 'balance_sheet') echo 'opened active'; ?> ">
            <a href="#">
                <span><i class="entypo-dot"></i> <?php echo get_phrase('reports'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'salary_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/salary_report/<?php echo date("m/Y"); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('salary_report'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'due_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/due_report/">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('due_report'); ?></span>
                    </a>
                </li><!-- 
                <li class="<?php if ($page_name == 'student_payment_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/student_payment_report/<?php echo date("m/Y"); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('students_payment_report'); ?></span>
                    </a>
                </li> -->
                <li class="<?php if ($page_name == 'income_expense_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/income_expense_report/<?php echo strtotime(date("d M,Y")); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('income') . '/' . get_phrase('expense_report'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'balance_sheet') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/balance_sheet/<?php echo strtotime(date("m/Y")); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('balance_sheet'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'journal_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/journal_report/<?php echo strtotime(date("d M,Y")); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('view_journal'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'gl_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/gl_report/<?php echo strtotime(date("m/Y")); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('view_G/L'); ?></span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<!-- LIBRARY -->
<!--        <li class="<?php if ($page_name == 'book_category' || $page_name == 'book' || $page_name == 'book_lease') echo 'opened active'; ?> ">
            <a href="#">
                <span><i class="entypo-book"></i> <?php echo get_phrase('library'); ?></span>
            </a>
            <ul>
            <li class="<?php if ($page_name == 'book_category') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/book_category">
                    <span><i class="entypo-dot"></i> Book Category</span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/book">
                    <span><i class="entypo-dot"></i> Book List</span>
                </a>
            </li>
                <li class="<?php if ($page_name == 'book_lease') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin/book_lease">
                        <span><i class="entypo-dot"></i> Issue Books</span>
                    </a>
                </li>
            </ul>
        </li>-->

<!--         TRANSPORT 
        <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/transport">
                <i class="entypo-location"></i>
                <span>Transport</span>
            </a>
        </li>

         DORMITORY 
        <li class="<?php if ($page_name == 'dormitory') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/dormitory">
                <i class="entypo-home"></i>
                <span>Dormitory</span>
            </a>
        </li>-->

        <!-- NOTICEBOARD -->
        <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/noticeboard">
                <i class="entypo-doc-text-inv"></i>
                <span>Noticeboard</span>
            </a>
        </li>

        <!-- MESSAGE -->
        <li class="<?php if ($page_name == 'message') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/message">
                <i class="entypo-mail"></i>
                <span>Message</span>
            </a>
        </li>

        <!-- SETTINGS -->
        <li class="<?php
        if ($page_name == 'system_settings' ||
        $page_name == 'sms' ||
        $page_name == 'sms_settings' ||
        $page_name == 'sms_subscribers'
        )
        echo 'opened active';
        ?> ">
        <a href="#">
            <i class="entypo-lifebuoy"></i>
            <span>Settings</span>
        </a>
        <ul>
            <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('general_settings'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'sms_settings') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/sms_settings">
                    <span><i class="entypo-dot"></i> SMS <?php echo get_phrase('settings'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'sms') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/sms">
                    <span><i class="entypo-dot"></i> SMS <?php echo get_phrase('list'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'sms_subscribers') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/sms_subscribers">
                    <span><i class="entypo-dot"></i> SMS <?php echo get_phrase('subscribers'); ?></span>
                </a>
            </li>
        </ul>
    </li>

    <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
        <a href="<?php echo base_url(); ?>index.php?admin/manage_profile">
            <i class="entypo-lock"></i>
            <span>Profile</span>
        </a>
    </li>
    <!-- MANAGE FRONTEND  -->


</ul>

</div>