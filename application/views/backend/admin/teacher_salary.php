
<br><br>
<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <th width="80"><div><?php echo get_phrase('photo'); ?></div></th>
            <th width="80"><div><?php echo 'ID'; ?></div></th>
            <th><div><?php echo get_phrase('name'); ?></div></th>
            <th><div><?php echo get_phrase('phone'); ?></div></th>
            <th><div><?php echo get_phrase('salary'); ?></div></th>
            <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $teachers = $this->db->get('teacher')->result_array();
        foreach ($teachers as $row):
            ?>
            <tr>
                <td><img src="<?php echo $this->crud_model->get_image_url('teacher', $row['teacher_id']); ?>" class="img-circle" width="30" /></td>
                <td><?php echo $row['teacher_id']; ?></td>
                <td><?php echo $row['name']; ?></td>                
                <td><?php echo $row['phone']; ?></td>
                <td><?php
                echo $row['salary'];
                if($row['teacher_type_id'] == '2'){
                    echo '/ class'; 
                }

                else{
                    echo '/ month';
                }
                ?></td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                        <!-- teacher EDITING LINK -->
                        <li>
                            <a href="#" class="btn btn-primary" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_generate_salary/<?php echo $row['teacher_id']; ?>');">
                                <i class="entypo-pencil"></i>
                                <?php echo get_phrase('generate_salary'); ?>
                            </a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php?admin/teacher_salary_report/<?php echo $row['teacher_id']; ?>">
                                <i class="entypo-bookmark"></i>
                                <?php echo get_phrase('salary_report'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                       
                    </ul>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</tbody>
</table>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                {
                    "sExtends": "xls",
                    "mColumns": [1, 2, 3, 4]
                },
                {
                    "sExtends": "pdf",
                    "mColumns": [1, 2, 3, 4]
                },
                {
                    "sExtends": "print",
                    "fnSetText": "Press 'esc' to return",
                    "fnClick": function (nButton, oConfig) {
                        datatable.fnSetColumnVis(0, false);
                        datatable.fnSetColumnVis(5, false);
                        this.fnPrint(true, oConfig);
                        window.print();
                        $(window).keyup(function (e) {
                            if (e.which == 27) {
                                datatable.fnSetColumnVis(0, true);
                                datatable.fnSetColumnVis(5, true);
                            }
                        });
                    },
                },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>