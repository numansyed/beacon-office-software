<div class="row">
    <div class="col-md-12">
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li <?php if ($active_page == 'add') echo 'course="active"'; ?>>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('expense'); ?>
                </a></li>
                <li <?php if ($active_page == 'list') echo 'course="active"'; ?>>
                    <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                        <?php echo get_phrase('list'); ?>
                    </a></li>
                </ul>
                <!------CONTROL TABS END------>
                <div class="tab-content">
                    <!----CREATION FORM STARTS---->
                    <div class="tab-pane box <?php if ($active_page == 'add') echo 'active'; ?>" id="add">
                        <?php echo form_open(base_url() . 'index.php?admin/expense/add', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default panel-shadow" data-collapsed="0">
                                    <div class="panel-heading">
                                        <div class="panel-title"><?php echo get_phrase('expense_information'); ?></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Type</label>
                                            <div class="col-sm-10">
                                                <select id="type" name="to_account_id" class="form-control" required onchange="showparty(this.value)">
                                                    <option value=""><?php echo get_phrase('select'); ?></option>
                                                    <?php
                                                    $this->db->select('a2.*, a3.a3_id');
                                                    $this->db->from('a2');
                                                    $this->db->join('a3', 'a3.a3_id=a2.a3_id');
                                                    $this->db->where(array('a2.inactive' => '0', 'a3.a4_id' => '4'));
                                                    $accounts = $this->db->get()->result_array();
                                                    $types = $this->db->get_where('a3', array('inactive' => '0', 'a3.a4_id' => '4'))->result_array();
                                                    foreach($types as $type):?>
                                                        <optgroup label="<?php echo $type['name']; ?>">
                                                            <?php foreach ($accounts as $account):?>
                                                                <?php if($account['a3_id'] == $type['a3_id']): ?>
                                                                    <option value="<?php echo $account['a2_id']; ?>">
                                                                        <?php echo $account['a2_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $account['name']; ?>
                                                                    </option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </optgroup>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="employee_details" style="display: none;">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">FOR</label>
                                                <div class="col-sm-10">
                                                    <select name="employee_id" class="form-control" style="float:left;" id="teacherID" onchange="addAnAttributBelow()">
                                                        <option value=""><?php echo get_phrase('select_employee'); ?></option>
                                                        <?php
                                                        $employees = $this->db->get('teacher')->result_array();
                                                        foreach ($employees as $row):?>
                                                            <option value="<?php echo $row['teacher_id']; ?>">
                                                                <?php echo $row['name']; ?></option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Month</label>
                                                <div class="col-sm-10">
                                                    <select name="month" class="form-control" style="float:left;" id="monthYearSelect">
                                                        <option value="">Select Month</option>
                                                        <?php for($m=1; $m <= 12; $m++):?>
                                                            <option value="<?php echo $m; ?>" ><?php $month = $this->crud_model->getfullmonthname($m); echo $month ?></option>
                                                            <!-- <input type="hidden" id="monthName" name="" value="<?php $this->crud_model->getfullmonthname($m);?>"> -->
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group" id="showOnPrevious" style="display: none">
                                                <label class="col-sm-2 control-label">Year</label>
                                                <div class="col-sm-10">
                                                    <select name="nm_year" class="form-control" style="float:left;" id="yearOnlySelect">
                                                        <option value="">Select Year</option>
                                                        <?php for($y=2018; $y <= 2024; $y++):?>
                                                            <option value="<?php echo $y; ?>" ><?php  echo $y ?></option>
                                                            <!-- <input type="hidden" id="monthName" name="" value="<?php $this->crud_model->getfullmonthname($m);?>"> -->
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Account</label>
                                                <div class="col-sm-6">
                                                    <select name="from_account_id" class="form-control" required onchange="getbalance(this.value)">
                                                        <option value="1065"><?php echo '1065&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('petty_cash'); ?></option>
                                                        <option value="1060"><?php echo '1060&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('checking_account_(Bank)'); ?></option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span id="balance" class="form-control"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="datepicker form-control" name="date" required value="<?php echo date('m/d/Y'); ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"><?php echo get_phrase('description'); ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="memo" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!----CREATION FORM ENDS-->
                                </div>
                                <div class="col-md-6">
                                  <div class="hiddenInFalse" style="display: none">

                                  </div>
                                  <div id="showInTrue" style="display: block;">
                                    <div class="panel panel-default panel-shadow" data-collapsed="0">
                                        <div class="panel-heading">
                                            <div class="panel-title"><?php echo get_phrase('amounts'); ?></div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('total'); ?></label>
                                                <div class="col-sm-9">
                                                    <input id="total_amount" type="text" class="form-control" name="total_amount" 
                                                    placeholder="<?php echo get_phrase('enter_total_amount'); ?>" required onchange="checkpaid(0)"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('paid_amount'); ?></label>
                                                <div class="col-sm-9">
                                                    <input id="paid_amount" type="text" class="form-control" name="paid_amount"
                                                    placeholder="<?php echo get_phrase('enter_paid_amount'); ?>" required onchange="checkpaid(this.value)"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                                                <div class="col-sm-9">
                                                    <select id="paidstatus" name="status" class="form-control" required>
                                                        <option value="2"><?php echo get_phrase('unpaid'); ?></option>
                                                        <option value="1"><?php echo get_phrase('paid'); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('method'); ?></label>
                                                <div class="col-sm-9">
                                                    <select name="method" class="form-control" required onchange="showcardcheck(this.value)">
                                                        <option value="1"><?php echo get_phrase('cash'); ?></option>
                                                        <option value="2"><?php echo get_phrase('check'); ?></option>
                                                        <option value="3"><?php echo get_phrase('card'); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group" id="check" style="display: none;">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('check_no'); ?></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="check_number"
                                                    placeholder="<?php echo get_phrase('enter_check_number'); ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group" id="card" style="display: none;">
                                                <label class="col-sm-3 control-label"><?php echo get_phrase('card_no'); ?></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="card_number"
                                                    placeholder="<?php echo get_phrase('enter_card_number'); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <button type="submit" id="addButton" class="btn btn-info"><?php echo get_phrase('add_expense'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="tab-pane box <?php if ($active_page == 'list') echo 'active'; ?>" id="list" style="padding: 5px">
                        <?php echo form_open(base_url() . 'index.php?admin/expense/filter', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                        <table  class="table table-bordered datatable">
                            <tr>
                                <td><?php echo get_phrase('from_date'); ?></td>
                                <td><?php echo get_phrase('to_date'); ?></td>
                                <td><?php echo get_phrase('options'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" class="datepicker form-control" name="fromdate" required value="<?php echo $fromdate; ?>"/>
                                </td>
                                <td>
                                    <input type="text" class="datepicker form-control" name="todate" required value="<?php echo $todate; ?>"/>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-info"><?php echo get_phrase('filter'); ?></button>
                                </td>
                            </tr>
                        </table>
                        <?php echo form_close(); ?>
                        <table  class="table table-bordered datatable" id="table_export">
                            <thead>
                                <tr>
                                    <th><div><?php echo get_phrase('ID'); ?></div></th>
                                    <th><div><?php echo get_phrase('employee'); ?></div></th>
                                    <th><div><?php echo get_phrase('type'); ?></div></th>
                                    <th><div><?php echo get_phrase('description'); ?></div></th>
                                    <th><div><?php echo get_phrase('total'); ?></div></th>
                                    <th><div><?php echo get_phrase('status'); ?></div></th>
                                    <th><div><?php echo get_phrase('date'); ?></div></th>
                                    <th><div><?php echo get_phrase('options'); ?></div></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($memos as $row): ?>
                                    <tr>
                                        <td><?php echo $row['a1_id']; ?></td>
                                        <td><?php
                                        if (intval($row['employee_id']) > 0) {
                                            echo $this->crud_model->get_type_name_by_id('teacher', $row['employee_id']);
                                        } else {
                                            echo 'N/A';
                                        }?></td>
                                        <td><?php echo $this->crud_model->get_type_name_by_id('a2', $row['a2_id']); ?></td>
                                        <td><?php echo $this->crud_model->get_type_name_by_id('a7', $row['a7_id'], 'details');?></td>
                                        <td align="right"><?php echo number_format((float)$row['total_amount'], 2, '.', ''); ?></td>
                                        <td align="center"><span class="label label-<?php echo (intval($row['status']) == 1 ? 'success' : 'danger'); ?>"><?php echo (intval($row['status']) == 1 ? 'Paid' : 'Has Due'); ?></span></td>
                                        <td><?php echo date('d M, Y', strtotime($row['date'])); ?></td>
                                        <td>
                                            <?php if (intval($row['status']) == 2): ?>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                        <li>
                                                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_send_payment/<?php echo $row['a1_id']; ?>');">
                                                                <i class="entypo-bookmarks"></i>
                                                                <?php echo get_phrase('pay_expense'); ?>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!----TABLE LISTING ENDS--->
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function checkpaid(amount) {
                var _total = parseFloat($("#total_amount").val());
                if (parseFloat(amount) >= _total) {
                    $("#paidstatus").val(1);
                } else {
                    $("#paidstatus").val(2);
                }
            }
            function showcardcheck(method) {
                $("#check").css('display', 'none');
                $("#card").css('display', 'none');
                $('#check input[type="text"]').each(function () {
                    $(this).prop('required', false);
                });
                $('#card input[type="text"]').each(function () {
                    $(this).prop('required', false);
                });

                if (method == 2) {
                    $("#check").css('display', 'block');
                    $('#check input[type="text"]').each(function () {
                        $(this).prop('required', true);
                    });
                }
                if (method == 3) {
                    $("#card").css('display', 'block');
                    $('#card input[type="text"]').each(function () {
                        $(this).prop('required', true);
                    });
                }
            }
            function showparty(party) {
               if (party == 5410 || party == 5420 || party == 5470) {
                   $("#employee_details").css('display', 'block');
                   $('#employee_details input[type="text"]').each(function () {
                      $(this).prop('required', true);
                  });
                   $('#employee_details select').each(function () {
                       $(this).prop('required', true);
                   });
               } else {
                   $("#employee_details").css('display','none');
                   $('#employee_details input[type="text"]').each(function () {
                       $(this).prop('required', false);
                   });
                   $('#employee_details select').each(function () {
                       $(this).prop('required', false);
                   });
               }
           }
           function getbalance(a2_id) {
            $.ajax({
                url: '<?php echo base_url();?>index.php?admin/getbalance/' + a2_id ,
                success: function(response)
                {
                    if (response.indexOf('-') != -1 || parseFloat(response) <= 0)
                        $('#balance').css('color', 'red');
                    else
                        $('#balance').css('color', 'black');

                    $('#balance').html(response);
                }
            });
        }
        jQuery(document).ready(function ($) {
            getbalance(1065);
        });
        $("form").submit(function (e) {
            var value = $('#balance').html();
            var value2 = $('#paid_amount').val();
            if ((parseInt(value) > 0) && (parseInt(value) >= parseInt(value2))) {

            } else {
            //e.preventDefault();
            alert("Selected account does not have sufficient balance to complete this request.\nDue will be added to respective account.");
        }
    });
        function addAnAttributBelow(){
            $("#monthYearSelect").attr("onchange","addTotalAmount()");
            $("#showOnPrevious").css('display','block');
            $("#showOnPrevious").attr("onchange","addTotalAmount()");
            return true;
        }
        function addTotalAmount(){
            var d = new Date();
            var teacherID = $("#teacherID").val();
            var monthVal = $("#monthYearSelect").val();
            var month = getFullMonth(monthVal); 
            var year = $("#yearOnlySelect").val();
            var monthYear = month+'-'+year;
            var allRightSide = $('#hiddenInFalse').html();

            $.ajax({
                url: '<?php echo base_url();?>index.php?admin/check_teacher_salary/' + teacherID + '/' + monthYear ,
                dataType:"json",
                success: function(response)
                {
                    if(response.status){
                        $('#showInTrue').css('display','block');
                        $(".hiddenInFalse").css('display', 'none');
                        $('#addButton').prop('disabled', false);
                        $('#total_amount').val(response.value);
                            // console.log(allRightSide);
                        }
                        else{
                            $(".hiddenInFalse").css('display', 'block');
                            $('#addButton').prop('disabled', true);
                            $('#showInTrue').css('display','none');
                            $(".hiddenInFalse").html(response.value);

                        }
                    }
                });

        }

        function getFullMonth(m){
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var n = month[m-1];
            return n;
        }
    </script>