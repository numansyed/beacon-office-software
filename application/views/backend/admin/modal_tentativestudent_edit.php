<?php
$edit_data = $this->db->get_where('tentative_student', array('tentative_student_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('Edit_tentative_student'); ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php echo form_open(base_url() . 'index.php?admin/tentative_student/do_update/' . $row['tentative_student_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                   
                    <div class="form-group">
                        <label for="field-1" class="col-sm-2 control-label"><?php echo get_phrase('name'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="student_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['student_name']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('course'); ?></label>
                        <div class="col-sm-10">
                           
                            <?php
                            $courses = $this->db->get('course')->result_array();
                            foreach ($courses as $row2):
                                ?>
                                <input type="checkbox" name="preferred_course[]" value="<?php echo $row2['course_id']; ?>"
                                  />
                                    <?php echo $row2['name']; ?>
                               
                                <?php
                            endforeach;
                            ?>
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('Area'); ?></label>
                    <div class="col-sm-10">
                        <select name="m_area" class="form-control " data-validate="required" id="mArea"
                        data-message-required="<?php echo get_phrase('value_required'); ?>">
                        <option value=""><?php echo get_phrase('select'); ?></option>
                        <?php
                        $m_areas = $this->db->get('m_area')->result_array();
                        foreach ($m_areas as $row5):
                            ?>
                            <option value="<?php  echo $row['m_area']; ?> ">
                                <?php echo $row5['name']; ?>
                            </option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>

          
            <div class="form-group">
                <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('phone'); ?></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="contact_no" value="<?php echo $row['contact_no']; ?>" >
                </div> 
            </div>
         

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                    <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_student'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</div>
<?php
endforeach;
?>
<script type="text/javascript">
    function get_course_batchs(course_id) {
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
            success: function (response)
            {
                jQuery('#batch_selector_holder').html(response);
            }
        });
    }
    var course_id = $("#course_id").val();
    $.ajax({
        url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
        success: function (response)
        {
            jQuery('#batch_selector_holder').html(response);
        }
    });
</script>