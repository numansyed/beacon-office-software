<?php 
$edit_data		=	$this->db->get_where('contact' , array('contact_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_contact');?>
            	</div>
            </div>
			<div class="panel-body">
                    <?php echo form_open(base_url() . 'index.php?admin/contact/do_update/'.$row['contact_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>
                            <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('Contact_name'); ?></label>

                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="contact_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['contact_name']; ?>">
                                    </div>
                                </div>
                            <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_phone'); ?></label>

                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="contact_phone" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['contact_phone']; ?>">
                                    </div>
                                </div>
                            <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('Contact_email'); ?></label>

                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="contact_email" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['contact_email']; ?>">
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('contact_address'); ?></label>

                                <div class="col-sm-5">
                                    <textarea type="text" class="form-control autogrow" name="contact_address" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" ><?php echo $row['contact_address']; ?></textarea>
                                </div> 
                            </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_contact');?></button>
                            </div>
                        </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>