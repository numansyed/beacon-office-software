<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li <?php if ($active_page == 'master') echo 'course="active"'; ?>>
                <a href="#master" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('master_accounts');?>
                </a>
            </li>
			<li <?php if ($active_page == 'type') echo 'course="active"'; ?>>
            	<a href="#cat" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('types');?>
                </a>
            </li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div <?php if($active_page == 'master'){echo 'course="tab-pane box active"';}else{echo 'course="tab-pane box"';}?> id="master">
                <table width="100%" border="0" class="table table-bordered">
                    <tr>
                        <td align="right">
                            <a id="addmaster" href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_add_coa/master');"
                               class="btn btn-info btn-sm"><i class="entypo-plus-circled"></i> Add New Master Account</a>
                        </td>
                    </tr>
                </table>
                <table  class="table table-bordered datatable" id="table_export1">
                    <thead>
                    <tr>
                        <th><div><?php echo get_phrase('code');?></div></th>
                        <th><div><?php echo get_phrase('name');?></div></th>
                        <th><div><?php echo get_phrase('type');?></div></th>
                        <th><div><?php echo get_phrase('active');?></div></th>
                        <th><div><?php echo get_phrase('comment');?></div></th>
                        <th><div><?php echo get_phrase('options');?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($coa_masters as $row):?>
                        <tr>
                            <td><?php echo $row['a2_id'];?></td>
                            <td><?php echo $row['name'];?></td>
                            <td><?php echo $this->crud_model->get_type_name_by_id('a3', $row['a3_id']); ?></td>
                            <td><?php if(intval($row['inactive']) == 1){echo 'No';}else{echo 'Yes';}?></td>
                            <td><?php echo $row['comment'];?></td>
                            <td>
                                <?php if (intval($row['system']) == 0){?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_coa/master/<?php echo $row['a2_id'];?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/coa/delete/master/<?php echo $row['a2_id'];?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <?php } else { ?>
                                    Readonly
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
			</div>

			<div <?php if($active_page == 'type'){echo 'course="tab-pane box active"';}else{echo 'course="tab-pane box"';}?> id="cat" style="padding: 5px">
                <div class="box-content">
                    <table width="100%" border="0" class="table table-bordered">
                        <tr>
                            <td align="right">
                                <a id="addtype" href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_add_coa/type');"
                                   class="btn btn-info btn-sm"><i class="entypo-plus-circled"></i> Add New Type</a>
                            </td>
                        </tr>
                    </table>
                    <table  class="table table-bordered datatable" id="table_export2">
                        <thead>
                        <tr>
                            <th><div><?php echo get_phrase('code');?></div></th>
                            <th><div><?php echo get_phrase('name');?></div></th>
                            <th><div><?php echo get_phrase('course');?></div></th>
                            <th><div><?php echo get_phrase('parent');?></div></th>
                            <th><div><?php echo get_phrase('active');?></div></th>
                            <th><div><?php echo get_phrase('comment');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($coa_types as $row):?>
                            <tr>
                                <td><?php echo $row['a3_id'];?></td>
                                <td><?php echo $row['name'];?></td>
                                <td><?php echo $this->crud_model->get_type_name_by_id('a4', $row['a4_id']); ?></td>
                                <td><?php if (intval($row['parent_id']) == 0)
                                        echo 'None';
                                    else
                                        echo $this->crud_model->get_type_name_by_id('a3', $row['parent_id']);
                                    ?></td>
                                <td><?php if(intval($row['inactive']) == 1){echo 'No';}else{echo 'Yes';}?></td>
                                <td><?php echo $row['comment'];?></td>
                                <td>
                                    <?php if (intval($row['system']) == 0){?>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Action<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                            <!-- EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_coa/type/<?php echo $row['a3_id'];?>');">
                                                    <i class="entypo-pencil"></i>
                                                    <?php echo get_phrase('edit');?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <!-- DELETION LINK -->
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/coa/delete/type/<?php echo $row['a3_id'];?>');">
                                                    <i class="entypo-trash"></i>
                                                    <?php echo get_phrase('delete');?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php } else { ?>
                                        Readonly
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>                
			</div>
            <!----TABLE LISTING ENDS--->
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export1").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(4, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(4, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
        var datatable = $("#table_export2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(4, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(4, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        /*var datatable = $("#table_export3").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(4, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(4, true);
                                }
                            });
                        },
                    },
                ]
            },
        });*/
    });
		
</script>