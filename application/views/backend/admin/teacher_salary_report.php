
<br><br>
<table class="table table-bordered " id="">
    <thead>
        <tr>
            <th colspan="7">
                <?php foreach($teacher_details as $detail):

                    ?>
                    <p class="text-center text-sm"> Name: <?php echo $detail['name']// var_dump($teacher_details);?></p> 
                    <p class="text-center text-sm"> Phone: <?php echo $detail['phone']// var_dump($teacher_details);?></p> 
                    <p class="text-center text-sm"> Salary: <?php 
                    echo $detail['salary'];
                    if($detail['teacher_type_id'] == '2'){
                        echo '/ class'; 
                    }

                    else{
                        echo '/ month';
                    }

                   // var_dump($teacher_details);?></p> 
                   <?php 
               endforeach;?>

           </th>
       </tr>
       <tr>            
        <th width="80"><div><?php echo 'SL'; ?></div></th>
        <th><div><?php echo get_phrase('Month'); ?></div></th>
        <th><div><?php echo get_phrase('no_of_class'); ?></div></th>
        <th><div><?php echo get_phrase('Amount'); ?></div></th>
        <th><div><?php echo get_phrase('Paid'); ?></div></th>
        <th><div><?php echo get_phrase('Due'); ?></div></th>
        <th><div><?php echo get_phrase('Status');?></div></th>
    </tr>
</thead>
<tbody>
    <?php
    $count = 0;
    $teachers_salary= $this->db->get_where('teacher_salary',array('teacher_id' => $teacher_id))->result_array();
    foreach ($teachers_salary as $row):
        ?>
        <tr>
            <td><?= ++$count;?></td>
            <td><?php echo $row['for_month']; ?></td>
            <td><?php echo $row['no_of_class']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['paid']; ?></td>
            <td><?php echo $row['due']?></td>
            <td>
                <?php 
                if($row['status'] == 2){
                    ?>
                    <span class="btn btn-danger disabled">Not Paid</span>
                    <?php 
                }
                elseif($row['status'] == 1){
                   ?>
                   <span class="btn btn-default">Partially Paid</span>
                   <?php 
               }

               elseif($row['status'] == 0){
                   ?>
                   <span class="btn btn-success">Full Paid</span>
                   <?php 
               }
               ?>                 
           </td>
       </tr>
   <?php endforeach; ?>
</tbody>
</table>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                {
                    "sExtends": "xls",
                    "mColumns": [1, 2, 3, 4]
                },
                {
                    "sExtends": "pdf",
                    "mColumns": [1, 2, 3, 4]
                },
                {
                    "sExtends": "print",
                    "fnSetText": "Press 'esc' to return",
                    "fnClick": function (nButton, oConfig) {
                        datatable.fnSetColumnVis(0, false);
                        datatable.fnSetColumnVis(5, false);
                        this.fnPrint(true, oConfig);
                        window.print();
                        $(window).keyup(function (e) {
                            if (e.which == 27) {
                                datatable.fnSetColumnVis(0, true);
                                datatable.fnSetColumnVis(5, true);
                            }
                        });
                    },
                },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>