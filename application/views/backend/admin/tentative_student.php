<hr />
<a href="<?php echo base_url(); ?>index.php?admin/tentative_student_add" 
   class="btn btn-primary pull-right">
   <i class="entypo-plus-circled"></i>
   <?php echo get_phrase('tentative_student_add'); ?>
</a> 

<br>

<div class="row">
    <div class="col-md-12">

        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#home" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-users"></i></span>
                    <span class="hidden-xs"><?php echo get_phrase('tentative_student');?></span>
                </a>
            </li>

        </ul>
        
        <div class="tab-content">
            <div class="tab-pane active" id="home">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            <th width="80"><div><?php echo get_phrase('Id');?></div></th>
                            <th width="80"><div><?php echo get_phrase('name');?></div></th>
                            <th width="80"><div><?php echo 'contact';?></div></th>
                            <th><div><?php echo get_phrase('preferred_course');?></div></th>
                            <th><div><?php echo get_phrase('status');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $students   =   $this->db->get('tentative_student')->result_array();
                        foreach($students as $row):?>
                            <tr>
                                <td><?php echo $row['tentative_student_id'];?></td>
                                <td><?= $row['student_name']?></td>
                                <td><?php echo $row['contact_no'];?></td>
                                <td><?php 
                                $c_int = array();
                                $c_int  = json_decode($row['preferred_course_id']);
                                $tot =  count($c_int[0]);

                                for($i = 0; $i <= $tot;$i++){
                                    ?>

                                    <p>  <?php echo $this->crud_model->get_type_name_by_id('course',$c_int[0][$i]);?></p>
                                    <?php
                                }


                                // $courses = json_decode($row['preferred_course_id']);
                                // foreach ($courses as $course) {
                                //     # code...
                                //     echo $this->crud_model->get_type_name_by_id('course',$course['id']).'<br>';

                                // }

                                ?></td>
                                <td><?php 
                                if($row['check_status'] == 1){
                                    ?>
                                    <button class="btn btn-success disabled">
                                                                        <?php
                                    echo "admitted";
                                    ?>
                                    </button>

                                    <?php 

                                }
                                else{
                                ?>
                                    <button class="btn btn-danger disabled">
                                                                        <?php
                                    echo "tentative";
                                    ?>
                                    </button>

                                    <?php 

                                }
                                ?></td>
                                <td>

                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                          
                                            <!---- SEND SMS LINK----->
                                            <?php
                                          
                                            if ($row['check_status'] == 0):?>
                                                <li>
                                                    <a href="<?php echo base_url();?>index.php?admin/student_add/admit_from_tentative/<?php echo $row['tentative_student_id'];?>">
                                                        <i class="entypo-mail"></i>
                                                        <?php echo get_phrase('Admit_this_student');?>
                                                    </a>
                                                </li>
                                            <?php endif;?>
                                            <li class="divider"></li>
   <!---- SEND SMS LINK----->
                                            <?php
                                            $active_sms_service = $this->db->get_where('settings', array(
                                                'type' => 'active_sms_service'
                                            ))->row()->description;
                                            if (strlen($active_sms_service) > 0 && $active_sms_service != 'disabled'):?>
                                                <li>
                                                    <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_tentativestudent_sms/<?php echo $row['tentative_student_id'];?>/student');">
                                                        <i class="entypo-mail"></i>
                                                        <?php echo get_phrase('Send_SMS');?>
                                                    </a>
                                                </li>
                                            <?php endif;?>
                                            <li class="divider"></li>
                                            <!-- STUDENT EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_tentativestudent_edit/<?php echo $row['tentative_student_id'];?>');">
                                                    <i class="entypo-pencil"></i>
                                                    <?php echo get_phrase('edit');?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>

                                            <!-- STUDENT DELETION LINK -->
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/tentative_student/delete/<?php echo $row['tentative_student_id'];?>');">
                                                    <i class="entypo-trash"></i>
                                                    <?php echo get_phrase('delete');?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>

            </div>


        </div>


    </div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable({
			"sPaginationType": "bootstrap",
			"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			"oTableTools": {
				"aButtons": [

               {
                  "sExtends": "xls",
                  "mColumns": [0, 2, 3, 4, 5]
              },
              {
                  "sExtends": "pdf",
                  "mColumns": [0, 2, 3, 4, 5]
              },
              {
                  "sExtends": "print",
                  "fnSetText"	   : "Press 'esc' to return",
                  "fnClick": function (nButton, oConfig) {
                     datatable.fnSetColumnVis(1, false);
                     datatable.fnSetColumnVis(6, false);

                     this.fnPrint( true, oConfig );

                     window.print();

                     $(window).keyup(function(e) {
                      if (e.which == 27) {
                         datatable.fnSetColumnVis(1, true);
                         datatable.fnSetColumnVis(6, true);
                     }
                 });
                 },

             },
             ]
         },

     });
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});

</script>