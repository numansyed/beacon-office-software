<?php
$edit_data = $this->db->get_where('a1', array('a1_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title"><?php echo get_phrase('expense_history'); ?></div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td><?php echo get_phrase('amount'); ?></td>
                                <td><?php echo get_phrase('method'); ?></td>
                                <td><?php echo get_phrase('details'); ?></td>
                                <td><?php echo get_phrase('date'); ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $total_paid = 0;
                            $payments = $this->db->get_where('a5', array(
                                        'a1_id' => $row['a1_id']
                                    ))->result_array();
                            foreach ($payments as $row2):?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td align="right"><?php echo $row2['amount']; ?></td>
                                    <td><?php
                                        if ($row2['method'] == 1)
                                            echo get_phrase('cash');
                                        if ($row2['method'] == 2)
                                            echo get_phrase('check');
                                        if ($row2['method'] == 3)
                                            echo get_phrase('card');
                                        if ($row2['method'] == 'paypal')
                                            echo 'paypal';
                                        ?></td>
                                    <td><?php
                                        if (intval($row2['method']) == 2)
                                            echo $row2['check_number'];
                                        else if (intval($row2['method']) == 3)
                                            echo $row2['card_number'];
                                        else
                                            echo 'N/A';
                                        ?></td>
                                    <td><?php echo date('d M, Y', strtotime($row2['date'])); ?></td>
                                </tr>
                                <?php $total_paid += floatval($row2['amount']); ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title"><?php echo get_phrase('pay_expense'); ?></div>
                </div>
                <div class="panel-body">
                    <?php echo form_open(base_url() . 'index.php?admin/expense/due', array(
                        'class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
                    ?>
                    <input type="hidden" name="da1_id" value="<?php echo $row['a1_id']; ?>">
                    <input type="hidden" name="dprev_amount" value="<?php echo $total_paid; ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('total_amount'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dtotal_amount" name="dtotal_amount" value="<?php echo number_format((float)$row['total_amount'], 2, '.', ''); ?>" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('paid_amount'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dtotal_paid" value="<?php echo number_format((float)$total_paid, 2, '.', ''); ?>" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('payment'); ?></label>
                        <div class="col-sm-6">
                            <input id="dpaid_amount" type="text" class="form-control" name="dpaid_amount" value=""
                                   placeholder="<?php echo get_phrase('enter_payment_amount'); ?>" required onchange="checkpaid(this.value)" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('due'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="due_amount" value="<?php echo number_format((float)(floatval($row['total_amount']) - floatval($total_paid)), 2, '.', ''); ?>" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Account</label>
                        <div class="col-sm-6">
                            <select name="dfrom_account_id" class="form-control" required onchange="getbalance(this.value)">
                                <option value="1065"><?php echo '1065&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('petty_cash'); ?></option>
                                <option value="1060"><?php echo '1060&nbsp;&nbsp;&nbsp;&nbsp;' . get_phrase('checking_account_(Bank)'); ?></option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <span id="dbalance" class="form-control"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('method'); ?></label>
                        <div class="col-sm-6">
                            <select name="dmethod" class="form-control" required onchange="showcardcheck(this.value)">
                                <option value="1"><?php echo get_phrase('cash'); ?></option>
                                <option value="2"><?php echo get_phrase('check'); ?></option>
                                <option value="3"><?php echo get_phrase('card'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="dcheck" style="display: none;">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('check_no'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="dcheck_number"
                                   placeholder="<?php echo get_phrase('enter_check_number'); ?>" />
                        </div>
                    </div>
                    <div class="form-group" id="dcard" style="display: none;">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('card_no'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="dcard_number"
                                   placeholder="<?php echo get_phrase('enter_card_number'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" class="datepicker form-control" name="ddate" required
                                   value="<?php echo date('m/d/Y'); ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('pay_expense'); ?></button>
                        </div>
                    </div>
    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<script type="text/javascript">
    $("form").submit(function (e) {
        var value = $('#dbalance').html();
        var value2 = $('#dpaid_amount').val();
        if ((parseInt(value) > 0) && (parseInt(value) >= parseInt(value2))) {

        } else {
            e.preventDefault();
            alert("Selected account does not have sufficient balance to complete this request.");
        }
    });
    function checkpaid(amount) {
        var _total = parseFloat($("#dtotal_amount").val());
        var _paid = parseFloat($("#dtotal_paid").val());
        $("#due_amount").val(_total - (parseFloat(amount) + _paid));
    }
    function showcardcheck(method) {
        $("#dcheck").css('display', 'none');
        $("#dcard").css('display', 'none');
        $('#dcheck input[type="text"]').each(function () {
            $(this).prop('required', false);
        });
        $('#dcard input[type="text"]').each(function () {
            $(this).prop('required', false);
        });

        if (method == 2) {
            $("#dcheck").css('display', 'block');
            $('#dcheck input[type="text"]').each(function () {
                $(this).prop('required', true);
            });
        }
        if (method == 3) {
            $("#dcard").css('display', 'block');
            $('#dcard input[type="text"]').each(function () {
                $(this).prop('required', true);
            });
        }
    }
    function getbalance(a2_id) {
        $.ajax({
            url: '<?php echo base_url();?>index.php?admin/getbalance/' + a2_id ,
            success: function(response)
            {
                if (response.indexOf('-') != -1 || parseFloat(response) <= 0)
                    $('#dbalance').css('color', 'red');
                else
                    $('#dbalance').css('color', 'black');

                $('#dbalance').html(response);
            }
        });
    }
    jQuery(document).ready(function ($) {
        getbalance(1065);
    });
</script>

