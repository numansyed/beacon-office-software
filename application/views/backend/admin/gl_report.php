<style>
    #head {display: none;}
</style>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th>From Date</th>
        <th>To Date</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    <?php echo form_open(base_url() . 'index.php?admin/gl_report', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
        <tr class="gradeA">
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="fromdate" value="<?php echo date('m/d/Y', $fromdate); ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="todate" value="<?php echo date('m/d/Y', $todate); ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td><input type="submit" value="<?php echo get_phrase('view'); ?>" class="btn btn-info"/></td>
        </tr>
    <?php echo form_close(); ?>
    </tbody>
</table>
<?php if (count($data) > 0):?>
<table width="100%" border="0" class="table table-bordered">
    <tr>
        <td align="center">
            <a id="download" href="#" target="_blank"
               class="btn btn-info btn-sm"><i class="entypo-download"></i> Download Report</a>
        </td>
    </tr>
</table>
<center>
    <div style="border: 1px solid black; color: #000000; background-color: #ffffff;">
        <div id="head">
    <table id="heading" border="0" cellpadding="0" cellspacing="0" style="border: hidden;" width="99%">
        <tr>
            <td align="center"><img src="uploads/logo.png"
                                    class="img-responsive"
                                    style="max-height:35px"/></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 14pt;"><strong><?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?></strong></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 12pt;"><strong>General Ledger</strong></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 11pt;"><strong>All Accounts</strong></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 10pt;"><strong><?php echo '(' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')'; ?></strong></td>
        </tr>
    </table>
        </div>
        <div id="data">
<table class="table table-bordered datatable" id="datatable" border="1" cellpadding="2" cellspacing="1" style="border: 1px solid black; border-collapse: collapse;" width="99%">
    <thead>
        <tr>
            <th><div>Ref.</div></th>
            <th><div><?php echo get_phrase('G/L_ID.'); ?></div></th>
            <th><div><?php echo get_phrase('type'); ?></div></th>
            <th><div><?php echo get_phrase('date'); ?></div></th>
            <th><div><?php echo get_phrase('account_code'); ?></div></th>
            <th><div><?php echo get_phrase('account_name'); ?></div></th>
            <th><div><?php echo get_phrase('debit'); ?></div></th>
            <th><div><?php echo get_phrase('credit'); ?></div></th>
            <th><div><?php echo get_phrase('memo'); ?></div></th>
        </tr>
    </thead>
<tbody>
    <?php
    $credit = 0;
    $debit = 0;
    $ref = 0;
    $rowspan = 0;
    foreach ($data as $row):?>
        <tr>
            <?php if($ref != intval($row['a1_id'])):?>
                <?php foreach ($data as $row2) {if ($row2['a1_id'] == $row['a1_id']) $rowspan++;}?>
                <td rowspan="<?php echo $rowspan; ?>">
                    <?php echo $row['a1_id'];
                    $ref = $row['a1_id'];
                    $rowspan = 0;?>
                </td>
            <?php endif; ?>
            <td><?php echo $row['a6_id'] ?></td>
            <td><?php echo $this->crud_model->get_type_name_by_id('a3', $this->crud_model->get_type_name_by_id('a2', $row['a2_id'], 'a3_id')); ?></td>
            <td><?php echo date('m/d/Y', strtotime($row['date'])); ?></td>
            <td><?php echo $row['a2_id'] ?></td>
            <td><?php echo $this->crud_model->get_type_name_by_id('a2', $row['a2_id']); ?></td>
            <td align="right"><?php
                if (($this->crud_model->get_table($row['a2_id'], 'a2') == 'a1' && strpos($row['amount'], '-') === FALSE) ||
                    ($this->crud_model->get_table($row['a2_id'], 'a2') == 'a2' && strpos($row['amount'], '-') !== FALSE)) {
                    $debit += floatval(str_replace('-', '', $row['amount']));
                    echo $this->crud_model->money(str_replace('-', '', $row['amount']));
                }
                ?></td>
            <td align="right"><?php
                if (($this->crud_model->get_table($row['a2_id'], 'a2') == 'a2' && strpos($row['amount'], '-') === FALSE) ||
                    ($this->crud_model->get_table($row['a2_id'], 'a2') == 'a1' && strpos($row['amount'], '-') !== FALSE)) {
                    $credit += floatval(str_replace('-', '', $row['amount']));
                    echo $this->crud_model->money(str_replace('-', '', $row['amount']));
                }
                ?></td>
            <td><?php echo $this->crud_model->get_type_name_by_id('a7', $row['a7_id'], 'details'); ?></td>
        </tr>
    <?php
    endforeach;
    ?>
</tbody>
</table>
        </div>
        </div>
</center>
<?php endif; ?>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    $("#download").click(function (e) {
        e.preventDefault();

        var pdfbody = $("#data").html();
        var pdfheader = $("#head").html();
        var pdffooter = '<table width="99%" border="0" style="font-family:sans-serif; font-size:8pt; font-weight:bold; color:#999999;"><tr><td align="right">{PAGENO}</td></tr></table>';

        var styledata = '<style media="print">' +
            '#heading {width: 99%; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial;}' +
            '#datatable {width: 8.5in; font-family: \'MS Sans Serif\', sans-serif, Verdana, Arial !important; border-style: solid;border-color: black;border-width: 1px;border-collapse: collapse;}' +
            '</style>';

        $.ajax({
            async: false,
            url: '<?php echo base_url() . 'index.php?admin/getreportpdf'; ?>',
            type: 'POST',
            data: {
                header: pdfheader,
                body: pdfbody,
                footer: pdffooter,
                style: styledata,
                pagesize: 'A4',
                filename: 'GL_Report__<?php echo date('m-d-Y', $fromdate) . '__' . date('m-d-Y', $todate); ?>'
            },
            timeout: 10000,
            success: function (result) {
                if (result.length > 0) {
                    var link = $("#download");
                    link.attr("href", result);
                    window.open(link.attr("href"));
                }
            }
        });
    });
</script>