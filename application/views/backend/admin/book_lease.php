<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('lease_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('lease_book');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('book');?></div></th>
                    		<th><div><?php echo get_phrase('author');?></div></th>
                            <th><div><?php echo get_phrase('publisher');?></div></th>
                            <th><div><?php echo get_phrase('student');?></div></th>
                            <th><div><?php echo get_phrase('date');?></div></th>
                            <th><div><?php echo get_phrase('remaining_days');?></div></th>
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($book_lease as $row):?>
                            <?php if ($row['status'] == 0): ?>
                        <tr>
                            <td><?php echo $this->crud_model->get_type_name_by_id('book',$row['book_id']);?></td>
                            <td><?php echo $this->crud_model->get_type_name_by_id('book',$row['book_id'],'author');?></td>
							<td><?php echo $this->crud_model->get_type_name_by_id('book',$row['book_id'],'publisher');?></td>
                            <td><?php echo $this->crud_model->get_type_name_by_id('student',$row['book_leasee_id']);?></td>
							<td><?php echo date('m-d-Y', strtotime($row['lease_date']));?></td>
							<td><?php
                                $_start = strtotime($row['lease_date']);
                                $_end = strtotime('now');
                                $_diff = abs($_end - $_start);
                                $_remaining_days = $row['lease_days'] - intval($_diff/86400);
                                echo $_remaining_days . ($_remaining_days > 0 ? '' : '/late');
                                ?>
                            </td>
                            <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm" onclick="return_book(<?php echo $row['book_id'] . ', ' . $row['book_lease_id']; ?>)">
                                    Return
                                </button>
                            </div>
                            
        					</td>
                        </tr>
                                <?php endif;?>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(base_url() . 'index.php?admin/book_lease/lease' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('book_name');?></label>
                                <div class="col-sm-5">
                                    <select name="book_id" class="form-control" style="width:100%;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('book');
                                        $this->db->order_by('book_category_id','asc');
                                        $book = $this->db->get()->result_array();
                                        $category = $this->db->get('book_category')->result_array();;
                                        foreach($category as $row):?>
                                        <optgroup label="<?php echo $row['name'];?>">
                                        <?php foreach($book as $row2):?>
                                        <?php if (($row2['locked'] == 0) && ($row2['available'] > 0) && ($row2['book_category_id'] == $row['book_category_id'])):?>
                                        <option value="<?php echo $row2['book_id'];?>"><?php echo $row2['name'];?></option>
                                        <?php endif;?>
                                        <?php endforeach; ?>
                                        </optgroup>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('student');?></label>
                                <div class="col-sm-5">
                                    <select name="student_id" class="form-control" style="width:100%;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('student');
                                        $this->db->order_by('course_id','asc');
                                        $student = $this->db->get()->result_array();
                                        $course = $this->db->get('course')->result_array();
                                        foreach($course as $row):?>
                                        <optgroup label="Course <?php echo $row['name'];?>">
                                        <?php foreach($student as $row2):?>
                                            <?php if ($row2['course_id'] == $row['course_id']):?>
                                            <option value="<?php echo $row2['student_id'];?>"><?php echo $row2['name'];?></option>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                        </optgroup>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('allowed_days');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="days" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            	<div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo get_phrase('lease_book');?></button>
                              </div>
								</div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS--->
		</div>
	</div>
</div>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(6, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(6, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

    function return_book(book_id, lease_id) {
        var _url = '<?php echo base_url() . 'index.php?admin/book_lease/return/'; ?>';
        _url += book_id + '/' + lease_id;

        $.ajax({
            async: false,
            url: _url,
            type: 'POST',
            data: { book: book_id, lease: lease_id },
            timeout: 4000,
            success: function(result) {
                  window.location.href = result;
            }
        });
    }
</script>