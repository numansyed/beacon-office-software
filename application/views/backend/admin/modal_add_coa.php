<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					<i class="entypo-plus-circled"></i>
					<?php
					if ($param2 == 'master') echo get_phrase('add_master_account');
					if ($param2 == 'type') echo get_phrase('add_type');
					?>
				</div>
			</div>
			<div class="panel-body">
				<?php if ($param2 == 'master'):?>
					<?php echo form_open(base_url() . 'index.php?admin/coa/create/master' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('code');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="a2_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus onchange="check_availble_id(this, 'a2')">
                            <span id="notice" style="color: darkred;display: none;">ID Not Available</span>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" value="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('type'); ?></label>
						<div class="col-sm-5">
							<select name="a3_id" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<option value=""><?php echo get_phrase('select'); ?></option>
								<?php
								$types = $this->db->get('a3')->result_array();
								foreach ($types as $row):
								if (intval($row['inactive']) == 0):
									?>
									<option value="<?php echo $row['a3_id']; ?>">
										<?php echo $row['a3_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row['name']; ?>
									</option>
								<?php
								endif;
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('active');?></label>
						<div class="col-sm-5">
							<select name="inactive" class="form-control" required>
								<option value=""><?php echo get_phrase('select');?></option>
								<option value="0"><?php echo get_phrase('yes');?></option>
								<option value="1"><?php echo get_phrase('no');?></option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="comment" value="">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info" ><?php echo get_phrase('add_master_account');?></button>
						</div>
					</div>
					<?php echo form_close();?>
				<?php endif; ?>
				<?php if ($param2 == 'type'):?>
					<?php echo form_open(base_url() . 'index.php?admin/coa/create/type' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('code');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="a3_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus onchange="check_availble_id(this, 'a3')">
                            <span id="notice" style="color: darkred;display: none;">ID Not Available</span>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" value="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('course'); ?></label>
						<div class="col-sm-5">
							<select name="a4_id" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<option value=""><?php echo get_phrase('select'); ?></option>
								<?php
								$courses = $this->db->get('a4')->result_array();
								foreach ($courses as $row):
									?>
									<option value="<?php echo $row['a4_id']; ?>">
										<?php echo $row['a4_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row['name']; ?>
									</option>
									<?php
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('parent'); ?></label>
						<div class="col-sm-5">
							<select name="parent_id" class="form-control">
								<option value=""><?php echo get_phrase('select'); ?></option>
								<?php
								$types = $this->db->get('a3')->result_array();
								foreach ($types as $row):
									if (intval($row['inactive']) == 0):
									?>
									<option value="<?php echo $row['a3_id']; ?>">
										<?php echo $row['a3_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row['name']; ?>
									</option>
									<?php
									endif;
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('active');?></label>
						<div class="col-sm-5">
							<select name="inactive" class="form-control" required>
								<option value=""><?php echo get_phrase('select');?></option>
								<option value="0"><?php echo get_phrase('yes');?></option>
								<option value="1"><?php echo get_phrase('no');?></option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('comment');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="comment" value="">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info" disabled><?php echo get_phrase('add_type');?></button>
						</div>
					</div>
					<?php echo form_close();?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var type ;
    function check_availble_id(obj, type) {
        var id = $(obj).val();
        var _reply = 0;

        $('button[type="submit"]').each(function() {
            $(this).prop('disabled', true);
        });
        $("#notice").css("display", "none");

        if (id.length > 0) {
            $.ajax({
                async: false,
                timeout: 4000,
                url: '<?php echo base_url();?>index.php?admin/checkid/' + type + '/' + id,
                success: function (reply) {
                    _reply = reply;
                    if (reply == 0) {
                        $('button[type="submit"]').each(function() {
                            $(this).prop('disabled', true);
                        });
                        $("#notice").css("display", "block");
                    } else if (reply == 1) {
                        $('button[type="submit"]').each(function() {
                            $(this).prop('disabled', false);
                        });
                        $("#notice").css("display", "none");
                    }
                }
            });
        }
        return (_reply == 1);
    }

    var element = document.querySelector("form");
    element.addEventListener("submit", function(event) {
        event.preventDefault();

        var obj = null;
        var type = "";

        $('input[type="text"]').each(function () {
            var _name = $(this).prop("name");           
            if (_name.indexOf('id') > 0) {
                obj = this;
                var _type = _name.split("_");
                type = _type[0];
 alert(type);
                return false;
            }
        });

        if (obj && type.length > 0) {
            if (check_availble_id(obj, type)) {
                $("form").submit();
            }
            else {
                return false;
            }
        }
    });
</script>