 <ul class="nav tabs-vertical">
                <?php
                $courses = $this->db->get('course')->result_array();
                foreach ($courses as $row):
                    ?>
                    <li class="<?php if ($row['course_id'] == $course_id) echo 'active'; ?>">
                        <a href="#" onclick="show_batch_list_page(<?php echo $row['course_id']; ?>)">
                            <i class="entypo-dot"></i>
                            <?php echo get_phrase('course'); ?> <?php echo $row['name']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <table class="table table-bordered responsive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('batch_name'); ?></th>
                                <th><?php echo get_phrase('status'); ?></th>
                                <th><?php echo get_phrase('trainer'); ?></th>
                                <th><?php echo get_phrase('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $batchs = $this->db->get_where('batch', array(
                                        'course_id' => $course_id
                                    ))->result_array();
                            foreach ($batchs as $row):
                                ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><span class="label label-<?php echo (intval($row['status']) == 1 ? 'success' : 'danger'); ?>"><?php echo (intval($row['status']) == 1 ? 'Active' : 'Inactive'); ?></span></td>
                                    <td>
                                        <?php
                                        if ($row['teacher_id'] != '')
                                            echo $this->db->get_where('teacher', array('teacher_id' => $row['teacher_id']))->row()->name;
                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/batch_edit/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-pencil"></i>
    <?php echo get_phrase('edit'); ?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <!-- Activate or Deactivate Batch -->
                                                <li>
                                                    <?php if($row['status'] == 0){
                                                        ?>
                                                      <a href="#" onclick="confirm_modal1('<?php echo base_url(); ?>index.php?admin/batchs/update/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-trash"></i>
    <?php echo get_phrase('activate'); ?>
                                                    </a>
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                         <a href="#" onclick="confirm_modal1('<?php echo base_url(); ?>index.php?admin/batchs/update/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-trash"></i>
    <?php echo get_phrase('Deactivate'); ?>
                                                    </a>
                                                        <?php 
                                                    }?>
                                                </li>
                                                <li class="divider"></li>
                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/batchs/delete/<?php echo $row['batch_id']; ?>');">
                                                        <i class="entypo-trash"></i>
    <?php echo get_phrase('delete'); ?>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
<?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>