<?php
$edit_data = $this->db->get_where('album', array('album_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_album'); ?>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(base_url() . 'index.php?admin/photogallery/do_update/' . $row['album_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('album cover'); ?></label>

                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 345px; height: 215px;" data-trigger="fileinput">
                                    <img src="<?php echo $this->crud_model->get_album_url('album_admin',$row['album_id'],$row['album_name']);?>" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="userfile" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('album_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="album_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['album_name']?>" autofocus>
                    </div>
                </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('album description');?></label>
                        <div class="col-sm-5">
                            <textarea class="form-control autogrow" name="album_description" placeholder="Enter album description" style="min-height: 120px;"><?php echo $row['album_description']?></textarea>
                        </div>
                    </div>

                    
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('edit'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
endforeach;
?>

<script type="text/javascript">

    function get_course_batchs(course_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
            success: function (response)
            {
                jQuery('#batch_selector_holder').html(response);
            }
        });

    }

    var course_id = $("#course_id").val();

    $.ajax({
        url: '<?php echo base_url(); ?>index.php?admin/get_course_batch/' + course_id,
        success: function (response)
        {
            jQuery('#batch_selector_holder').html(response);
        }
    });


</script>