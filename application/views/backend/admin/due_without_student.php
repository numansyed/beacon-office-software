    <table  class="table table-bordered datatable" id="table_export">
        <thead>
            <tr>

                <th><div><?= get_phrase('ID');?></div></th>
                <th><div><?php echo get_phrase('Name'); ?></div></th>
                <th><div><?php echo get_phrase('Fee'); ?></div></th>
                <th><div><?php echo get_phrase('Paid'); ?></div></th>
                <th><div><?php echo get_phrase('Due'); ?></div></th>
            </tr>
        </thead>
        <tfoot>
            <td><p class="text text-success">Total</p></td>
            <td></td>
            <td><p class="text-success"><?php echo $this->crud_model->get_total_fee_from_database($course_id,$batch_id,$student_id);?></p></td>
            <td><p class="text-success"><?php echo $this->crud_model->get_total_paid_from_database($course_id,$batch_id,$student_id);?></p></td>
            <td><p class="text-success"><?php echo $this->crud_model->get_total_due_from_database($course_id,$batch_id,$student_id);?></p></td>
        </tfoot>
        <tbody>
            <?php
            $this->db->where(array(
                'course_id' => $course_id,
                'batch_id' => $batch_id
            ));
            $courses = $this->db->get('a1')->result_array();
            foreach ($courses as $row): ?>
                <tr>
                   <td><?php echo $row['student_id']; ?></td>
                   <td><?php echo $this->crud_model->get_type_name_by_id('student', $row['student_id']); ?></td>
                   <td><?php echo $row['total_amount']?></td>
                   <td><?php $paid_amount = $this->crud_model->get_individual_paid_student($row['a1_id']); echo $paid_amount?></td>
                   <td><?php echo $row['total_amount'] - $paid_amount; ?></td>
               </tr>
           <?php endforeach; ?>
       </tbody>
       <script type="text/javascript">
        jQuery(document).ready(function ($)
        {
            var datatable = $("#table_export").dataTable({
                "sPaginationType": "bootstrap",
                "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
                "oTableTools": {
                    "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [2]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [2]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(1, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(1, true);
                                }
                            });
                        },
                    },
                    ]
                },
            });
            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });
    </script>