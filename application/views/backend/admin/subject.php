<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('subject_list'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_subject'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">            
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            <th><div><?php echo get_phrase('course'); ?></div></th>
                            <th><div><?php echo get_phrase('subject_name'); ?></div></th>
                            <th><div><?php echo get_phrase('category'); ?></div></th>
                            <th><div><?php echo get_phrase('trainer'); ?></div></th>
                            <th><div><?php echo get_phrase('options'); ?></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = 1;
                        foreach ($subjects as $row):
                            ?>
                            <tr>
                                <td><?php echo $this->crud_model->get_type_name_by_id('course', $row['course_id']); ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $this->crud_model->get_type_name_by_id('subject_category', $row['subject_category_id']); ?></td>
                                <td><?php echo $this->crud_model->get_type_name_by_id('teacher', $row['teacher_id']); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                            <!-- EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_subject/<?php echo $row['subject_id']; ?>');">
                                                    <i class="entypo-pencil"></i>
                                                    <?php echo get_phrase('edit'); ?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>

                                            <!-- DELETION LINK -->
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/subject/delete/<?php echo $row['subject_id']; ?>/<?php echo $course_id; ?>');">
                                                    <i class="entypo-trash"></i>
                                                    <?php echo get_phrase('delete'); ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url() . 'index.php?admin/subject/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('course'); ?></label>
                            <div class="col-sm-5">
                                <select name="course_id" class="form-control" style="width:100%;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                    <?php
                                    $courses = $this->db->get('course')->result_array();
                                    foreach ($courses as $row):
                                        ?>
                                        <option value="<?php echo $row['course_id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('category'); ?></label>
                            <div class="col-sm-5">
                                <select name="subject_category_id" class="form-control" style="width:100%;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                    <?php
                                    $subject_categories = $this->db->get('subject_category')->result_array();
                                    foreach ($subject_categories as $row):
                                        ?>
                                        <option value="<?php echo $row['subject_category_id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('trainer'); ?></label>
                            <div class="col-sm-5">
                                <select name="teacher_id" class="form-control" style="width:100%;">
                                    <?php
                                    $teachers = $this->db->get('teacher')->result_array();
                                    foreach ($teachers as $row):
                                        ?>
                                        <option value="<?php echo $row['teacher_id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('full_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="fullmark" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('pass_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('theory_full_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="theoryfullmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('theory_pass_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="theorypassmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('subjective_full_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="subjectivefullmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('subjective_pass_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="subjectivepassmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('objective_full_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="objectivefullmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('theory_pass_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="theorypassmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('practical_full_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="practicalfullmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('practical_pass_mark'); ?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="practicalpassmark" placeholder="leave blank if not applicable" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('add_subject'); ?></button>
                        </div>
                    </div>
                    </form>                
                </div>                
            </div>
            <!----CREATION FORM ENDS-->

        </div>
    </div>
</div>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(4, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(4, true);
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>