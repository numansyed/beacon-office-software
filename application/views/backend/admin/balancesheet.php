<br><br>
<div class="col-lg-12">
    <?php echo form_open(base_url() . 'index.php?admin/balancesheetmonthly', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="col-sm-3">
                <input type="text" class="form-control datepicker" name="datefrom" value="" data-start-view="3">
            </div> 
            <div class="col-sm-4 ">
                <button type="submit" class="btn btn-info">Monthly Balance Sheet</button>
            </div>
        </div>      
    </div>

    <?php echo form_close(); ?>
    <?php echo form_open(base_url() . 'index.php?admin/balancesheetdaily', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="col-sm-3">
                <input type="text" class="form-control datepicker" name="datefrom" value="" data-start-view="3">
            </div> 
            <div class="col-sm-4 ">
                <button type="submit" class="btn btn-info">Daily Balance Sheet</button>
            </div>
        </div>

    </div>

    <?php echo form_close(); ?>
</div>

<br/>
<br/>
<br/>
<div class="col-md-6">
    <table class="table table-bordered datatable" id="table_export">
        <thead>
            <tr>
                <th><div>#</div></th>
        <th><div>Date</div></th>
        <th><div>Income</div></th>
        </tr>
        </thead>
        <tfoot>
        <td></td>
        <td>Total Income</td>
        <td><?php
            //	$length = $this->db->count_all('payment');
            $this->db->where('payment_type', 'income');
            $this->db->order_by('timestamp', 'desc');
            $result = $this->db->get('payment')->result_array();
            $total = 0;
            foreach ($result as $row):
                      if(date('M,Y',$row['timestamp'])==date('M,Y',$monthfrom)):
			$total = $total + $row['amount'];
                                                      endif;
            endforeach;
            echo $total;
            ?></td>
        </tfoot>
        <tbody>
            <?php
            $count = 1;
            $this->db->where('payment_type', 'income');
            $this->db->order_by('timestamp', 'desc');
            $payments = $this->db->get('payment')->result_array();
            foreach ($payments as $row):
                     if(date('M,Y',$row['timestamp'])==date('M,Y',$monthfrom)):
                ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo date('d M,Y', $row['timestamp']); ?></td>  
                    <td><?php echo $row['amount']; ?></td>
                </tr>
                <?php   endif;?>       
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="col-md-6">
    <table class="table table-bordered datatable" id="table_export1">
        <thead>
            <tr>
                <th><div>#</div></th>
        <th><div>Date</div></th>
        <th><div>Expense</div></th>
        </tr>
        </thead>
        <tfoot>
            <tr>
                <td></td>
                <td>Total Expense</td>
                <td><?php
            //	$length = $this->db->count_all('payment');
            $this->db->where('payment_type', 'expense');
            $this->db->order_by('timestamp', 'desc');
            $result = $this->db->get('payment')->result_array();
            $etotal = 0;
            foreach ($result as $row):
                     if(date('M,Y',$row['timestamp'])==date('M,Y',$monthfrom)):
			$etotal = $eotal + $row['amount'];
                                                      endif;
            endforeach;
            echo $etotal;
            ?></td>
            </tr> 
            <tr>
                <td></td>
                <td>Balance</td>
                <td><?php
                    $btotal = $total - $etotal;
                    echo $btotal;
            ?></td>
            </tr>

        </tfoot>
        <tbody>
<?php
$count = 1;
$this->db->where('payment_type', 'expense');
$this->db->order_by('timestamp', 'desc');
$payments = $this->db->get('payment')->result_array();
foreach ($payments as $row):
    if(date('M,Y',$row['timestamp'])==date('M,Y',$monthfrom)):
    ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo date('d M,Y', $row['timestamp']); ?></td>  
                    <td><?php echo $row['amount']; ?></td>
                </tr>
                <?php   endif;?>       
<?php endforeach; ?>
        </tbody>
    </table>
</div>
<br>
<br>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(6, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(6, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });


    jQuery(document).ready(function ($)
    {


        var datatable = $("#table_export1").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(6, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(6, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>

