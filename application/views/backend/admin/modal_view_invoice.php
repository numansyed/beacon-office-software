<style type="text/css">
    #invoice_print h5{margin:0;}
    .amount-class tr {margin: 0;padding: 0;}
</style>
<?php
$edit_data = $this->db->get_where('a1', array('a1_id' => $param2))->result_array();
foreach ($edit_data as $row):
    $paid = 0;
    $due = 0;

    $this->db->order_by('date', 'desc');
    $payments = $this->db->get_where('a8', array(
        'a1_id' => $param2
    ))->result_array();
    $date = $payments[0]['date'];

    foreach ($payments as $row2) {
        $paid += floatval($row2['amount']);
    }

    $due = floatval($row['total_amount']) - $paid;
?>
<center>
    <a onClick="PrintElem('#invoice_print')" class="btn btn-default btn-icon icon-left hidden-print pull-right">
        Print Invoice
        <i class="entypo-print"></i>
    </a>
</center>

    <br><br>

    <div id="invoice_print">
        <table width="100%" border="0">
            <tr>
                 <td align = "left"><img src="uploads/logo.png" height="60" alt="" /></td>
                <td align="right">
                    <h5><?php echo get_phrase('creation_date'); ?> : <?php echo $row['date'];?></h5>
                    <h5><?php echo get_phrase('receipt_no'); ?> : <?php echo $row['a1_id'];?></h5>
                    <h5><?php echo get_phrase('type'); ?> : <?php echo $this->db->get_where('a2', array('a2_id' => $row['a2_id']))->row()->name;?></h5>
                    <h5><?php echo get_phrase('description'); ?> : <?php echo $this->db->get_where('a7', array('a7_id' => $row['a7_id']))->row()->details;?></h5>
                </td>
            </tr>
        </table>
        <hr>
        <table width="100%" border="0">    
            <tr>
                <td align="left"><h4><?php echo get_phrase('payment_to'); ?> </h4></td>
                <td align="right"><h4><?php echo get_phrase('bill_to'); ?> </h4></td>
            </tr>

            <tr>
                <td align="left" valign="top">
                    <?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?><br>
                    <?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?><br>
                    <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>            
                </td>
                <td align="right" valign="top">
                    <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name; ?><br>
                    <?php 
                        $course_id = $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->course_id;
                        echo get_phrase('course') . ' ' . $this->db->get_where('course', array('course_id' => $course_id))->row()->name;
                    ?><br>
                     <?php 
                        $batch_id = $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->batch_id;
                        echo get_phrase('batch') . ' ' . $this->db->get_where('batch', array('batch_id' => $batch_id))->row()->name;
                    ?><br>
                    <?php echo 'Roll - ' .  $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->roll; ?><br>
                </td>
            </tr>
        </table>
        <hr>

        <table class="amount-class" width="100%" border="0">    
            <tr>
                <td align="right" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align="right"><?php echo $row['total_amount']; ?></td>
            </tr>
            <tr>
                <td align="right" width="80%"><h4><?php echo get_phrase('paid_amount'); ?> :</h4></td>
                <td align="right"><h4><?php echo $paid; ?></h4></td>
            </tr>          
             <?php //if ($row['due'] != 0):?> <tr>
                <td align="right" width="80%"><h4><?php echo get_phrase('due'); ?> :</h4></td>
                <td align="right"><h4><?php echo $due; ?></h4></td>
            </tr><?php //endif;?>
        </table>

        <hr>

        <!-- payment history -->
        <h4><?php echo get_phrase('payment_history'); ?></h4>
        <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <th><?php echo get_phrase('date'); ?></th>
                    <th><?php echo get_phrase('amount'); ?></th>
                    <th><?php echo get_phrase('method'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $payment_history = $this->db->get_where('a8', array('a1_id' => $row['a1_id']))->result_array();
                foreach ($payment_history as $row2):
                    ?>
                    <tr>
                        <td><?php echo $row2['date']; ?></td>
                        <td align="right"><?php echo $row2['amount']; ?></td>
                        <td>
                            <?php 
                                if ($row2['method'] == 1)
                                    echo get_phrase('cash');
                                if ($row2['method'] == 2)
                                    echo get_phrase('check');
                                if ($row2['method'] == 3)
                                    echo get_phrase('card');
                                if ($row2['method'] == 'paypal')
                                    echo 'paypal';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tbody>
        </table>
    </div>
<?php endforeach; ?>


<script type="text/javascript">

    // print invoice function
    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'invoice', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Invoice</title>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css" type="text/css" />');
        mywindow.document.write('</head><style type="text/css">table>tr{margin:0 !important;padding: 0 !important;} td>h5{margin:0 !important; padding:0 !important;}td>h4{margin:0 !important; padding:0 !important;}#invoice_print h5{margin:0 !important;}.amount-class tr {margin: 0 !important;padding: 0 !important;}</style><body style="margin:0;padding:0;">');
        mywindow.document.write(data);
        mywindow.document.write('</td></table><br><br><p>Authorized Signature</p></div>');
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>