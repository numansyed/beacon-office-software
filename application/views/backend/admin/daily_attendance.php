<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th>Date</th>
        <th>Type</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    <form method="post" action="<?php echo base_url(); ?>index.php?admin/daily_attendance" class="form">
        <tr class="gradeA">
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker" name="date" value="<?php echo $month . '/' . $day . '/' . $year; ?>" data-start-view="3">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-sm-12">
                        <select name="type"  class="form-control" onchange="show_courses(this.value)">
                            <option value="student" <?php if ($type == 'student') echo 'selected="selected"'; ?>>Student</option>
                            <option value="employee" <?php if ($type == 'employee') echo 'selected="selected"'; ?>>Employee</option>
                        </select>
                    </div>
                    <div class="col-sm-12" id="courses" style="visibility: <?php if ($type == 'student') {echo 'visible';} else {echo 'hidden';} ?>">
                        <select name="course_id"  class="form-control">
                            <option value="all" <?php if (isset($course_id) && $course_id == 'all') echo 'selected="selected"'; ?>>All</option>
                            <?php
                            $courses = $this->db->get('course')->result_array();
                            foreach ($courses as $row):
                                ?>
                                <option value="<?php echo $row['course_id']; ?>"
                                    <?php if (isset($course_id) && $course_id == $row['course_id']) echo 'selected="selected"'; ?>>
                                    <?php echo $row['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </td>
            <td><input type="submit" value="<?php echo get_phrase('Check'); ?>" class="btn btn-info"/></td>
        </tr>
    </form>
    </tbody>
</table>
<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <?php if($type == 'student'): ?>
                <th width="80"><div>Roll</div></th>
            <?php endif; ?>
            <th width="80"><div>ID</div></th>
            <th><div><?php echo get_phrase('name'); ?></div></th>
            <?php if($type == 'student'): ?>
                <th width="80"><div><?php echo get_phrase('phone'); ?></div></th>
            <?php endif; ?>
            <th><div><?php echo get_phrase('status'); ?></div></th>
            <th><div><?php echo get_phrase('time'); ?></div></th>
            <?php if ($type == 'employee'):?>
                <th><div><?php echo get_phrase('details'); ?></div></th>
            <?php endif;?>
</tr>
</thead>
<tbody>
    <?php
    foreach ($attendance as $row):
        ?>
        <tr>
            <?php if($type == 'student'): ?>
                <td><?php echo $row['roll']; ?></td>
            <?php endif; ?>
            <td><?php if($type == 'employee') {echo $row['teacher_id'];}else {echo $row['student_id'];} ?></td>
            <td><div><?php echo $row['name']; ?></div></td>
            <?php if($type == 'student'): ?>
                <td><?php echo $this->crud_model->get_type_name_by_id('parent', $row['parent_id'], 'phone'); ?></td>
            <?php endif; ?>
            <td align="center"><?php
				if ($row['status'] == 0) {
					echo '<span class="badge badge-danger">' . get_phrase('absent') . '</span></td><td>N/A</td>';
				}
				else if ($row['status'] == 1) {
					echo '<span class="badge badge-success">' . get_phrase('In') . '</span></td><td>' . date('g:i a',strtotime($row['time']));
				}
				else if ($row['status'] == 2) {
					echo '<span class="badge badge-danger">' . get_phrase('Out') . '</span></td><td>' . date('g:i a',strtotime($row['time']));
				}
			?>
		</td>
        <?php if ($type == 'employee'):?>
            <td>
                <a class="btn btn-info" href="#" onclick="showAjaxModal('<?php echo base_url() . 'index.php?modal/popup/modal_attendance_detail/' . $type . '/' . $month . '/' . $day . '/' . $year . '/' . $row['employee_id']; ?>');">
                    <?php echo get_phrase('details'); ?>
                </a>
            </td>
        <?php endif;?>
        </tr>
<?php endforeach; ?>
</tbody>
</table>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3<?php if ($type == 'student') echo ', 4, 5'; ?>]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3<?php if ($type == 'student') echo ', 4, 5'; ?>]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            //datatable.fnSetColumnVis(0, false);
                            <?php if ($type == 'employee') echo 'datatable.fnSetColumnVis(4, false);'; ?>
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    //datatable.fnSetColumnVis(0, true);
                                    <?php if ($type == 'employee') echo 'datatable.fnSetColumnVis(4, true);'; ?>
                                }
                            });
                        },
                    },
                ]
            },
        });
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
    function show_courses(type) {
        try {
            document.getElementById('courses').style.visibility = 'hidden';
            //document.getElementById('student_id_' + i).setAttribute("name", "temp");
        }
        catch (err) {
        }

        if (type == 'student') {
            document.getElementById('courses').style.visibility = 'visible';
            //document.getElementById('courses').setAttribute("name", "student_id");
        }
    }
</script>