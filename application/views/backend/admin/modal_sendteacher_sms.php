<?php
$edit_data = $this->db->get_where('teacher', array('teacher_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('Send_SMS_to');?>&nbsp;::&nbsp;<?php   echo  $row['name']; ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <?php echo form_open(base_url().'index.php?admin/sms/teacher/'.$row['teacher_id'], array('class' => 'form-horizontal form-groups-bordered validate'));?>
                  
                   <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo get_phrase('message');?></label>
                                <div class="col-sm-10">
                                    <div class="box closable-chat-box">
                                        <div class="box-content padded">
                                                <div class="chat-message-box">
                                                <textarea name="message" id="ttt" rows="8" placeholder="<?php echo get_phrase('type_your_message_here');?>" class="form-control"></textarea>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="col-sm-2 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Send">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
<?php endforeach; ?>