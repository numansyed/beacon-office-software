<div class="row">
    <div class="col-md-12">
        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
                    <?php echo get_phrase('invoice/payment_list'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_invoice/payment'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table  class="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            
                    <th><div>Invoice ID#</div></th>
                    <th><div><?php echo get_phrase('student'); ?></div></th>
                    <th><div><?php echo get_phrase('course_&_batch'); ?></div></th>
                    <th><div><?php echo get_phrase('Category'); ?></div></th>
                    <th><div>Title</div></th>
                    <th><div><?php echo get_phrase('total'); ?></div></th>
                    <th><div><?php echo get_phrase('paid'); ?></div></th>
                    <th><div><?php echo get_phrase('status'); ?></div></th>
                    <th><div><?php echo get_phrase('date'); ?></div></th>
                    <th><div><?php echo get_phrase('options'); ?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $this->db->from($this->invoice);
                        $this->db->order_by("invoice_id", "desc");
                        $invoices = $this->db->get('invoice')->result_array();
                        foreach ($invoices as $row): ?>
                            <tr>
                                 <td><?php echo $row['invoice_id']; ?></td>
                                <td><?php echo $this->crud_model->get_type_name_by_id('student', $row['student_id']); ?></td>
                                <td>
                                    <?php
                                    $courseid = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->course_id;
                                    echo $this->db->get_where('course', array('course_id' => $courseid))->row()->name;
                                    ?>
                                    /
                                    <?php
                                    $batchid = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->batch_id;
                                    echo $this->db->get_where('batch', array('batch_id' => $batchid))->row()->name;
                                    ?>
                                </td>
                                <td><?php if ($row['income_category_id'] != 0 || $row['income_category_id'] != '') echo $this->db->get_where('income_category', array('income_category_id' => $row['income_category_id']))->row()->name; ?></td>
                                <td><?php echo $row['title']; ?></td>
                                <td><?php echo $row['amount']; ?></td>
                                <td><?php echo $row['amount_paid']; ?></td>
                                <td>
                                    <span class="label label-<?php
                                    if ($row['due'] == 0)
                                        echo 'success';
                                    else
                                        echo 'danger';
                                    ?>"><?php
                                              if ($row['due'] == 0)
                                                  echo 'paid';
                                              else
                                                  echo'has due';
                                              ?></span>
                                </td>
                                <td><?php echo date('d M,Y', $row['creation_timestamp']); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">
    <?php if ($row['due'] != 0): ?>
                                                <li>
                                                    <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_take_payment/<?php echo $row['invoice_id']; ?>');">
                                                        <i class="entypo-bookmarks"></i>
        <?php echo get_phrase('take_payment'); ?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
    <?php endif; ?>
                                            <!-- VIEWING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_view_invoice/<?php echo $row['invoice_id']; ?>');">
                                                    <i class="entypo-credit-card"></i>
    <?php echo get_phrase('view_invoice'); ?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                              <!---- SENT SMS----->
                                    <?php
$active_sms_service = $this->db->get_where('settings', array(
            'type' => 'active_sms_service'
        ))->row()->description;
 if ($active_sms_service == 'robi' || $active_sms_service == 'clickatell' || $active_sms_service == 'twilio' ): 
?>
                                        <li>
                                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_invoice_sms/<?php echo $row['invoice_id'];?>/student');">
                                                <i class="entypo-mail"></i>Send Payment Confirmation SMS</a>
                                        </li>
              <?php endif;?>                         
                                        </ul>
                                    </div>
                                </td>
                            </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->
            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
<?php echo form_open(base_url() . 'index.php?admin/invoice/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title"><?php echo get_phrase('invoice_informations'); ?></div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Invoice for</label>
                                    <div class="col-sm-10">
                                        <select name="income_category_id" class="form-control" data-validate="required" id="income_category_id" 
                                                data-message-required="<?php echo get_phrase('value_required'); ?>"
                                                   onchange="return selectsetup(this.value);">
                                            <option value=""><?php echo get_phrase('select'); ?></option>
                                            <?php
                                            $icategories = $this->db->get('income_category')->result_array();
                                            foreach ($icategories as $row):
                                                $incomecat = $row['income_category_id'];
                                                ?>
                                                <option value="<?php echo $row['income_category_id']; ?>">
                                                <?php echo $row['name']; ?>
                                                </option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="student_holder">
                                       <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label"><?php echo get_phrase('course'); ?></label>
                                    <div class="col-sm-5">
                                        <select name="course_id" class="form-control" <?php if($incomecat == 4 || $incomecat == 1) echo "required"?>
                                                onchange="return get_batch_name(this.value)&get_student_list(this.value);">
                                            <option value=""><?php echo get_phrase('select'); ?></option>
                                            <?php
                                            $courses = $this->db->get('course')->result_array();
                                            foreach ($courses as $row):
                                                ?>
                                                <option value="<?php echo $row['course_id']; ?>">
                                                <?php echo $row['name']; ?>
                                                </option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div> 
                                      <div class="col-sm-5">
                                        <select name="batch_id" class="form-control" <?php if($incomecat == 4 || $incomecat == 1) echo "required"?>
                                               id ="batch_selector_holder">
                                            <option value="">batch</option>

                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Student & Roll</label>
                                    <div class="col-sm-10">
                                        <select name="student_id" class="form-control" style="" id="student_selector_holder" <?php if(isset($incomecat) && $incomecat == 4 ||$incomecat == 1) echo "required"?>
                                              >
                                            <option value="">select course First</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                
                                <div class="form-group" id="exam_holder">
                                    <label for="field-2" class="col-sm-2 control-label">Exam & Date</label>
                                    <div class="col-sm-10">
                                         <select name="exam_id" class="form-control" data-validate="required" id="exam_selector_holder"
                                                 data-message-required="<?php echo get_phrase('value_required'); ?>" <?php if(isset($incomecat) && $incomecat == 1) echo "required"?>
                                                onchange="return checkinfo(this.value);">
                                            <option value="">Select Exam</option>
                                          <?php
                                            $exam = $this->db->get('exam')->result_array();
                                            foreach ($exam as $row):
                                                ?>
                                                <option value="<?php echo $row['exam_id']; ?>">
                                                <?php echo $row['name']; ?>------<?php echo $row['date'];?>
                                                </option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>                                      
                                </div>
                                <div class="form-group" id="fees_holder">
                                    <label for="field-2" class="col-sm-2 control-label">Fees for</label>
                                    <div class="col-sm-5">
                                        <select name="month" id= "feemonth"class="form-control" <?php if(isset($incomecat) && $incomecat == 4) echo "required"?>
                                                onchange="checkmonthlyfees1(this.value);">
                        	<?php 
							for($i=0;$i<=12;$i++):
								if($i==0)$m='select month';
                                                                                                                                               else if($i==1)$m='january';
								else if($i==2)$m='february';
								else if($i==3)$m='march';
								else if($i==4)$m='april';
								else if($i==5)$m='may';
								else if($i==6)$m='june';
								else if($i==7)$m='july';
								else if($i==8)$m='august';
								else if($i==9)$m='september';
								else if($i==10)$m='october';
								else if($i==11)$m='november';
								else if($i==12)$m='december';
							?>
                            	<option value="<?php echo $i;?>"
                                	<?php if($month==$i)echo 'selected="selected"';?>>
										<?php echo $m;?>
                                        	</option>
                            <?php 
							endfor;
							?>
                        </select>
                                    </div>    
                                    <div class="col-sm-5">
                                        <select name="year" id ="feeyear"class="form-control" onchange="checkmonthlyfees(this.value);">
                                                <?php for($i=2016;$i<=2020;$i++):?>
                                                <option value="<?php echo $i;?>"
                                                        <?php if(isset($year) && $year==$i)echo 'selected="selected"';?>>
                                                                                                <?php echo $i;?>
                                                                </option>
                                            <?php endfor;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo get_phrase('title'); ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo get_phrase('description'); ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="description" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo get_phrase('date'); ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="datepicker form-control" name="date" required/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="paymentinfo">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title"><?php echo get_phrase('payment_informations'); ?></div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('total'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="amount"
                                               placeholder="<?php echo get_phrase('enter_total_amount'); ?>" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('payment'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="amount_paid"
                                               placeholder="<?php echo get_phrase('enter_payment_amount'); ?>" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                                    <div class="col-sm-9">
                                        <select name="status" class="form-control" required>
                                            <option value="paid"><?php echo get_phrase('paid'); ?></option>
                                            <option value="unpaid"><?php echo get_phrase('unpaid'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo get_phrase('method'); ?></label>
                                    <div class="col-sm-9">
                                        <select name="method" class="form-control" required>
                                            <option value="1"><?php echo get_phrase('cash'); ?></option>
                                            <option value="2"><?php echo get_phrase('check'); ?></option>
                                            <option value="3"><?php echo get_phrase('card'); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('add_invoice'); ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="exam_label">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title"><?php echo get_phrase('payment_informations'); ?></div>
                            </div>
                         
                            <div class="panel-body">
                              <h1 class="alert alert-danger">Selected Student Has already paid this fee!</h1>
                            </div>
                        </div>
                        
                    </div>
                </div>
<?php echo form_close(); ?>
            </div>
            <!----CREATION FORM ENDS-->
        </div>
    </div>
</div>
<script type="text/javascript">
   $(function(){
        $("#exam_holder").hide();
        $("#fees_holder").hide();
        $("#student_holder").hide();
        $("#exam_label").hide();
                $("#fees_label").hide();
    });
  
       function selectsetup(income_category_id) {
    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/incomesetup/' + income_category_id ,
            success: function(response)
            {
                if( response === '1'){
                $("#exam_holder").show();
                $("#student_holder").show();
                $("#fees_holder").hide();
            }
                if( response === '4'){
                $("#fees_holder").show();
                $("#student_holder").show();
                $("#exam_holder").hide();
            }
               if( response === '0'){
                $("#student_holder").show();
                   $("#fees_holder").hide();
                      $("#exam_holder").hide();
            }
            }
        });

    }
     function get_batch_name(course_id) {
    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_batch_name/' + course_id ,
            success: function(response)
            {
                jQuery('#batch_selector_holder').html(response);
                
            }
        });

    }
    function get_student_list(course_id) {

    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_student_list/' + course_id ,
            success: function(response)
            {
                jQuery('#student_selector_holder').html(response);
            }
        });

    }
      function checkinfo(exam_id) {
       var student_id = document.getElementById("student_selector_holder").value;
       
    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/examinfo/' + exam_id +'/' + student_id ,
            success: function(response)
            {
                console.log(response);
                if(response === '1'){
                $('#paymentinfo').hide();
                $("#exam_label").show();
            }
            if(response === '0'){
                 $('#paymentinfo').show();
                  $("#exam_label").hide();
            }
            }
        });

    }
     function checkmonthlyfees(year) {
       var month = document.getElementById("feemonth").value;       
       var student_id = document.getElementById("student_selector_holder").value;
    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/checkmonthlyfees/' + month+'-'+year+'/'+student_id ,
            success: function(response)
            {
                
                if(response === '1'){
                $('#paymentinfo').hide();
                $("#exam_label").show();
            }
            if(response === '0'){
                 $('#paymentinfo').show();
                  $("#exam_label").hide();
            }
            }
        });

    }
    
     function checkmonthlyfees1(month) {
       var year = document.getElementById("feeyear").value;       

       var student_id = document.getElementById("student_selector_holder").value;
    
    	$.ajax({
            url: '<?php echo base_url();?>index.php?admin/checkmonthlyfees/' + month+'-'+year+'/'+student_id ,
            success: function(response)
            {
                if(response === '1'){
                $('#paymentinfo').hide();
                $("#exam_label").show();
            }
            if(response === '0'){
                 $('#paymentinfo').show();
                  $("#exam_label").hide();
            }
            }
        });

    }
</script>