<?php

/**
 * Created by Gobinda Joy
 * Date: 7/10/2016
 * Time: 4:51 AM
 * Version: 1.0
 */
class cl_grade {

    public $id = 0;
    public $name = '';
    public $point = 0;
    public $markfrom = 0;
    public $markupto = 0;
    public $formarks = 0;
    public $comment = '';

    public function getgrade($mark) {
        if (round(floatval($mark), 0) >= $this->markfrom && round(floatval($mark), 0) <= $this->markupto)
            return $this->name;

        return false;
    }

    public function getpoint($mark, $fullmarks) {
        if (!empty($fullmarks)) {
            if (round(floatval($mark), 0) >= $this->markfrom && round(floatval($mark), 0) <= $this->markupto && round(floatval($fullmarks), 0) == $this->formarks)
                return number_format((float) $this->point, 2, '.', '');
        }
        else {
            if (round(floatval($mark), 0) >= $this->markfrom && round(floatval($mark), 0) <= $this->markupto)
                return number_format((float) $this->point, 2, '.', '');
        }


        return false;
    }

}

class cl_subject {

    public $id = 0;
    public $name = '';
    public $shortname = '';
    public $code = '';
    public $fullmark = 0;
    public $passmark = 0;
    public $theoryfullmark = 0;
    public $theorypassmark = 0;
    public $subjectivefullmark = 0;
    public $subjectivepassmark = 0;
    public $objectivefullmark = 0;
    public $objectivepassmark = 0;
    public $practicalfullmark = 0;
    public $practicalpassmark = 0;
    public $mark = 0;
    public $theorymark = 0;
    public $subjectivemark = 0;
    public $objectivemark = 0;
    public $practicalmark = 0;
    public $enabled = true;
    public $grades = array();
    private $ret = 0;

    public function addgrade($grade) {
        $this->grades[] = $grade;
    }

    public function __get($prop) {
        if (strtolower($prop) == 'total') {
            if ($this->isAbsent)
                return 0;

            $ret = floatval($this->mark) +
                    floatval($this->theorymark) +
                    floatval($this->practicalmark) +
                    floatval($this->subjectivemark) +
                    floatval($this->objectivemark);

            return number_format((float) $ret, 2, '.', '');
        } else if (strtolower($prop) == 'grade') {
            if (!$this->enabled)
                return 'N/A';

            if ($this->isFailed || $this->isAbsent)
                return 'F';

            foreach ($this->grades as $grade)
                if ($grade->getgrade($this->total) !== FALSE)
                    return $grade->getgrade($this->total);

            return 'N/A';
        } else if (strtolower($prop) == 'gradepoint') {
            if (!$this->enabled || $this->isFailed || $this->isAbsent)
                return number_format((float) $this->ret, 2, '.', '');

            foreach ($this->grades as $grade) {
                if ($grade->getpoint($this->total, $this->fullmark) !== FALSE) {
                    return number_format((float) $grade->getpoint($this->total, $this->fullmark), 2, '.', '');
                }
            }
            return number_format((float) $this->ret, 2, '.', '');
        } else if (strtolower($prop) == 'isabsent') {
            if (!$this->enabled)
                return false;

            if ((strtolower($this->mark) == 'a') ||
                    (strtolower($this->theorymark) == 'a') ||
                    (strtolower($this->practicalmark) == 'a') ||
                    (strtolower($this->subjectivemark) == 'a') ||
                    (strtolower($this->objectivemark) == 'a'))
                return true;

            return false;
        } else if (strtolower($prop) == 'isfailed') {
            if (!$this->enabled)
                return false;

            if ((floatval($this->mark) < floatval($this->passmark)) ||
                    (floatval($this->theorymark) < floatval($this->theorypassmark)) ||
                    (floatval($this->practicalmark) < floatval($this->practicalpassmark)) ||
                    (floatval($this->subjectivemark) < floatval($this->subjectivepassmark)) ||
                    (floatval($this->objectivemark) < floatval($this->objectivepassmark)))
                return true;

            return false;
        }/* else if(strtolower($prop) == 'theorypractical') {
          if (!$this->enabled)
          return false;

          if ($this->theorypassmark > 0 || $this->practicalpassmark > 0)
          return true;

          return false;
          } */ else {
            die('Invalid member access: ' . $prop);
        }
    }

}

class cl_student {

    public $id = 0;
    public $roll = '';
    public $examid = 0;
    public $courseid = 0;
    public $batchid = 0;
    public $departmentid = 0;
    public $session = '';
    public $subjects = array();
    public $grades = array();
    public $examtype = 0;
    public $prevexamtotal = 0;
    private $ret = 0;

    public function addgrade($grade) {
        $this->grades[] = $grade;
    }

    public function addsubject($sub) {
        $this->subjects[] = $sub;
    }

    private function cmpa($a, $b) {
        if ($a->point == $b->point) {
            return 0;
        }
        return ($a->point < $b->point) ? -1 : 1;
    }

    private function cmpd($a, $b) {
        if ($a->point == $b->point) {
            return 0;
        }
        return ($a->point < $b->point) ? 1 : -1;
    }

    public function __get($prop) {
        if (strtolower($prop) == 'result') {
            $_tsa = 0;
            $_tsf = 0;
            $_tsc = count($this->subjects);

            foreach ($this->subjects as $subject) {
                if ($subject->isAbsent)
                    $_tsa++;

                if ($subject->isFailed)
                    $_tsf++;
            }

            if ($_tsc == $_tsa)
                return 'Absent';

            if ($_tsc == $_tsf)
                return 'Failed';

            if ($_tsf > 0 || $_tsa > 0)
                return 'Failed';

            return 'Passed';
        } else if (strtolower($prop) == 'result_single') {
            $_tsa = 0;
            $_tsf = 0;
            $_tsc = count($this->subjects);

            foreach ($this->subjects as $subject) {
                if ($subject->isAbsent)
                    $_tsa++;

                if ($subject->isFailed)
                    $_tsf++;
            }

            if ($_tsc == $_tsa)
                return 'A';

            if ($_tsc == $_tsf)
                return 'F';

            if ($_tsf > 0 || $_tsa > 0)
                return 'F';

            return 'P';
        } else if (strtolower($prop) == 'totalfailed') {
            $_tsf = 0;
            foreach ($this->subjects as $subject)
                if ($subject->isFailed)
                    $_tsf++;

            return $_tsf;
        } else if (strtolower($prop) == 'totalabsent') {
            $_tsa = 0;
            foreach ($this->subjects as $subject)
                if ($subject->isAbsent)
                    $_tsa++;

            return $_tsa;
        } else if (strtolower($prop) == 'total') {
            $_gt = 0;

            foreach ($this->subjects as $subject) {
                $_gt += $subject->total;
            }

            return number_format((float) $_gt, 2, '.', '');
        } else if (strtolower($prop) == 'avgtotal') {
            $_gt = 0;
            foreach ($this->subjects as $subject)
                $_gt += $subject->total;

            $_gt += $this->prevexamtotal;
            $_avg = $_gt / 3;

            return number_format((float) $_avg, 2, '.', '');
        } else if (strtolower($prop) == 'totalpoints') {
            $_gp = 0;

            foreach ($this->subjects as $sub)
                $_gp += $sub->gradepoint;

            return number_format((float) $_gp, 2, '.', '');
        } else if (strtolower($prop) == 'grade') {
            if ($this->result == 'Failed')
                return 'F';

            $_tsc = count($this->subjects);

            usort($this->grades, "self::cmpd");

            if ($this->examtype == 3) {
                $_gt = $this->avgtotal / $_tsc;
                foreach ($this->grades as $grade)
                    if ($grade->getgrade($_gt) !== FALSE)
                        return $grade->getgrade($_gt);
            } else {
                $_gp = round(($this->totalpoints / $_tsc), 2);
                for ($i = 0; $i < count($this->grades); $i++) {
                    if ($_gp >= $this->grades[$i]->point)
                        return $this->grades[$i]->name;
                }
            }
            return 'N/A';
        } else if (strtolower($prop) == 'gpa') {
            if ($this->result == 'Failed')
                return number_format((float) $this->ret, 2, '.', '');

            if ($this->examtype == 3) {
                $_tsc = count($this->subjects);
                $_gt = $this->avgtotal / $_tsc;
                foreach ($this->grades as $grade)
                    if ($grade->getpoint($_gt) !== FALSE)
                        return $grade->getpoint($_gt);
            } else {
                $_tsc = count($this->subjects);
                return number_format((float) round(($this->totalpoints / $_tsc), 2), 2, '.', '');
            }
            return number_format((float) $this->ret, 2, '.', '');
        } else if (strtolower($prop) == 'remark') {
            if ($this->result == 'Passed')
                foreach ($this->grades as $grade)
                    if ($this->grade == $grade->name)
                        return $grade->comment;

            $_rf = 'Failed in ';
            $_rfs = '';
            $_ra = 'Absent in ';
            $_ras = '';

            foreach ($this->subjects as $subject) {
                if ($subject->isFailed)
                    $_rfs .= (strlen($_rfs) > 0 ? ', ' : '') . $subject->name;

                if ($subject->isAbsent)
                    $_ras .= (strlen($_ras) > 0 ? ', ' : '') . $subject->name;
            }
            return (strlen($_rfs) > 0 ? $_rf . $_rfs : '') . (strlen($_rfs) > 0 ? (strlen($_ras) > 0 ? ' and ' : '.') : '') . (strlen($_ras) > 0 ? $_ra . $_ras . '.' : '');
        } else {
            die('Invalid member access: ' . $prop);
        }
    }

}

?>