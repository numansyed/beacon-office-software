<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// error_reporting(E_ALL);


class Admin extends CI_Controller {  

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');

        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*     * *default functin, redirects to login page if no admin logged in yet** */

    public function index() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
    }

    /*     * *ADMIN DASHBOARD** */
    
    function dashboard() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('admin_dashboard');
        $this->load->view('backend/index', $page_data);
    }

    /*     * **MANAGE STUDENTS courseWISE**** */

    function student_add($param1 = "", $param2 ="") {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if($param1 == 'admit_from_tentative'){      
            $data['check_status'] = 1;
            $page_data['student_name'] = $this->db->get_where('tentative_student',array('tentative_student_id' => $param2))->row()->student_name;
            // echo $page_data['student_name'];
            // die();
            $this->db->where('tentative_student_id', $param2);
            $this->db->update('tentative_student', $data);


        }

        $page_data['page_name'] = 'student_add';
        $page_data['page_title'] = get_phrase('add_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_information($course_id = '' , $student_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_information';
        $page_data['page_title'] = get_phrase('student_information') . " - " . get_phrase('course') . " : " .
        $this->crud_model->get_course_name($course_id);
        $page_data['course_id'] = $course_id;
        $this->load->view('backend/index', $page_data);
    }

    function get_batch_name($course_id) {
        $batchs = $this->db->get_where('batch', array(
            'course_id' => $course_id
        ))->result_array();
        echo '<option value="">Select below</option>';
        foreach ($batchs as $row) {
            echo '<option value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
        }
    }

    function get_batch_list($course_id) {
        $batchs = $this->db->get_where('batch', array(
            'course_id' => $course_id,
            'status' => 1
        ))->result_array();
        echo '<option value="">Select below</option>';
        foreach ($batchs as $row) {
            echo '<option value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
        }
    }

    function get_student_list($batch_id) {
        $this->db->order_by('roll', 'asc');
        $students = $this->db->get_where('student', array(
            'batch_id' => $batch_id 
        ))->result_array();
        echo '<option value="">Select below</option>';
        foreach ($students as $row) {
            echo '<option value="' . $row['student_id'] . '">' . $row['roll'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $row['name'] . '</option>';

        }
    }
    function get_exam_list($session) {
        $exams = $this->db->get_where('exam', array(
            'session' => $session
        ))->result_array();
        foreach ($exams as $row) {
            echo '<option value="' . $row['exam_id'] . '">' . $row['name'] . '</option>';
        }
    }
    function examinfo($exam_id , $student_id) {
        $query = $this->db->get_where('invoice', array(
            'exam_id' => $exam_id, 'student_id' => $student_id));
        if ($query->num_rows() > 0){
           echo '1';  
       }
       else echo '0';
   }
   function checkmonthlyfees($feesmonth , $student_id) {
    $query = $this->db->get_where('invoice', array(
        'feesmonth' => $feesmonth, 'student_id' => $student_id));
    if ($query->num_rows() > 0){
       echo '1';  
   }
   else echo '0';
}
function incomesetup($income_category_id) {
    if($income_category_id == 1){
        echo "1";            
    }
    elseif($income_category_id == 4){
        echo '4';
    }
    else
        echo '0';
}
private function cmparoll($a, $b) {
    if (intval($a['roll']) == intval($b['roll'])) {
        return 0;
    }
    return (intval($a['roll']) < intval($b['roll'])) ? -1 : 1;
}
private function cmpdgrade($a, $b) {
    if (floatval($a['grade_point']) == floatval($b['grade_point'])) {
        return 0;
    }
    return (floatval($a['grade_point']) < floatval($b['grade_point'])) ? 1 : -1;
}
function student_marksheet() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_session = $this->input->post('session');
    $_exam_id = $this->input->post('exam_id');
    $_course_id = $this->input->post('course_id');
    if (!empty($_course_id) && !empty($_exam_id) && !empty($_session)) {
        $_sqlresult = "SELECT result.*, student.name
        FROM   result
        JOIN   student 
        ON student.student_id = result.student_id
        WHERE  result.session = '$_session'
        AND result.exam_id = '$_exam_id'
        AND result.course_id = '$_course_id'";

        $this->db->order_by("grade_id", "asc");
        $grades = $this->db->get_where('grade',array('for_marks' => 100))->result_array();
        $result = $this->db->query($_sqlresult)->result_array();
        usort($grades, 'self::cmpdgrade');
        usort($result, 'self::cmparoll');
        $page_data['grades'] = $grades;
        $page_data['result'] = $result;
        $page_data['session'] = $_session;
        $page_data['exam_id'] = $_exam_id;
        $page_data['course_id'] = $_course_id;
    }
    $page_data['page_name'] = 'student_marksheet';
    $page_data['page_title'] = get_phrase('student_marksheets');
    $this->load->view('backend/index', $page_data);
}
function check_teacher_salary($teacher_id = "", $month_year = ""){
   $status = false;
   $value = null;
   $this->db->where(array('teacher_id' => $teacher_id, 'for_month' => $month_year));
   $query = $this->db->get('teacher_salary');
   $num_row = $query->num_rows();
   $has_due = $query->row()->due;
   if(($num_row == 1) && ($has_due != 0)){
    $value = $query->row()->due;
    $status = true;
}
elseif(($num_row == 1) && ($has_due == 0)){
    $status = false;
    $value = '<p class="text-danger">Salary already paid for selected month , please check report , THANK YOU!!</p>';
}
else{
    $status= false;
    $value = '<p class="text-danger">Salary not generated for this month please generate salary and try again , THANK YOU!!</p>';
}


echo json_encode(array('status' => $status, 'value'=> $value ));

}

function gettabulationsheetpdf() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_data = $this->input->post('body');
    $_header = $this->input->post('header');
    $_footer = $this->input->post('footer');
    $_style = $this->input->post('style');
    $_file_name = $this->input->post('filename') . '.pdf';
    $_page_size = $this->input->post('pagesize');
    if (strlen($_header) > 0 &&
        strlen($_data) > 0 &&
        strlen($_footer) > 0 &&
        strlen($_style) > 0 &&
        strlen($_file_name) > 0 &&
        strlen($_page_size) > 0) {
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    require_once('mpdf60/mpdf.php');

    $mpdf = new mPDF('C', $_page_size, '', '', 10, 10, 65, 30);
    $mpdf->useOnlyCoreFonts = false;
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($_style, 1);
    $mpdf->SetHTMLHeader($_header, 'E', true);
    $mpdf->SetHTMLFooter($_footer, 'E');
    $mpdf->SetHTMLHeader($_header, 'O', true);
    $mpdf->SetHTMLFooter($_footer, 'O');
    $mpdf->WriteHTML($_style . $_data);
    $mpdf->Output('uploads/' . $_file_name, 'F');
    echo base_url() . "uploads/$_file_name";
}
exit(0);
}

function student_tabulationsheet() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_session = $this->input->post('session');
    $_exam_id = $this->input->post('exam_id');
    $_course_id = $this->input->post('course_id');
    if (!empty($_course_id) && !empty($_exam_id) && !empty($_session)) {
        $_sqlresult = "SELECT result.*, student.name
        FROM   result
        JOIN   student 
        ON student.student_id = result.student_id
        WHERE  result.session = '$_session'
        AND result.exam_id = '$_exam_id'
        AND result.course_id = '$_course_id'";

        $grades = $this->db->get('grade')->result_array();
        $result = $this->db->query($_sqlresult)->result_array();
        $subjects = $this->db->get_where('subject', array('course_id' => $_course_id))->result_array();

        usort($grades, 'self::cmpdgrade');
        usort($result, 'self::cmparoll');

        $page_data['grades'] = $grades;
        $page_data['result'] = $result;
        $page_data['subjects'] = $subjects;
        $page_data['session'] = $_session;
        $page_data['exam_id'] = $_exam_id;
        $page_data['course_id'] = $_course_id;
    }
    $page_data['page_name'] = 'student_tabulationsheet';
    $page_data['page_title'] = get_phrase('student_tabulationsheets');
    $this->load->view('backend/index', $page_data);
}

function get_subjects() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_course_id = $this->input->post('course_id');
    $_exam_id = $this->input->post('exam_id');
    $_session = $this->input->post('session');
    $_subject_id = $this->input->post('subject_id');

    if (!empty($_course_id) && !empty($_exam_id) && !empty($_session) && !empty($_subject_id)) {
        $_sql = "SELECT student.student_id as student_id, 
        student.name as name, 
        student.course_id as course_id, 
        student.batch_id as batch_id, 
        student.roll as roll, 
        mark.mark as mark, 
        mark.theorymark as theorymark, 
        mark.subjectivemark as subjectivemark, 
        mark.objectivemark as objectivemark, 
        mark.practicalmark as practicalmark 
        FROM student
        LEFT JOIN mark ON mark.student_id = student.student_id
        AND mark.course_id = student.course_id
        AND mark.batch_id = student.batch_id
        AND mark.subject_id = '$_subject_id'
        AND mark.exam_id = '$_exam_id'
        AND mark.session = '$_session'
        WHERE student.course_id = '$_course_id' ORDER BY roll";

        $page_data['session'] = $_session;
        $page_data['exam_id'] = $_exam_id;
        $page_data['course_id'] = $_course_id;
        $page_data['subject_id'] = $_subject_id;
        $page_data['subject'] = $this->db->get_where('subject', array('subject_id' => $_subject_id))->result_array();
        $page_data['students'] = $this->db->query($_sql)->result_array();
            // echo $this->db->last_query();
            // die();
    }
    $page_data['page_name'] = 'marks';
    $page_data['page_title'] = get_phrase('manage_marks');
    $this->load->view('backend/index', $page_data);
}
private function cmpdmarks($a, $b) {
    if (floatval($a['total']) == floatval($b['total'])) {
        return 0;
    }
    return (floatval($a['total']) < floatval($b['total'])) ? 1 : -1;
}
function save_marks() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_courseid = $this->input->post('course_id'); $_examid = $this->input->post('exam_id');
    $_session = $this->input->post('session'); $_subjectid = $this->input->post('subject_id');
        //9/5/2018
    $_checkfullmark = $this->db->get_where('subject', array('subject_id' => $_subjectid))->row()->fullmark;
         //9/5/2018
    if (!empty($_courseid) && !empty($_examid) && !empty($_session) && !empty($_subjectid)) {
        $_dbsub = $this->db->get_where('subject', array('subject_id' => $_subjectid))->result_array()[0];

        $_student_id = $this->input->post('student_id');
            //$_roll = $this->input->post('roll');
        $_batch_id = $this->input->post('batch_id');
        $_mark = $this->input->post('mark');
        $_theorymark = $this->input->post('theorymark');
        $_subjectivemark = $this->input->post('subjectivemark');
        $_objectivemark = $this->input->post('objectivemark');
        $_practicalmark = $this->input->post('practicalmark');

        $_count = sizeof($_student_id);
        $_max = 0;
        $data_batch = array();
        for ($i = 0; $i < $_count; $i++) {
            $_absent = false;
            $_failed = false;
            if (floatval($_mark[$i]) <= 0 &&
                floatval($_theorymark[$i]) <= 0 &&
                floatval($_subjectivemark[$i]) <= 0 &&
                floatval($_objectivemark[$i]) <= 0 &&
                floatval($_practicalmark[$i]) <= 0) {
                if (strpos($_mark[$i], 'a') === TRUE ||
                    strpos($_theorymark[$i], 'a') === TRUE ||
                    strpos($_subjectivemark[$i], 'a') === TRUE ||
                    strpos($_objectivemark[$i], 'a') === TRUE ||
                    strpos($_practicalmark[$i], 'a') === TRUE) {
                    $_absent = true;
            } else {
                continue;
            }
        }

        $_mark_batch = floatval($_dbsub['passmark']) > 0;
        $_tmark_batch = floatval($_dbsub['theorypassmark']) > 0;
        $_smark_batch = floatval($_dbsub['subjectivepassmark']) > 0;
        $_omark_batch = floatval($_dbsub['objectivepassmark']) > 0;
        $_pmark_batch = floatval($_dbsub['practicalpassmark']) > 0;

        if ($_mark_batch && (floatval($_mark[$i]) < floatval($_dbsub['passmark'])))
            $_failed = true;

        if ($_tmark_batch && (floatval($_theorymark[$i]) < floatval($_dbsub['theorypassmark'])))
            $_failed = true;

        if ($_smark_batch && (floatval($_subjectivemark[$i]) < floatval($_dbsub['subjectivepassmark'])))
            $_failed = true;

        if ($_omark_batch && (floatval($_objectivemark[$i]) < floatval($_dbsub['objectivepassmark'])))
            $_failed = true;

        if ($_pmark_batch && (floatval($_practicalmark[$i]) < floatval($_dbsub['practicalpassmark'])))
            $_failed = true;

        $data['student_id'] = $_student_id[$i];
        $data['subject_id'] = $_subjectid;
        $data['course_id'] = $_courseid;
        $data['batch_id'] = $_batch_id[$i];
        $data['exam_id'] = $_examid;
        $data['session'] = $_session;
        $data['mark'] = $_mark[$i];
        $data['theorymark'] = $_theorymark[$i];
        $data['subjectivemark'] = $_subjectivemark[$i];
        $data['objectivemark'] = $_objectivemark[$i];
        $data['practicalmark'] = $_practicalmark[$i];
        $data['total'] = number_format((float)(floatval($_mark[$i]) +
         floatval($_theorymark[$i]) +
         floatval($_subjectivemark[$i]) +
         floatval($_objectivemark[$i]) +
         floatval($_practicalmark[$i])), 2, '.', '');
        $data['grade'] = ($_absent == false && $_failed == false) ? $this->crud_model->get_grade($data['total'],$_checkfullmark)['name'] : 'F';
        $data['point'] = ($_absent == false && $_failed == false) ? number_format((float)$this->crud_model->get_grade($data['total'],$_checkfullmark)['grade_point'], 2, '.', '') : '0.00';
        $data['comment'] = ($_absent == false && $_failed == false) ? $this->crud_model->get_grade($data['total'],$_checkfullmark)['comment'] : '';
        $data_batch[] = $data;
    }

    usort($data_batch, 'self::cmpdmarks');
    for($i = 0; $i < count($data_batch); $i++) {
        if ($data_batch[$i]['grade'] != 'F') {
            $_max = $data_batch[$i]['total'];
            break;
        }
    }

    foreach($data_batch as $data) {
        $_check = array('student_id' => $data['student_id'],
            'subject_id' => $data['subject_id'],
            'course_id' => $data['course_id'],
            'batch_id' => $data['batch_id'],
            'exam_id' => $data['exam_id'],
            'session' => $data['session']);

        $_oldmark = $this->db->get_where('mark', $_check)->result_array();

        $data['highestmark'] = number_format((float)(floatval($_max)), 2, '.', '');

        if (count($_oldmark) > 0) {
            $this->db->where($_check);
            $this->db->update('mark', $data);
        } else {
            $this->db->insert('mark', $data);
        }
    }
    $this->session->set_flashdata('flash_message', get_phrase('mark_saved_successfully.'));
}
$page_data['session'] = $_session;
$page_data['exam_id'] = $_examid;
$page_data['course_id'] = $_courseid;
$page_data['subject_id'] = $_subjectid;
$page_data['page_name'] = 'marks';
$page_data['page_title'] = get_phrase('manage_marks');
$this->load->view('backend/index', $page_data);
}

private function cmpdavgtotalarray($a, $b) {
    if (floatval($a['avg_total']) == floatval($b['avg_total'])) {
        return 0;
    }
    return (floatval($a['avg_total']) < floatval($b['avg_total'])) ? 1 : -1;
}

function student_promotion($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'individual') {
        $_courseid = $this->input->post('i_course_id');
        $_session = $this->input->post('i_session');
        $_tocourseid = $this->input->post('i_to_course_id');
        $_frombatchid = $this->input->post('i_from_batch_id');
        $_tobatchid = $this->input->post('i_to_batch_id');

        $_examid = 0;
        foreach($this->db->get_where('exam', array('session' => $_session, 'type' => '3'))->result_array() as $row)
            $_examid = $row['exam_id'];

        if (intval($_examid) > 0) {

            $_sql = "SELECT result.student_id, 
            result.result, 
            result.result_single, 
            result.totalfailed, 
            result.totalabsent, 
            result.remark as details,
            result.roll,
            student.name
            FROM   result 
            JOIN   student 
            ON student.student_id = result.student_id
            AND student.course_id = result.course_id
            AND student.batch_id = result.batch_id
            WHERE  result.course_id = '$_courseid'
            AND student.course_id = '$_courseid'
            AND student.batch_id = '$_frombatchid'
            AND result.exam_id = '$_examid'
            AND result.session = '$_session' 
            AND result.result_single != 'P'";

            if ($param2 == 'promote') {
                $newroll = (count($this->db->get_where('student', array('course_id' => $_tocourseid, 'batch_id' => $_tobatchid))->result_array()) + 1);
                $_student_id = $this->input->post('i_student_id');
                $data['course_id'] = $_tocourseid;
                $data['roll'] = str_pad($newroll, 2, '0', STR_PAD_LEFT);
                $data['batch_id'] = $_tobatchid;

                $this->db->where('student_id', $_student_id);
                $this->db->update('student', $data);
                echo 'ok';
                return;
            }
            $page_data['student'] = $this->db->query($_sql)->result_array();
            $page_data['i_session'] = $_session;
            $page_data['i_course_id'] = $_courseid;
            $page_data['i_from_batch_id'] = $_frombatchid;
            $page_data['i_to_course_id'] = $_tocourseid;
            $page_data['i_to_batch_id'] = $_tobatchid;
        }
    } else if ($param1 == 'promote') {
        $_examid = 0;
        $_session = $this->input->post('session');

        foreach($this->db->get_where('exam', array('session' => $_session, 'type' => '3'))->result_array() as $row)
            $_examid = $row['exam_id'];

        if (intval($_examid) > 0) {
            $_from_course_id = $this->input->post('from_course_id');
            $_to_course_id = $this->input->post('to_course_id');
            $_from_batch_id = $this->input->post('from_batch_id');
            $_to_batch_id = $this->input->post('to_batch_id');
            $_sql = "SELECT * 
            FROM   result 
            JOIN   student 
            ON student.student_id = result.student_id
            AND student.course_id = result.course_id
            AND student.batch_id = result.batch_id
            WHERE  result.course_id = '$_from_course_id'
            AND student.course_id = '$_from_course_id'
            AND student.batch_id = '$_from_batch_id'
            AND result.exam_id = '$_examid'
            AND result.session = '$_session'
            AND result.result_single = 'P'
            ORDER BY result.avg_total DESC";

            $_student = $this->db->query($_sql)->result_array();
                //usort($_student, 'self::cmpdavgtotalarray');
            $oldstudent = $this->db->get_where('student', array('course_id' => $_to_course_id, 'batch_id' => $_to_batch_id))->result_array();
            $newroll = count($oldstudent);
            $oldroll = (count($_student) + 1);
            foreach ($oldstudent as $row) {
                $res = $this->db->get_where('result', array('session' => $_session, 'batch_id' => $_to_batch_id, 'exam_id' => $_examid, 'student_id' => $row['student_id']))->result_array()[0]['result_single'];
                if ($res != 'P') {
                    $data['roll'] = str_pad($oldroll, 2, '0', STR_PAD_LEFT);
                    $this->db->where('student_id', $row['student_id']);
                    $this->db->update('student', $data);
                    $newroll--;
                    $oldroll++;
                }
            }
            $newroll = $newroll < 0 ? 0 : $newroll;
            foreach ($_student as $row) {
                $data['course_id'] = $_to_course_id;
                $data['batch_id'] = $_to_batch_id;
                $data['roll'] = str_pad($newroll, 2, '0', STR_PAD_LEFT);
                $this->db->where('student_id', $row['student_id']);
                $this->db->update('student', $data);
                $newroll++;
            }
            if (count($_student) > 0) {
                $_sql = "SELECT student.student_id,
                student.name, 
                student.roll, 
                student.course_id,
                result.result, 
                result.roll as oldroll
                FROM   result 
                JOIN   student 
                ON student.student_id = result.student_id
                WHERE  result.course_id = '$_from_course_id'
                AND student.course_id = '$_to_course_id'
                AND student.batch_id = '$_to_batch_id'
                AND result.exam_id = '$_examid'
                AND result.session = '$_session'
                AND result.result_single = 'P'";
                $page_data['student_promoted'] = $this->db->query($_sql)->result_array();
                $this->session->set_flashdata('flash_message', get_phrase('student_promoted_successfully.'));
            } else {
                $this->session->set_flashdata('flash_message', get_phrase('no_students_found.'));
            }
            $page_data['session'] = $_session;
            $page_data['from_course_id'] = $_from_course_id;
            $page_data['to_course_id'] = $_to_course_id;
            $page_data['from_batch_id'] = $_from_batch_id;
            $page_data['to_batch_id'] = $_to_batch_id;
        }
    }
    $page_data['page_name'] = 'student_promotion';
    $page_data['individual'] = ($param1 == 'individual');
    $page_data['page_title'] = get_phrase('student_promotion');
    $this->load->view('backend/index', $page_data);
}

private function cmpdtotal($a, $b) {
    if (floatval($a->total) == floatval($b->total)) {
        return 0;
    }
    return (floatval($a->total) < floatval($b->total)) ? 1 : -1;
}
private function cmpdavgtotal($a, $b) {
    if (floatval($a->avgtotal) == floatval($b->avgtotal)) {
        return 0;
    }
    return (floatval($a->avgtotal) < floatval($b->avgtotal)) ? 1 : -1;
}
function generate_result() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');


    $_course_id = $this->input->post('course_id_result');
    $_exam_id = $this->input->post('exam_id_result');
    $_session = $this->input->post('session_result');

    if (!empty($_course_id) && !empty($_exam_id) && !empty($_session)) {
        include 'Result.course.php';

        $_gradelist = array();
        foreach ($this->db->get('grade')->result_array() as $row) {

            $_grade = new cl_grade();

            $_grade->id = $row['grade_id'];
            $_grade->name = trim($row['name']);
            $_grade->point = $row['grade_point'];
            $_grade->markfrom = $row['mark_from'];
            $_grade->markupto = $row['mark_upto'];
            $_grade->comment = trim($row['comment']);
            $_grade->formarks = $row['for_marks'];
            $_gradelist[] = $_grade;
        }


        $_stdlist = array();
        $_dbstudents = $this->db->get_where('student', array('course_id' => $_course_id))->result_array();
        if (count($_dbstudents) <= 0) {
            $this->session->set_flashdata('flash_message', get_phrase('result_is_not_generated.'));
            return;
        }
        $_final = false;
        $_firstid = 0;
        $_secondid = 0;
        if (intval($this->db->get_where('exam', array('exam_id' => $_exam_id, 'session' => $_session))->result_array()[0]['type']) == 3) {
            $_final = true;
            $_allexams = $this->db->get_where('exam', array('session' => $_session))->result_array();
            foreach($_allexams as $_exam) {
                if ($_exam['type'] == '1')
                    $_firstid = $_exam['exam_id'];
                if ($_exam['type'] == '2')
                    $_secondid = $_exam['exam_id'];
            }
        }
        foreach ($_dbstudents as $student) {
            $_sublist = array();
            $_sub_sql = "SELECT mark.subject_id,
            subject.name, 
            mark.course_id, 
            subject.fullmark, 
            subject.passmark, 
            subject.theoryfullmark, 
            subject.theorypassmark, 
            subject.practicalfullmark,
            subject.practicalpassmark, 
            subject.objectivefullmark, 
            subject.objectivepassmark, 
            subject.subjectivefullmark, 
            subject.subjectivepassmark
            FROM   mark 
            JOIN   subject ON subject.subject_id = mark.subject_id
            WHERE  mark.course_id = '$_course_id' 
            AND mark.exam_id = '$_exam_id' 
            AND mark.session = '$_session'
            GROUP BY mark.subject_id";
            $_all_sub = $this->db->query($_sub_sql)->result_array();
            $_first_total = $this->db->get_where('result', array('session' => $_session,
                'course_id' => $_course_id,
                'exam_id' => $_firstid,
                'student_id' => $student['student_id']))->result_array()[0]['total'];
            $_second_total = $this->db->get_where('result', array('session' => $_session,
                'course_id' => $_course_id,
                'exam_id' => $_secondid,
                'student_id' => $student['student_id']))->result_array()[0]['total'];
            foreach ($_all_sub as $sub) {
                $_sub = new cl_subject();
                $_sub->grades = $_gradelist;
                $_sub->id = $sub['subject_id'];
                $_sub->name = trim($sub['name']);
                $_sub->shortname = 'N/A';
                $_sub->code = 'N/A';
                $_sub->fullmark = $sub['fullmark'];
                $_sub->passmark = $sub['passmark'];
                $_sub->theoryfullmark = $sub['theoryfullmark'];
                $_sub->theorypassmark = $sub['theorypassmark'];
                $_sub->practicalfullmark = $sub['practicalfullmark'];
                $_sub->practicalpassmark = $sub['practicalpassmark'];
                $_sub->subjectivefullmark = $sub['subjectivefullmark'];
                $_sub->subjectivepassmark = $sub['subjectivepassmark'];
                $_sub->objectivefullmark = $sub['objectivefullmark'];
                $_sub->objectivepassmark = $sub['objectivepassmark'];
                $_sub->mark = 0;
                $_sub->theorymark = 0;
                $_sub->practicalmark = 0;
                $_sub->subjectivemark = 0;
                $_sub->objectivemark = 0;
                $_mark = $this->db->get_where('mark', array(
                    'course_id' => $_course_id,
                    'exam_id' => $_exam_id,
                    'session' => $_session,
                    'subject_id' => $sub['subject_id'],
                    'student_id' => $student['student_id']))->result_array()[0];
                $_sub->mark = $_mark['mark'];
                $_sub->theorymark = $_mark['theorymark'];
                $_sub->practicalmark = $_mark['practicalmark'];
                $_sub->subjectivemark = $_mark['subjectivemark'];
                $_sub->objectivemark = $_mark['objectivemark'];
                $_sub->enabled = true;
                $_sublist[] = $_sub;
            }
            $_student = new cl_student();
            $_student->batchid = $student['batch_id'];
            $_student->departmentid = $student['department_id'];
            $_student->roll = $student['roll'];
            $_student->grades = $_gradelist;
            $_student->subjects = $_sublist;
            $_student->id = $student['student_id'];
            $_student->courseid = $_course_id;
            $_student->examid = $_exam_id;
            $_student->session = $_session;
            if ($_final) {
                $_student->examtype = 3;
                $_student->prevexamtotal = $_first_total + $_second_total;
            } else {
                $_student->examtype = 0;
                $_student->prevexamtotal = 0;
            }
            $_stdlist[] = $_student;
        }
        $_pos = 1;
        if ($_final)
            usort($_stdlist, 'self::cmpdavgtotal');
        else
            usort($_stdlist, 'self::cmpdtotal');

        foreach($_stdlist as $_std) {
            $data['student_id'] = $_std->id;
            $data['course_id'] = $_std->courseid;
            $data['department_id'] = $_std->departmentid;
            $data['batch_id'] = $_std->batchid;
            $data['session'] = $_std->session;
            $data['exam_id'] = $_std->examid;
            $data['result'] = $_std->result;
            $data['result_single'] = $_std->result_single;
            $data['total'] = $_std->total;
            $data['totalfailed'] = $_std->totalfailed;
            $data['totalabsent'] = $_std->totalabsent;
            $data['points'] = $_std->totalpoints;
            $data['grade'] = $_std->grade;
            $data['gpa'] = $_std->gpa;
            $data['remark'] = $_std->remark;
            $data['roll'] = $_std->roll;
            $data['avg_total'] = $_std->avgtotal;
            $data['gen_datetime'] = date('Y-m-d H:i:s');

            if ($_std->result_single == 'P' && $_final) {
                if ($_pos == 1) {
                    $data['position'] = $_pos . 'st';
                } else if ($_pos == 2) {
                    $data['position'] = $_pos . 'nd';
                } else if ($_pos == 3) {
                    $data['position'] = $_pos . 'rd';
                } else if ($_pos < 11) {
                    $data['position'] = $_pos . 'th';
                } else {
                    $data['position'] = 'N/A';
                }
                $_pos++;
            } else {
                $data['position'] = 'N/A';
            }

            $_check = array('student_id' => $_std->id,
                'course_id' => $_std->courseid,
                'batch_id' => $_std->batchid,
                'exam_id' => $_std->examid,
                'session' => $_std->session);

            $_oldresult = $this->db->get_where('result', $_check)->result_array();

            if (count($_oldresult) > 0) {
                $this->db->where($_check);
                $this->db->update('result', $data);
            } else {
                $this->db->insert('result', $data);
            }
        }
        $this->session->set_flashdata('flash_message', get_phrase('result_generated_successfully.'));
        $page_data['exam_id_result'] = $_exam_id;
        $page_data['session_result'] = $_session;
        $page_data['course_id_result'] = $_course_id;
    } else {
        $this->session->set_flashdata('flash_message', get_phrase('result_is_not_generated.'));
    }
    $page_data['page_name'] = 'marks';
    $page_data['page_title'] = get_phrase('manage_marks');
    $this->load->view('backend/index', $page_data);
}

function getmarksheetpdf() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_data = $this->input->post('body');
    $_header = $this->input->post('header');
    $_footer = $this->input->post('footer');
    $_style = $this->input->post('style');
    $_file_name = $this->input->post('filename') . '.pdf';
    $_page_size = $this->input->post('pagesize');

    if (strlen($_data) > 0 ) {

        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        require_once('mpdf60/mpdf.php');

        $mpdf = new mPDF('C', $_page_size, '','', 15, 15, 50, 30);
        $mpdf->useOnlyCoreFonts = false;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($_style, 1);
        $mpdf->SetHTMLHeader($_header, 'E', true);
        $mpdf->SetHTMLFooter($_footer, 'E');
        $mpdf->SetHTMLHeader($_header, 'O', true);
        $mpdf->SetHTMLFooter($_footer, 'O');
        $mpdf->WriteHTML($_data);
        $mpdf->Output('uploads/' . $_file_name, 'F');
        echo base_url() . "uploads/$_file_name";
    }
    exit(0);
}
    /**
     * @param string $param1
     * @param string $param2
     * @param string $param3
     */
    function student($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
//            $data2['name'] = $this->input->post('parent_name');
//            $data2['email'] = $this->input->post('parent_email');
//            $data2['password'] = $this->input->post('parent_password');
//            $data2['phone'] = $this->input->post('parent_phone');
//            $data2['address'] = $this->input->post('parent_address');            
//            $data2['profession'] = $this->input->post('parent_profession');           
//            $this->db->insert('parent', $data2);
//            $parent_id = $this->db->insert_id(); 
//            $data['parent_id'] = $parent_id; 
//            if($this->input->post('parent_id') == true){
//               $data['parent_id'] = $this->input->post('parent_id'); 
//            }

            $data['name'] = $this->input->post('name');
            
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');            
            $data['phone'] = $this->input->post('phone');           
            $data['email'] = $this->input->post('email');
            $data['address'] = $this->input->post('address');
            $data['m_area'] = $this->input->post('m_area');
            $data['password'] = $this->input->post('password');
            $data['course_id'] = $this->input->post('course_id');
         //   $data['department_id'] = $this->input->post('department_id');
            if ($this->input->post('batch_id') != '') {
                $data['batch_id'] = $this->input->post('batch_id');
            }
            
          //  $data['admission_type_id'] = $this->input->post('admission_type_id');
            $data['father_name'] = $this->input->post('father_name');
            $data['mother_name'] = $this->input->post('mother_name');
//            $data['religion'] = $this->input->post('religion');
//            $data['blood_group'] = $this->input->post('blood_group');
//            $data['previous_school'] = $this->input->post('previous_school');
//            $data['previous_course'] = $this->input->post('previous_course');
            $data['admission_date'] = $this->input->post('admission_date');
//            $data['father_phone'] = $this->input->post('father_phone');
//            $data['mother_phone'] = $this->input->post('mother_phone');
//            
//            $data['father_email'] = $this->input->post('father_email');
//            $data['mother_email'] = $this->input->post('mother_email');
//            $data['father_profession'] = $this->input->post('father_profession');
//            $data['mother_profession'] = $this->input->post('mother_profession');
//            $data['father_monthly_income'] = $this->input->post('father_monthly_income');
//            $data['mother_monthly_income'] = $this->input->post('mother_monthly_income');
//            
//            $data['transport_id'] = $this->input->post('transport_id');
//            $data['dormitory_id'] = $this->input->post('dormitory_id');
//            $data['dormitory_room_number'] = $this->input->post('dormitory_room_number');
//            
            
            $data['roll'] = $this->input->post('roll');
            $this->db->insert('student', $data);
            $student_id = $this->db->insert_id();       
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
          //  $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            redirect(base_url() . 'index.php?admin/student_information/' . $data['course_id'], 'refresh');
        }
        if ($param2 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['m_area'] = $this->input->post('m_area');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['course_id'] = $this->input->post('course_id');
//         $data['department_id'] = $this->input->post('department_id');
            $data['batch_id'] = $this->input->post('batch_id');
//            $data['parent_id'] = $this->input->post('parent_id');
            $data['roll'] = $this->input->post('roll');
            
//            $data['admission_type_id'] = $this->input->post('admission_type_id');
            $data['father_name'] = $this->input->post('father_name');
            $data['mother_name'] = $this->input->post('mother_name');
//            $data['religion'] = $this->input->post('religion');
//            $data['blood_group'] = $this->input->post('blood_group');
//            $data['previous_school'] = $this->input->post('previous_school');
//            $data['previous_course'] = $this->input->post('previous_course');
            $data['admission_date'] = $this->input->post('admission_date');
//            $data['father_phone'] = $this->input->post('father_phone');
//            $data['mother_phone'] = $this->input->post('mother_phone');
//            
//            $data['father_email'] = $this->input->post('father_email');
//            $data['mother_email'] = $this->input->post('mother_email');
//            $data['father_profession'] = $this->input->post('father_profession');
//            $data['mother_profession'] = $this->input->post('mother_profession');
//            $data['father_monthly_income'] = $this->input->post('father_monthly_income');
//            $data['mother_monthly_income'] = $this->input->post('mother_monthly_income');
//            
//            $data['transport_id'] = $this->input->post('transport_id');
//            $data['dormitory_id'] = $this->input->post('dormitory_id');
//            $data['dormitory_room_number'] = $this->input->post('dormitory_room_number');

            $this->db->where('student_id', $param3);
            $this->db->update('student', $data);
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $param3 . '.jpg');
            $this->crud_model->clear_cache();
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/student_information/' . $param1, 'refresh');
        }

        if ($param2 == 'delete') {
            $this->db->where('student_id', $param3);
            $this->db->delete('student');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/student_information/' . $param1, 'refresh');
        }

    }
    // ADD TENTATIVE STUDENT 
    function tentative_student($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
//      
            $data['student_name'] = $this->input->post('student_name');
            $course_interested = array();
           // $course_interested[]= json_encode($this->input->post('preferred_course'),true);
            $course_interested[]= $this->input->post('preferred_course');
            $result = array();
            $checking = array();
            // print_r($course_interested);
            // die();
            foreach ($course_interested as $i => $row) {
              $result[] = $row;

          }
          $data['preferred_course_id'] = json_encode($course_interested);
          $data['check_status'] = 0;

          $data['contact_no'] = $this->input->post('contact_no');	
          $data['m_area'] = $this->input->post('m_area');

          $this->db->insert('tentative_student', $data);
          $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
          //  $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
          redirect(base_url() . 'index.php?admin/tentative_student/', 'refresh');
      }

      if ($param1 == 'do_update') {
          $data['student_name'] = $this->input->post('student_name');
          $course_interested = array();
           // $course_interested[]= json_encode($this->input->post('preferred_course'),true);
          $course_interested[]= $this->input->post('preferred_course');
          $result = array();
          $checking = array();

          foreach ($course_interested as $i => $row) {
              $result[] = $row;

          }
          $data['preferred_course_id'] = json_encode($course_interested);
          $data['check_status'] = 0;

          $data['contact_no'] = $this->input->post('contact_no');  
          $data['m_area'] = $this->input->post('m_area');
    // print_r($data);
    //         die();
          $this->db->where('tentative_student_id', $param2);
          $this->db->update('tentative_student', $data);
          $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
          redirect(base_url() . 'index.php?admin/tentative_student/', 'refresh');
      }

      if ($param1 == 'delete') {
        $this->db->where('tentative_student_id', $param2);
        $this->db->delete('tentative_student');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/tentative_student/', 'refresh');
    }
 
    $page_data['page_name'] = 'tentative_student';
    $page_data['page_title'] = get_phrase('tentative_student');
    $this->load->view('backend/index', $page_data);
}
    //add new tentative student
function tentative_student_add(){
   if ($this->session->userdata('admin_login') != 1)
    redirect(base_url(), 'refresh');


$page_data['page_name'] = 'tentative_student_add';
$page_data['page_title'] = get_phrase('tentative_student_add');
$this->load->view('backend/index', $page_data);
}

/*     * **MANAGE PARENTS courseWISE**** */

function parent($param1 = '', $param2 = '', $param3 = '')
{
    if($this->  session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['profession'] = $this->input->post('profession');
        $this->db->insert('parent', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        $this->email_model->account_opening_email('parent', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['profession'] = $this->input->post('profession');
        $this->db->where('parent_id', $param2);
        $this->db->update('parent', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('parent_id', $param2);
        $this->db->delete('parent');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    $page_data['page_title'] = get_phrase('all_parents');
    $page_data['page_name'] = 'parent';
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE TEACHERS**** */

function teacher($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['designation'] = $this->input->post('designation');
        $data['intime'] = $this->input->post('intime');
        $data['outtime'] = $this->input->post('outtime');
        $data['birthday'] = $this->input->post('birthday');
        $data['joindate'] = $this->input->post('joindate');
        $data['salary'] = $this->input->post('salary');
        $data['eosdate'] = 'N/A';
        $data['sex'] = $this->input->post('sex');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');        
        $data['religion'] = $this->input->post('religion');
        $data['blood_group'] = $this->input->post('blood_group');
        $data['qualification'] = $this->input->post('qualification');
        $data['teacher_type_id'] = $this->input->post('teacher_type_id');
        $data['speciality'] = $this->input->post('speciality');
        $data['previous_institute'] = $this->input->post('previous_institute');
        $data['comment'] = $this->input->post('comment');
        
        
        $this->db->insert('teacher', $data);
        $teacher_id = $this->db->insert_id();
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $teacher_id . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        $this->email_model->account_opening_email('teacher', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['designation'] = $this->input->post('designation');
        $data['intime'] = $this->input->post('intime');
        $data['outtime'] = $this->input->post('outtime');
        $data['birthday'] = $this->input->post('birthday');
        $data['joindate'] = $this->input->post('joindate');
        $data['salary'] = $this->input->post('salary');
        $data['eosdate'] = $this->input->post('eosdate');
        $data['sex'] = $this->input->post('sex');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');

        $data['religion'] = $this->input->post('religion');
        $data['blood_group'] = $this->input->post('blood_group');
        $data['qualification'] = $this->input->post('qualification');
        $data['teacher_type_id'] = $this->input->post('teacher_type_id');
        $data['speciality'] = $this->input->post('speciality');
        $data['previous_institute'] = $this->input->post('previous_institute');
        $data['comment'] = $this->input->post('comment');
        
        $this->db->where('teacher_id', $param2);
        $this->db->update('teacher', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile'] = true;
        $page_data['current_teacher_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('teacher', array(
            'teacher_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('teacher_id', $param2);
        $this->db->delete('teacher');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    }
    $page_data['teachers'] = $this->db->get('teacher')->result_array();
    $page_data['page_name'] = 'teacher';
    $page_data['page_title'] = get_phrase('manage_Teachers/Employee');
    $this->load->view('backend/index', $page_data);
}

/*
    Generate teacher Salary
*/
    function teacher_salary($param1 ="", $param2 ="", $param3 =""){
        if($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if($param1 == 'create'){
            $month_year = date('F-Y',strtotime($this->input->post('datetime')));
            $this->db->where(array('teacher_id' => $param2, 'for_month' => $month_year));
            $query = $this->db->get('teacher_salary');
            // echo $query->num_rows();
            // var_dump($query->result());
            // die();
            if($query->num_rows() == 0){
                $data['teacher_id'] = $param2;
                $data['datetime'] = strtotime($this->input->post('datetime'));
                $data['no_of_class'] = $this->input->post('no_of_class');
                $teacher_type_id =  $this->db->get_where('teacher',array('teacher_id' => $param2))->row()->teacher_type_id;
                $salary= $this->db->get_where('teacher', array('teacher_id' => $param2))->row()->salary;
                if($teacher_type_id == '2'){
                    $data['amount'] = $data['no_of_class'] *  $salary;
                } 
                else
                    $data['amount'] =  $salary;
                $data['for_month'] = date('F-Y',$data['datetime']);
                $data['status'] = 2;
                $data['paid'] = 0;
                $data['due'] = $data['amount'];

                $this->db->insert('teacher_salary', $data);

            }
            else{
                $this->session->set_flashdata('flash_message', 'already generated for this month, try for different month');//message rendered\
                $this->session->set_flashdata('url_redirect', base_url('index.php?admin/teacher_salary'));//url to be redirected
                redirect(base_url() . 'index.php?admin/teacher_salary/', 'refresh');

            }
            redirect(base_url() . 'index.php?admin/teacher_salary/', 'refresh');

        }

        $page_data['page_name'] = 'teacher_salary';
        $page_data['page_title'] = get_phrase('teachers_salary');
        $this->load->view('backend/index', $page_data);
    }
    /*teacher salary report
    */
    function teacher_salary_report($param1 = "", $param2="", $param3=""){
        if($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'teacher_salary_report';
        $page_data['teacher_id'] = $param1;
        $page_data['teacher_details'] = $this->db->get_where('teacher', array('teacher_id' => $param1))->result_array();
        $page_data['page_title'] = get_phrase('teacher_salary_report');
        $this->load->view('backend/index', $page_data);


    }  /*students due report
    */
    function due_report($param1 = "", $param2="", $param3=""){
        if($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'due_report';
        $page_data['page_title'] = get_phrase('due_report');
        $this->load->view('backend/index', $page_data);


    }
    function ajax_due_report1($param1 = "", $param2="", $param3=""){
        if($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
      //  $student_id = $this->input->post('student_id');
        if($param1 ="all"){
         $this->load->view('backend/admin/due_all_report');
     }

 }
 function ajax_due_report($param1 = "", $param2="", $param3=""){
    if($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $student_id = $this->input->post('student_id');

    if(!empty($student_id)){

       $page_data['student_id'] = $student_id;
       $page_data['course_id'] = $param1;
       $page_data['batch_id'] = $param2;
       $page_data['page_title'] = get_phrase('due_report');
       $this->load->view('backend/admin/due_with_student', $page_data);
   }
   else{

    $page_data['course_id'] = $param1;
    $page_data['batch_id'] = $param2;
    $page_data['page_title'] = get_phrase('due_report');
    $this->load->view('backend/admin/due_without_student', $page_data);
}
}
/* * **MANAGE SUBJECTS**** */
function subject_category($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['comment'] = $this->input->post('comment');
        $this->db->insert('subject_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/subject_category/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['comment'] = $this->input->post('comment');

        $this->db->where('subject_category_id', $param3);
        $this->db->update('subject_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/subject_category/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('subject_category', array(
            'subject_category_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('subject_category_id', $param2);
        $this->db->delete('subject_category');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/subject_category/', 'refresh');
    }
    $page_data['subject_category'] = $this->db->get('subject_category')->result_array();
    $page_data['page_name'] = 'subject_category';
    $page_data['page_title'] = get_phrase('manage_subject_category');
    $this->load->view('backend/index', $page_data);
}

function subject($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['course_id'] = $this->input->post('course_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $data['fullmark'] = $this->input->post('fullmark');
        $data['passmark'] = $this->input->post('passmark');
        $data['theoryfullmark'] = $this->input->post('theoryfullmark');
        $data['theorypassmark'] = $this->input->post('theorypassmark');
        $data['subjectivefullmark'] = $this->input->post('subjectivefullmark');
        $data['subjectivepassmark'] = $this->input->post('subjectivepassmark');
        $data['objectivefullmark'] = $this->input->post('objectivefullmark');
        $data['objectivepassmark'] = $this->input->post('objectivepassmark');
        $data['practicalfullmark'] = $this->input->post('practicalfullmark');
        $data['practicalpassmark'] = $this->input->post('practicalpassmark');

        $this->db->insert('subject', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/subject/' . $data['course_id'], 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['course_id'] = $this->input->post('course_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $data['fullmark'] = $this->input->post('fullmark');
        $data['passmark'] = $this->input->post('passmark');
        $data['theoryfullmark'] = $this->input->post('theoryfullmark');
        $data['theorypassmark'] = $this->input->post('theorypassmark');
        $data['subjectivefullmark'] = $this->input->post('subjectivefullmark');
        $data['subjectivepassmark'] = $this->input->post('subjectivepassmark');
        $data['objectivefullmark'] = $this->input->post('objectivefullmark');
        $data['objectivepassmark'] = $this->input->post('objectivepassmark');
        $data['practicalfullmark'] = $this->input->post('practicalfullmark');
        $data['practicalpassmark'] = $this->input->post('practicalpassmark');

        $this->db->where('subject_id', $param2);
        $this->db->update('subject', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/subject/' . $data['course_id'], 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('subject', array(
            'subject_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('subject_id', $param2);
        $this->db->delete('subject');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/subject/' . $param3, 'refresh');
    }
    $page_data['course_id'] = $param1;
    $page_data['subjects'] = $this->db->get_where('subject', array('course_id' => $param1))->result_array();
    $page_data['page_name'] = 'subject';
    $page_data['page_title'] = get_phrase('manage_subject');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE courses**** */

function courses($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['name_numeric'] = $this->input->post('name_numeric');
        $data['total_hrs'] = $this->input->post('total_hrs');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $this->db->insert('course', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/courses/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['name_numeric'] = $this->input->post('name_numeric');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $data['total_hrs'] = $this->input->post('total_hrs');

        $this->db->where('course_id', $param2);
        $this->db->update('course', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/courses/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('course', array(
            'course_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('course_id', $param2);
        $this->db->delete('course');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/courses/', 'refresh');
    }    
    $page_data['courses'] = $this->db->get('course')->result_array();
    $page_data['page_name'] = 'course';
    $page_data['page_title'] = get_phrase('manage_course');
    $this->load->view('backend/index', $page_data);
}
/*
    Send SMS batch wise
*/
    function batch_send_sms($batch_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $data['message'] = $this->input->post('message');
        $students = $this->db->get_where('student',array('batch_id' => $batch_id))->result_array();

    // detect the first course
        foreach ($students as $row) {
            $reciever_phone = $row['phone'];
            $this->sms_model->send_sms($message, $reciever_phone);
        }
        $this->session->set_flashdata('flash_message', get_phrase('Messge_sent_successfully'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');


    }

    /* * **MANAGE batchS**** */

    function batch($course_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    // detect the first course
        if ($course_id == '')
            $course_id = $this->db->get('course')->first_row()->course_id;

        $page_data['page_name'] = 'batch';
        $page_data['page_title'] = get_phrase('manage_batches');
        $page_data['course_id'] = $course_id;
        $this->load->view('backend/index', $page_data);
    }
    function batch_list($course_id =""){
       if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $page_data['course_id'] = $course_id;
    $this->load->view('backend/admin/batch_list', $page_data);

}
function batchs($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['nick_name'] = $this->input->post('nick_name');
        $data['course_id'] = $this->input->post('course_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');

        $this->db->insert('batch', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/batch/' . $data['course_id'], 'refresh');
    }

    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $data['nick_name'] = $this->input->post('nick_name');
        $data['course_id'] = $this->input->post('course_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $this->db->where('batch_id', $param2);
        $this->db->update('batch', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/batch/' . $data['course_id'], 'refresh');
    }

    if ($param1 == 'update') {
            // $this->db->select('status');
        $this->db->where('batch_id', $param2);
        $status = $this->db->get('batch')->row()->status;
        if($status == 0){
            $data['status'] = 1;
        }
        if($status == 1){
            $data['status'] = 0;
        }
        $this->db->where('batch_id', $param2);
        $this->db->update('batch', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/batch', 'refresh');
    }

    if ($param1 == 'delete') {
        $this->db->where('batch_id', $param2);
        $this->db->delete('batch');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/batch', 'refresh');
    }
}

function get_course_batch($course_id) {
    $batchs = $this->db->get_where('batch', array(
        'course_id' => $course_id,
        'status' => 1
    ))->result_array();
    foreach ($batchs as $row) {
        echo '<option value="' . $row['batch_id'] . '">' . $row['name'] . '</option>';
    }
}

function get_course_subject($course_id) {
    $subjects = $this->db->get_where('subject', array(
        'course_id' => $course_id
    ))->result_array();
    foreach ($subjects as $row) {
        echo '<option value="' . $row['subject_id'] . '">' . $row['name'] . '</option>';
    }
}

/* * **MANAGE POST TYPE**** */

function posttype($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['data'] = $this->input->post('data');
        $this->db->insert('post_type', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/posttype/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['data'] = $this->input->post('data');

        $this->db->where('id', $param3);
        $this->db->update('post_type', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/posttype/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('post_type', array(
            'id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('id', $param2);
        $this->db->delete('post_type');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/posttype/', 'refresh');
    }
    $page_data['post_type'] = $this->db->get('post_type')->result_array();
    $page_data['page_name'] = 'posttype';
    $page_data['page_title'] = get_phrase('manage_post_type');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE ADMISSION TYPE**** */

function admissiontype($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['data'] = $this->input->post('data');
        $this->db->insert('admission_type', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/admissiontype/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['data'] = $this->input->post('data');

        $this->db->where('id', $param3);
        $this->db->update('admission_type', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/admissiontype/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('admission_type', array(
            'id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('id', $param2);
        $this->db->delete('admission_type');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/admissiontype/', 'refresh');
    }
    $page_data['admission_type'] = $this->db->get('admission_type')->result_array();
    $page_data['page_name'] = 'admissiontype';
    $page_data['page_title'] = get_phrase('manage_admission_type');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE COA**** */

function coa($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $active_page = 'master';

    if ($param1 == 'create') {
        $active_page = 'master';
        if ($param2 == 'master') {
            $data['a2_id'] = $this->input->post('a2_id');
            $data['name'] = $this->input->post('name');
            $data['a3_id'] = $this->input->post('a3_id');
            $data['inactive'] = $this->input->post('inactive');
            $data['comment'] = $this->input->post('comment');
            $this->db->insert('a2', $data);
            $active_page = 'master';
        }
        if ($param2 == 'type') {
            $data['a3_id'] = $this->input->post('a3_id');
            $data['name'] = $this->input->post('name');
            $data['a4_id'] = $this->input->post('a4_id');
            $data['parent_id'] = intval($this->input->post('parent_id'));
            $data['inactive'] = $this->input->post('inactive');
            $data['comment'] = $this->input->post('comment');
            $this->db->insert('a3', $data);
            $active_page = 'type';
        }
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/coa/' . $active_page, 'refresh');
    }
    if ($param1 == 'edit') {
        if ($param2 == 'master') {
            $data['a2_id'] = $this->input->post('a2_id');
            $data['name'] = $this->input->post('name');
            $data['a3_id'] = $this->input->post('a3_id');
            $data['inactive'] = $this->input->post('inactive');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('a2_id', $param3);
            $this->db->update('a2', $data);
            $page_data['active_page'] = 'master';
        }
        if ($param2 == 'type') {
            $data['a3_id'] = $this->input->post('a3_id');
            $data['name'] = $this->input->post('name');
            $data['a4_id'] = $this->input->post('a4_id');
            $data['parent_id'] = intval($this->input->post('parent_id'));
            $data['inactive'] = $this->input->post('inactive');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('a3_id', $param3);
            $this->db->update('a3', $data);
            $active_page = 'type';
        }
            /*if ($param2 == 'course') {
                $data['a4_id'] = $this->input->post('a4_id');
                $data['name'] = $this->input->post('name');
                $data['acc_coa_ctype_id'] = $this->input->post('acc_coa_ctype_id');
                $data['comment'] = $this->input->post('comment');

                $this->db->where('a4_id', $param3);
                $this->db->update('a4', $data);
                $active_page = 'course';
            }*/
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/coa/' . $active_page, 'refresh');
        }
        if ($param1 == 'delete') {
            if ($param2 == 'master') {
                $this->db->where('a2_id', $param3);
                $this->db->delete('a2');
                $active_page = 'master';
            }
            if ($param2 == 'type') {
                $this->db->where('a3_id', $param3);
                $this->db->delete('a3');
                $active_page = 'type';
            }
            /*if ($param2 == 'course') {
                $this->db->where('a4_id', $param3);
                $this->db->delete('a4');
                $active_page = 'course';
            }*/
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/coa/' . $active_page, 'refresh');
        }

        if (strlen($param1) > 0)
            $active_page = $param1;

        /*$page_data['coa_courses'] = $this->db->get_where('a4')->result_array();*/
        $page_data['coa_types'] = $this->db->get_where('a3')->result_array();
        $page_data['coa_masters'] = $this->db->get_where('a2')->result_array();
        $page_data['active_page'] = $active_page;
        $page_data['page_name'] = 'coa';
        $page_data['page_title'] = get_phrase('manage_chart_of_accounts');
        $this->load->view('backend/index', $page_data);
    }

    /* * **MANAGE EXAMS**** */

    function exam($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['session'] = $this->input->post('session');
            $data['date'] = $this->input->post('date');
            $data['startdate'] = $this->input->post('startdate');
            $data['enddate'] = $this->input->post('enddate');
            $data['workingdays'] = $this->input->post('workingdays');
            $data['type'] = $this->input->post('type');
            $data['comment'] = $this->input->post('comment');
            $this->db->insert('exam', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(base_url() . 'index.php?admin/exam/', 'refresh');
        }
        if ($param1 == 'edit' && $param2 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['session'] = $this->input->post('session');
            $data['date'] = $this->input->post('date');
            $data['startdate'] = $this->input->post('startdate');
            $data['enddate'] = $this->input->post('enddate');
            $data['workingdays'] = $this->input->post('workingdays');
            $data['type'] = $this->input->post('type');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('exam_id', $param3);
            $this->db->update('exam', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/exam/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('exam', array(
                'exam_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('exam_id', $param2);
            $this->db->delete('exam');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/exam/', 'refresh');
        }
        $session = $this->input->post('session');
        if (strlen($session) <= 0)
            $session = date('Y');

        $page_data['session'] = $session;
        $page_data['exams'] = $this->db->get_where('exam', array('session' => $session))->result_array();
        $page_data['page_name'] = 'exam';
        $page_data['page_title'] = get_phrase('manage_exam');
        $this->load->view('backend/index', $page_data);
    }

    /* * **** SEND EXAM MARKS VIA SMS ******* */

    function exam_marks_sms($param1 = '', $param2 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'send_sms') {
            $exam_id = $this->input->post('exam_id');
            $course_id = $this->input->post('course_id');
            $receiver = $this->input->post('receiver');

        // get all the students of the selected course
            $students = $this->db->get_where('student', array(
                'course_id' => $course_id
            ))->result_array();
        // get the marks of the student for selected exam
            foreach ($students as $row) {
                if ($receiver == 'student')
                    $receiver_phone = $row['phone'];
                if ($receiver == 'parent' && $row['parent_id'] != '')
                    $receiver_phone = $this->db->get_where('parent', array('parent_id' => $row['parent_id']))->row()->phone;


                $this->db->where('exam_id', $exam_id);
                $this->db->where('student_id', $row['student_id']);
                $marks = $this->db->get('mark')->result_array();
                $message = '';
                foreach ($marks as $row2) {
                    $subject = $this->db->get_where('subject', array('subject_id' => $row2['subject_id']))->row()->name;
                    $mark_obtained = $row2['total'];
                    $message .= $row2['student_id'] . $subject . ' : ' . $mark_obtained . ' , ';
                }
            // send sms
                $this->sms_model->send_sms($message, $receiver_phone);
            }
            $this->session->set_flashdata('flash_message', get_phrase('message_sent'));
            redirect(base_url() . 'index.php?admin/exam_marks_sms', 'refresh');
        }

        $page_data['page_name'] = 'exam_marks_sms';
        $page_data['page_title'] = get_phrase('send_marks_by_sms');
        $this->load->view('backend/index', $page_data);
    }

    /* * **MANAGE EXAM MARKS**** */

    function marks($exam_id = '', $course_id = '', $subject_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($this->input->post('operation') == 'selection') {
            $page_data['exam_id'] = $this->input->post('exam_id');
            $page_data['course_id'] = $this->input->post('course_id');
            $page_data['subject_id'] = $this->input->post('subject_id');

            if ($page_data['exam_id'] > 0 && $page_data['course_id'] > 0 && $page_data['subject_id'] > 0) {
                redirect(base_url() . 'index.php?admin/marks/' . $page_data['exam_id'] . '/' . $page_data['course_id'] . '/' . $page_data['subject_id'], 'refresh');
            } else {
                $this->session->set_flashdata('mark_message', 'Choose exam, course and subject');
                redirect(base_url() . 'index.php?admin/marks/', 'refresh');
            }
        }
        if ($this->input->post('operation') == 'update') {
            $data['mark_obtained'] = $this->input->post('mark_obtained');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('mark_id', $this->input->post('mark_id'));
            $this->db->update('mark', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/marks/' . $this->input->post('exam_id') . '/' . $this->input->post('course_id') . '/' . $this->input->post('subject_id'), 'refresh');
        }
        $page_data['exam_id'] = $exam_id;
        $page_data['course_id'] = $course_id;
        $page_data['subject_id'] = $subject_id;

        $page_data['page_info'] = 'Exam marks';

        $page_data['page_name'] = 'marks';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }

    /* * **MANAGE EXAM MARKS**** */

    function studentreport($month = '', $year = '', $course_id = '',$student_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['month'] = $month;
        $page_data['year'] = $year;
        $page_data['course_id'] = $course_id;
        $page_data['student_id'] = $student_id;
        $page_data['student_name']=     $this->crud_model->get_student_name($student_id);
        $page_data['page_name'] = 'studentreport';
        $page_data['page_title'] = get_phrase('Monthly_Report_of '). $page_data['student_name'];
        $this->load->view('backend/index', $page_data);
    }

    function studentreport_selector() {
        redirect(base_url() . 'index.php?admin/studentreport/' .
            $this->input->post('month') . '/' .
            $this->input->post('year') . '/' .
            $this->input->post('course_id') .'/'.            
            $this->input->post('student_id'), 'refresh');
    }

    /* * **MANAGE GRADES**** */

    function grade($param1 = '', $param2 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['grade_point'] = $this->input->post('grade_point');
            $data['mark_from'] = $this->input->post('mark_from');
            $data['mark_upto'] = $this->input->post('mark_upto');
            $data['comment'] = $this->input->post('comment');
            $data['for_marks'] = $this->input->post('for_marks');
            $this->db->insert('grade', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(base_url() . 'index.php?admin/grade/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['grade_point'] = $this->input->post('grade_point');
            $data['mark_from'] = $this->input->post('mark_from');
            $data['mark_upto'] = $this->input->post('mark_upto');
            $data['comment'] = $this->input->post('comment');
            $data['for_marks'] = $this->input->post('for_marks');
            $this->db->where('grade_id', $param2);
            $this->db->update('grade', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/grade/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('grade', array(
                'grade_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('grade_id', $param2);
            $this->db->delete('grade');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/grade/', 'refresh');
        }
        $page_data['grades'] = $this->db->get('grade')->result_array();
        $page_data['page_name'] = 'grade';
        $page_data['page_title'] = get_phrase('manage_grade');
        $this->load->view('backend/index', $page_data);
    }

    /* * ********MANAGING course ROUTINE***************** */

    function course_routine($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['course_id'] = $this->input->post('course_id');
            $data['subject_id'] = $this->input->post('subject_id');
            $data['time_start'] = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
            $data['time_end'] = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
            $data['day'] = $this->input->post('day');
            $this->db->insert('course_routine', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(base_url() . 'index.php?admin/course_routine/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['course_id'] = $this->input->post('course_id');
            $data['subject_id'] = $this->input->post('subject_id');
            $data['time_start'] = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
            $data['time_end'] = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
            $data['day'] = $this->input->post('day');

            $this->db->where('course_routine_id', $param2);
            $this->db->update('course_routine', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/course_routine/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('course_routine', array(
                'course_routine_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('course_routine_id', $param2);
            $this->db->delete('course_routine');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/course_routine/', 'refresh');
        }
        $page_data['page_name'] = 'course_routine';
        $page_data['page_title'] = get_phrase('manage_course_routine');
        $this->load->view('backend/index', $page_data);
    }

    /* * **** ID CARD **************** */
    function id_card($type = '', $course_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        $course_id = $this->input->post('course_id');
        $batch_id = $this->input->post('batch_id');
        $student_id = $this->input->post('student_id');

        if (!empty($student_id)) {

            $this->db->where(array(
                'course_id' => $course_id,
                'batch_id' => $batch_id,
                'student_id' => $student_id
            ));

        }

        elseif(empty($student_id)) {
            $course_id = $this->input->post('course_id');
            $batch_id = $this->input->post('batch_id');
            $this->db->where(array(
                'course_id' => $course_id,
                'batch_id' => $batch_id
            ));

        }


        $page_data['course_id'] = $this->input->post('course_id');
        $page_data['data'] = $this->db->get('student')->result_array();
        // echo $this->db->last_query();
        // die();
        $page_data['page_name'] = 'id_card';
        $page_data['page_title'] = get_phrase('certificates');
        $this->load->view('backend/index', $page_data);
    }

    /* * **** DAILY ATTENDANCE **************** */
    function daily_attendance($type = '', $month = '', $day = '', $year = '', $course_id = '', $single = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if (empty($year) || empty($month) || empty($day)) {
            $date = explode('-', date('Y-m-d'));
            $year = $date[0];
            $month = $date[1];
            $day = $date[2];
        }

        if (empty($course_id)) {
            $course_id = 'all';
        }

        if (empty($type)) {
            $type = 'student';
        }

        if ($_POST) {
            $_temp_url = $this->input->post('type') . '/' . $this->input->post('date');

            if ($this->input->post('type') == 'student')
                $_temp_url .= '/' . $this->input->post('course_id');

            redirect(base_url() . 'index.php?admin/daily_attendance/' . $_temp_url, 'refresh');
        }

        if ($type == 'student') {
            if ($course_id == 'all') {
                $_sql = "SELECT * FROM student
                LEFT JOIN (
                SELECT student_id as id, time, status, date FROM attendance                
                WHERE attendance.date = '$year-$month-$day') a ON (a.id = student.student_id)
                ORDER BY student.student_id ASC";
            } else {
                $_sql = "SELECT * FROM student
                LEFT JOIN (
                SELECT student_id as id, time, status, date FROM attendance                
                WHERE attendance.date = '$year-$month-$day') a ON (a.id = student.student_id)
                WHERE student.course_id = $course_id
                ORDER BY student.student_id ASC";
            }
            $_query = $this->db->query($_sql);
        } else if ($type == 'employee') {
            $_sql = "SELECT * FROM teacher
            LEFT JOIN (
            SELECT b.employee_id, MAX(b.time) as time, b.status, b.date FROM
            (SELECT * FROM employee_attendance ea 
            WHERE ea.date = '$year-$month-$day' ORDER BY ea.time DESC
            ) b GROUP BY b.employee_id
            ) a ON (a.employee_id = teacher.teacher_id)
            ORDER BY teacher.teacher_id ASC";
            $_query = $this->db->query($_sql);
        }

        $page_data['day'] = $day;
        $page_data['month'] = $month;
        $page_data['year'] = $year;
        $page_data['type'] = $type;
        $page_data['course_id'] = $course_id;
        $page_data['attendance'] = $_query->result_array();
        $page_data['page_name'] = 'daily_attendance';
        $page_data['page_title'] = get_phrase($type) . ' : ' . get_phrase('Daily_attendance') . ' of ' . $month . '-' . $day . '-' . $year;
        $this->load->view('backend/index', $page_data);
    }

    function attendance_report($type = '', $fdate = '', $tdate = '', $course_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if (empty($fdate) || empty($tdate)) {
            $fdate = date('m/d/Y');
            $tdate = date('m/d/Y');
        }

        if (empty($type)) {
            $type = 'student';
        }

        if (empty($course_id)) {
            $course_id = 'all';
        }

        if (!strstr($fdate, '/')) {
            $_fdate = date('Y-m-d', $fdate);
            $_tdate = date('Y-m-d', $tdate);
            $fdate = date('m/d/Y', $fdate);
            $tdate = date('m/d/Y', $tdate);
        } else {
            $_fdate = $fdate;
            $_tdate = $tdate;
        }

        $this->db->select('date');
        $this->db->distinct();

        if ($type == 'student') {
            $result = $this->db->get_where('attendance', array('status' => 1, 'date >=' => $_fdate, 'date <=' => $_tdate));
        } else if ($type == 'employee') {
            $result = $this->db->get_where('employee_attendance', array('status' => 1, 'date >=' => $_fdate, 'date <=' => $_tdate));
        }

        $query = $result->result_array();
        $workingdays = count($query);

        $this->db->select('*');

        if ($type == 'student') {
            $this->db->from('student');
            if ($course_id != 'all') {
                $this->db->where(array('student.course_id' => $course_id));
            }
            $this->db->order_by('student_id', 'asc');
        } else if ($type == 'employee') {
            $this->db->from('teacher');
            $this->db->order_by('teacher_id', 'asc');
        }

        $page_data['fdate'] = $fdate;
        $page_data['tdate'] = $tdate;
        $page_data['workingdays'] = $workingdays;
        $page_data['type'] = $type;
        $page_data['course_id'] = $course_id;
        $page_data['data'] = $this->db->get()->result_array();
        $page_data['page_name'] = 'attendance_report';
        $page_data['page_title'] = get_phrase($type) . ': ' . get_phrase('attendance_report') . ' of ' . str_replace('/','-',$fdate) . ' to ' . str_replace('/','-',$tdate);
        $this->load->view('backend/index', $page_data);
    }

    function attendancereport_selector()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $datefrom = strtotime($this->input->post('fdate'));
        $dateupto = strtotime($this->input->post('tdate'));

        $_temp_url = $this->input->post('type') .'/'. $datefrom . '/' . $dateupto;
        if ($this->input->post('type') == 'student') {
            $_temp_url .= '/' . $this->input->post('course_id');
        }

        redirect(base_url() . 'index.php?admin/attendance_report/' . $_temp_url, 'refresh');
    }

    /* * ****MANAGE BILLING / INVOICES WITH STATUS**** */

    function invoice($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['student_id'] = $this->input->post('student_id');
            $data['income_category_id'] = $this->input->post('income_category_id');
            $data['exam_id'] = $this->input->post('exam_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['amount'] = $this->input->post('amount');
            $data['amount_paid'] = $this->input->post('amount_paid');
            $data['due'] = $data['amount'] - $data['amount_paid'];
            $data['status'] = $this->input->post('status');
            $data['feesmonth'] = $this->input->post('month').'-'.$this->input->post('year');
            $data['creation_timestamp'] = strtotime($this->input->post('date'));

            $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            $data2['invoice_id'] = $invoice_id;
            $data2['student_id'] = $this->input->post('student_id');
            $data2['income_category_id'] = $this->input->post('income_category_id');
            $data2['title'] = $this->input->post('title');
            $data2['description'] = $this->input->post('description');
            $data2['payment_type'] = 'income';
            $data2['method'] = $this->input->post('method');
            $data2['amount'] = $this->input->post('amount_paid');
            $data2['timestamp'] = strtotime($this->input->post('date'));

            $this->db->insert('payment', $data2);

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(base_url() . 'index.php?admin/invoice', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['student_id'] = $this->input->post('student_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['amount'] = $this->input->post('amount');
            $data['status'] = $this->input->post('status');
            $data['creation_timestamp'] = strtotime($this->input->post('date'));

            $this->db->where('invoice_id', $param2);
            $this->db->update('invoice', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/invoice', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('invoice', array(
                'invoice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'take_payment') {
            $data['invoice_id'] = $this->input->post('invoice_id');
            $data['student_id'] = $this->input->post('student_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['payment_type'] = 'income';
            $data['method'] = $this->input->post('method');
            $data['amount'] = $this->input->post('amount');
            $data['timestamp'] = strtotime($this->input->post('timestamp'));
            $this->db->insert('payment', $data);

            $data2['amount_paid'] = $this->input->post('amount');
            $this->db->where('invoice_id', $param2);
            $this->db->set('amount_paid', 'amount_paid + ' . $data2['amount_paid'], FALSE);
            $this->db->set('due', 'due - ' . $data2['amount_paid'], FALSE);
            $this->db->update('invoice');

            $this->session->set_flashdata('flash_message', get_phrase('payment_successfull'));
            redirect(base_url() . 'index.php?admin/invoice', 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('invoice_id', $param2);
            $this->db->delete('invoice');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/invoice', 'refresh');
        }
        $page_data['page_name'] = 'invoice';
        $page_data['page_title'] = get_phrase('manage_invoice/payment');
        $this->load->view('backend/index', $page_data);
    }

    /* * ********ACCOUNTING******************* */
    function checkid($param1, $param2) {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if (!empty($param1) && !empty($param2)) {
            $_test = $this->db->get_where(trim($param1), array(trim($param1) . '_id' => trim($param2)))->result_array();

            if (!empty($_test)) {
                if (strlen($_test[0]['name']) > 0) {
                    echo 0;
                    return;
                }
            }
            echo 1;
        }
    }

    function payment($param1) {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $active_page = 'add';
        if ($param1 == 'add') {
            $from_account_id = $this->input->post('from_account_id');
            $course_id = $this->input->post('course_id');
            $batch_id = $this->input->post('batch_id');
            $student_id = $this->input->post('student_id');
            $session = $this->input->post('session');
            $exam_id = $this->input->post('exam_id');
            $month = $this->input->post('month');
            $to_account_id = $this->input->post('to_account_id');
            $date = $this->crud_model->mysqldate($this->input->post('date'));
            $memo = $this->input->post('memo');
            $total_amount = $this->input->post('total_amount');
            $paid_amount = $this->input->post('paid_amount');
            $status = $this->input->post('status');
            $method = $this->input->post('method');
            $check_number = $this->input->post('check_number');
            $card_number = $this->input->post('card_number');

        //Add Memo Entry
            $memo_data['details'] = $memo;
            $this->db->insert('a7', $memo_data);
            $memo_id = $this->db->insert_id();

        //Add Ref Entry
            $ref_data['a7_id'] = $memo_id;
            $ref_data['a2_id'] = $from_account_id;
            if (intval($student_id) > 0) {
                $ref_data['student_id'] = $student_id;
                $ref_data['course_id'] = $course_id;
                $ref_data['batch_id'] = $batch_id;
                if (intval($exam_id) > 0) {
                    $ref_data['exam_id'] = $exam_id;
                    $ref_data['session'] = $session;
                }
                if (intval($month) > 0)
                    $ref_data['month'] = $month;
            }
            $due_amount = (float)(floatval($total_amount) - floatval($paid_amount));
            $ref_data['employee_id'] = 0;
            $ref_data['date'] = $date;
            $ref_data['total_amount'] = $total_amount;
            $ref_data['status'] = $status;
            $this->db->insert('a1', $ref_data);
            $ref_id = $this->db->insert_id();

        //Add payment entry
            $payment_data['a1_id'] = $ref_id;
            $payment_data['amount'] = $paid_amount;
            $payment_data['date'] = $date;
            $payment_data['method'] = $method;
            if (intval($method) == 2)
                $payment_data['check_number'] = $check_number;
            if (intval($method) == 3)
                $payment_data['card_number'] = $card_number;
            $this->db->insert('a8', $payment_data);

        //Add GL Entry
            $gl_data_1['date'] = $date;
            $gl_data_1['a2_id'] = $from_account_id;
            $gl_data_1['a1_id'] = $ref_id;
            $gl_data_1['amount'] = $total_amount;
            $gl_data_1['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_1);

            $gl_data_2['date'] = $date;
            $gl_data_2['a2_id'] = $to_account_id;
            $gl_data_2['a1_id'] = $ref_id;
            $gl_data_2['amount'] = $paid_amount;
            $gl_data_2['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_2);

            if ($due_amount > 0) {
                $gl_data_3['date'] = $date;
                $gl_data_3['a2_id'] = 1200;
                $gl_data_3['a1_id'] = $ref_id;
                $gl_data_3['amount'] = $due_amount;
                $gl_data_3['user'] = $this->session->userdata('login_type');
                $this->db->insert('a6', $gl_data_3);
            }
        }
        if ($param1 == 'due') {
            $from_account_id = 1200;

            $to_account_id = $this->input->post('dto_account_id');
            $ref_id = $this->input->post('da1_id');
            $date = $this->crud_model->mysqldate($this->input->post('ddate'));
            $total_amount = $this->input->post('dtotal_amount');
            $prev_amount = $this->input->post('dprev_amount');
            $paid_amount = $this->input->post('dpaid_amount');
            $due_amount = (float)(floatval($total_amount) - (floatval($paid_amount) + floatval($prev_amount)));
            $method = $this->input->post('dmethod');
            $check_number = $this->input->post('dcheck_number');
            $card_number = $this->input->post('dcard_number');

        //Add GL Entry
            $gl_data_1['date'] = $date;
            $gl_data_1['a2_id'] = $from_account_id;
            $gl_data_1['a1_id'] = $ref_id;
            $gl_data_1['amount'] = '-' . $paid_amount;
            $gl_data_1['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_1);

            $gl_data_2['date'] = $date;
            $gl_data_2['a2_id'] = $to_account_id;
            $gl_data_2['a1_id'] = $ref_id;
            $gl_data_2['amount'] = $paid_amount;
            $gl_data_2['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_2);

        //Add payment entry
            $payment_data['a1_id'] = $ref_id;
            $payment_data['amount'] = $paid_amount;
            $payment_data['date'] = $date;
            $payment_data['method'] = $method;
            if (intval($method) == 2)
                $payment_data['check_number'] = $check_number;
            if (intval($method) == 3)
                $payment_data['card_number'] = $card_number;

            $this->db->insert('a8', $payment_data);

        //Update Ref Entry
            if ($due_amount <= 0) {
                $ref_data['status'] = '1';
                $this->db->where('a1_id', $ref_id);
                $this->db->update('a1', $ref_data);
            }
        }

        if ($param1 == 'filter' || $param1 == 'due')
            $active_page = 'list';

        $fromdate = $this->crud_model->mysqldate($this->input->post('fromdate'));
        $todate = $this->crud_model->mysqldate($this->input->post('todate'));
        if (strlen($fromdate) < 3)
            $fromdate = date('Y') . '-' . date('m') . '-1';
        if (strlen($todate) < 3)
            $todate = date('Y-m-t');

        $sql = 'SELECT a1.*
        FROM a1
        JOIN a2 ON a2.a2_id = a1.a2_id
        JOIN a3 ON a3.a3_id = a2.a3_id
        WHERE a3.a4_id = 3
        ORDER BY a1.a1_id ASC';

        $page_data['memos'] = $this->db->query($sql)->result_array();
        // echo $this->db->last_query();
        // die();
        $page_data['fromdate'] = date('m/d/Y', strtotime($fromdate));
        $page_data['todate'] = date('m/d/Y', strtotime($todate));

        $page_data['active_page'] = $active_page;
        $page_data['page_name'] = 'payment';
        $page_data['page_title'] = get_phrase('receive_payment');
        $this->load->view('backend/index', $page_data);
    }
    function expense($param1 = '', $param2 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $active_page = 'add';
        if ($param1 == 'add') {
            $from_account_id = $this->input->post('from_account_id');
            $to_account_id = $this->input->post('to_account_id');
            $account_balance = $this->igetbalance($from_account_id);
            $month = $this->input->post('month');
            $employee_id = $this->input->post('employee_id');
            $date = $this->crud_model->mysqldate($this->input->post('date'));
            $memo = $this->input->post('memo');
            $total_amount = $this->input->post('total_amount');
            $paid_amount = $this->input->post('paid_amount');
            $status = $this->input->post('status');
            if (floatval($account_balance) > 0) {
                if (floatval($paid_amount) > floatval($account_balance)) {
                    $paid_amount = $account_balance;
                    $status = 2;
                }
            } else {
                $paid_amount = 0;
                $status = 2;
            }
            $method = $this->input->post('method');
            $check_number = $this->input->post('check_number');
            $card_number = $this->input->post('card_number');

            //Add Memo Entry
            $memo_data['details'] = $memo;
            $this->db->insert('a7', $memo_data);
            $memo_id = $this->db->insert_id();

            //Add Ref Entry
            $ref_data['a7_id'] = $memo_id;
            $ref_data['a2_id'] = $to_account_id;
            if (intval($month) > 0)
                $ref_data['month'] = $month;

            $due_amount = (float)(floatval($total_amount) - floatval($paid_amount));
            if (intval($employee_id) > 0){
                $ref_data['employee_id'] = $employee_id;
                $for_month = $this->crud_model->getfullmonthname($month).'-'.$this->input->post('nm_year');
                $nm_t_paid = $this->db->get_where('teacher_salary',array('teacher_id' => $employee_id,'for_month' => $for_month))->row()->paid;
                $salary_data['paid']  = $nm_t_paid + $this->input->post('paid_amount');
                $salary_data['due'] = $this->input->post('total_amount') - $this->input->post('paid_amount');
                if($salary_data['due'] > 0){
                    $salary_data['status'] = 1;
                }
                elseif($salary_data['status'] == 0){
                 $salary_data['status'] = 0;
             }
             $this->db->where(array('teacher_id' => $employee_id,'for_month' => $for_month));
             $this->db->update('teacher_salary',$salary_data);

         }


         $ref_data['date'] = $date;
         $ref_data['total_amount'] = $total_amount;
         $ref_data['status'] = $status;
         $this->db->insert('a1', $ref_data);
         $ref_id = $this->db->insert_id();

         if (floatval($paid_amount) > 0) {
                //Add expense entry
            $expense_data['a1_id'] = $ref_id;
            $expense_data['amount'] = $paid_amount;
            $expense_data['date'] = $date;
            $expense_data['method'] = $method;
            if (intval($method) == 2)
                $expense_data['check_number'] = $check_number;
            if (intval($method) == 3)
                $expense_data['card_number'] = $card_number;
            $this->db->insert('a5', $expense_data);

                //Add GL Entry
            $gl_data_1['date'] = $date;
            $gl_data_1['a2_id'] = $from_account_id;
            $gl_data_1['a1_id'] = $ref_id;
            $gl_data_1['amount'] = '-' . $paid_amount;
            $gl_data_1['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_1);
        }
        $gl_data_2['date'] = $date;
        $gl_data_2['a2_id'] = $to_account_id;
        $gl_data_2['a1_id'] = $ref_id;
        $gl_data_2['amount'] = $total_amount;
        $gl_data_2['user'] = $this->session->userdata('login_type');
        $this->db->insert('a6', $gl_data_2);

        if ($due_amount > 0) {
            $gl_data_3['date'] = $date;
            $gl_data_3['a2_id'] = 2100;
            $gl_data_3['a1_id'] = $ref_id;
            $gl_data_3['amount'] = $due_amount;
            $gl_data_3['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_3);
        }
    }
    if ($param1 == 'due') {
        $to_account_id = 2100;
        $from_account_id = $this->input->post('dfrom_account_id');
        $ref_id = $this->input->post('da1_id');
        $account_balance = $this->igetbalance($from_account_id);
        $date = $this->crud_model->mysqldate($this->input->post('ddate'));
        $total_amount = $this->input->post('dtotal_amount');
        $prev_amount = $this->input->post('dprev_amount');
        $paid_amount = $this->input->post('dpaid_amount');
        if (floatval($account_balance) > 0) {
            if (floatval($paid_amount) > floatval($account_balance)) {
                $paid_amount = $account_balance;
            }
        } else {
            $paid_amount = 0;
        }
        $due_amount = (float)(floatval($total_amount) - (floatval($paid_amount) + floatval($prev_amount)));
        $method = $this->input->post('dmethod');
        $check_number = $this->input->post('dcheck_number');
        $card_number = $this->input->post('dcard_number');

        if (floatval($paid_amount) > 0) {
                //Add expense entry
            $expense_data['a1_id'] = $ref_id;
            $expense_data['amount'] = $paid_amount;
            $expense_data['date'] = $date;
            $expense_data['method'] = $method;
            if (intval($method) == 2)
                $expense_data['check_number'] = $check_number;
            if (intval($method) == 3)
                $expense_data['card_number'] = $card_number;

            $this->db->insert('a5', $expense_data);

                //Add GL Entry
            $gl_data_1['date'] = $date;
            $gl_data_1['a2_id'] = $from_account_id;
            $gl_data_1['a1_id'] = $ref_id;
            $gl_data_1['amount'] = '-' . $paid_amount;
            $gl_data_1['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_1);

            $gl_data_2['date'] = $date;
            $gl_data_2['a2_id'] = $to_account_id;
            $gl_data_2['a1_id'] = $ref_id;
            $gl_data_2['amount'] = '-' . $paid_amount;
            $gl_data_2['user'] = $this->session->userdata('login_type');
            $this->db->insert('a6', $gl_data_2);

                //Update Ref Entry
            if ($due_amount <= 0) {
                $ref_data['status'] = '1';
                $this->db->where('a1_id', $ref_id);
                $this->db->update('a1', $ref_data);
            }
        }
    }

    if ($param1 == 'filter' || $param1 == 'due')
        $active_page = 'list';

    $fromdate = $this->crud_model->mysqldate($this->input->post('fromdate'));
    $todate = $this->crud_model->mysqldate($this->input->post('todate'));
    if (strlen($fromdate) < 3)
        $fromdate = date('Y') . '-' . date('m') . '-1';
    if (strlen($todate) < 3)
        $todate = date('Y-m-t');

    $sql = 'SELECT a1.*
    FROM a1
    JOIN a2 ON a2.a2_id = a1.a2_id
    JOIN a3 ON a3.a3_id = a2.a3_id
    WHERE a3.a4_id = 4
    ORDER BY a1.a1_id ASC';

    $page_data['memos'] = $this->db->query($sql)->result_array();
    $page_data['fromdate'] = date('m/d/Y', strtotime($fromdate));
    $page_data['todate'] = date('m/d/Y', strtotime($todate));

    $page_data['active_page'] = $active_page;
    $page_data['page_name'] = 'expense';
    $page_data['page_title'] = get_phrase('manage_expense');
    $this->load->view('backend/index', $page_data);
}

function student_payment_report($month = '', $year = '', $course_id = '', $student_id = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $month = $this->input->post('month');
    $year = $this->input->post('year');

    if (strlen($month) <= 0)
        $month = date('m');

    if (strlen($year) <= 0)
        $year = date('Y');

    $course_id = $this->input->post('course_id');
    $student_id = $this->input->post('student_id');

    $fromdate = $year . '-' . $month . '-1';
    $todate = date('Y-m-t', strtotime($fromdate));

    $page_data['page_title'] = get_phrase('Monthly_Report');
    if (strlen($course_id) > 0 && strlen($student_id) > 0) {
        $this->db->select('a8.*, a1.a2_id, a1.a7_id, a1.total_amount');
        $this->db->from('a8');
        $this->db->join('a1', 'a1.a1_id=a8.a1_id');
        $this->db->where(array('a8.date>=' => $fromdate, 'a8.date<=' => $todate, 'a1.student_id' => $student_id));
        $this->db->order_by('a8.date', 'desc');
        $page_data['data'] = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('a1');
        $this->db->where(array('a1.date>=' => $fromdate, 'a1.date<=' => $todate, 'a1.student_id' => $student_id));
        $this->db->order_by('a1.date', 'desc');
        $page_data['ref'] = $this->db->get()->result_array();
        $page_data['student_name']=  $this->crud_model->get_student_name($student_id);
        $page_data['page_title'] = get_phrase('Monthly_Report_of '). $page_data['student_name'];
    }
    $page_data['month'] = $month;
    $page_data['year'] = $year;
    $page_data['course_id'] = $course_id;
    $page_data['student_id'] = $student_id;
    $page_data['page_name'] = 'student_payment_report';
    $this->load->view('backend/index', $page_data);
}
function journal_report($fromdate = '', $todate = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_str = $this->input->post('fromdate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $fromdate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $fromdate = strtotime(date('Y-m-') . '1');
    }
    $_str = '';
    $_str = $this->input->post('todate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $todate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $todate = strtotime(date('Y-m-t', $fromdate));
    }

    $this->db->select('a6.a6_id, 
     a6.a2_id, 
     a6.amount, 
     a6.user,
     a6.date,
     a1.a1_id,                            
     a1.a7_id');
    $this->db->from('a6');
    $this->db->join('a1', 'a1.a1_id=a6.a1_id');
    $this->db->where(array('a6.date >= ' => date('Y-m-d', $fromdate), 'a6.date <= ' => date('Y-m-d', $todate)));
    $this->db->order_by('a6.a1_id, a6.date','asc');
    $page_data['data'] = $this->db->get()->result_array();
        //$page_date['refs'] = $this->db->get_where('a1', array('a1.date >= ' => date('Y-m-d', $fromdate), 'a1.date <= ' => date('Y-m-d', $todate)))->result_array();
    $page_data['fromdate'] = $fromdate;
    $page_data['todate'] = $todate;
    $page_data['page_name'] = 'journal_report';
    $page_data['page_title'] = get_phrase('journal_report') . ' (' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')';
    $this->load->view('backend/index', $page_data);
}
function salary_report($fromdate = '', $todate = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_str = $this->input->post('fromdate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $fromdate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $fromdate = strtotime(date('Y-m-') . '1');
    }
    $_str = '';
    $_str = $this->input->post('todate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $todate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $todate = strtotime(date('Y-m-t', $fromdate));
    }

    $this->db->select('a5.*, a1.a2_id, teacher.name');
    $this->db->from('a5');
    $this->db->join('a1', 'a1.a1_id=a5.a1_id');
    $this->db->join('a2', 'a2.a2_id=a1.a2_id');
    $this->db->join('teacher', 'teacher.teacher_id = a1.employee_id');
    $this->db->where(array('a5.date >= ' => date('Y-m-d', $fromdate)));
    $this->db->where(array('a5.date <= ' => date('Y-m-d', $todate)));
    $this->db->where(array('a2.a3_id' => '11'));
    $page_data['data'] = $this->db->get()->result_array();
    $page_data['fromdate'] = $fromdate;
    $page_data['todate'] = $todate;
    $page_data['page_name'] = 'salary_report';
    $page_data['page_title'] = get_phrase('salary_report') . ' (' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')';
    $this->load->view('backend/index', $page_data);
}
function balance_sheet($fromdate = '', $todate = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_str = $this->input->post('fromdate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $fromdate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $fromdate = strtotime(date('Y-m-') . '1');
    }
    $_str = '';
    $_str = $this->input->post('todate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $todate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $todate = strtotime(date('Y-m-t', $fromdate));
    }

    $this->db->select('a6.a6_id, 
     a6.date, 
     a6.a2_id, 
     a6.a1_id, 
     SUM(a6.amount) as balance, 
     a2.name as a2, 
     a3.name as a3, 
     a4.name as a4,
     a1.a2_id as t_id');
    $this->db->from('a6');
    $this->db->join('a2', 'a2.a2_id = a6.a2_id');
    $this->db->join('a3', 'a3.a3_id = a2.a3_id');
    $this->db->join('a4', 'a4.a4_id = a3.a4_id');
    $this->db->join('a1', 'a1.a1_id = a6.a1_id');
    $this->db->where(array('a6.date >= ' => date('Y-m-d', $fromdate)));
    $this->db->where(array('a6.date <= ' => date('Y-m-d', $todate)));
    $this->db->where(array('a4.a4_id<=' => '2'));
    $this->db->order_by('a4.a4_id, a3.a3_id, a2.a2_id');
    $this->db->group_by('a6.a2_id');
    $page_data['data'] = $this->db->get()->result_array();
    $page_data['fromdate'] = $fromdate;
    $page_data['todate'] = $todate;
    $page_data['page_name'] = 'balance_sheet';
    $page_data['page_title'] = get_phrase('balance_sheet') . ' (' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')';
    $this->load->view('backend/index', $page_data);
}
function getreportpdf() {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_data = $this->input->post('body');
    $_header = $this->input->post('header');
    $_footer = $this->input->post('footer');
    $_style = $this->input->post('style');
    $_file_name = $this->input->post('filename') . '.pdf';
    $_page_size = $this->input->post('pagesize');
    if (strlen($_data) > 0 ) {
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        require_once('mpdf60/mpdf.php');

        $mpdf = new mPDF('C', $_page_size, '','', 15, 15, 40, 20);
        $mpdf->useOnlyCoreFonts = false;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($_style, 1);
        $mpdf->SetHTMLHeader($_header, 'E', true);
        $mpdf->SetHTMLFooter($_footer, 'E');
        $mpdf->SetHTMLHeader($_header, 'O', true);
        $mpdf->SetHTMLFooter($_footer, 'O');
        $mpdf->WriteHTML($_data);
        $mpdf->Output('uploads/' . $_file_name, 'F');
        echo base_url() . "uploads/$_file_name";
    }
    exit(0);
}
function gl_report($fromdate = '', $todate = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_str = $this->input->post('fromdate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $fromdate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $fromdate = strtotime(date('Y-m-') . '1');
    }
    $_str = '';
    $_str = $this->input->post('todate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $todate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $todate = strtotime(date('Y-m-t', $fromdate));
    }

    $this->db->select('a6.a6_id, 
     a6.a2_id, 
     a6.amount, 
     a6.user,
     a6.date,
     a1.a1_id, 
     a1.a7_id');
    $this->db->from('a6');
    $this->db->join('a1', 'a1.a1_id=a6.a1_id');
    $this->db->where(array('a6.date >= ' => date('Y-m-d', $fromdate), 'a6.date <= ' => date('Y-m-d', $todate)));
    $this->db->order_by('a6.a1_id, a6.date','asc');
        //$this->db->group_by('a6.a1_id');
    $page_data['data'] = $this->db->get()->result_array();
        //$page_date['refs'] = $this->db->get_where('a1', array('a1.date >= ' => date('Y-m-d', $fromdate), 'a1.date <= ' => date('Y-m-d', $todate)))->result_array();
    $page_data['fromdate'] = $fromdate;
    $page_data['todate'] = $todate;
    $page_data['page_name'] = 'gl_report';
    $page_data['page_title'] = get_phrase('G/L_report') . ' (' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')';
    $this->load->view('backend/index', $page_data);
}

function income_expense_report($fromdate = '', $todate = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    $_str = $this->input->post('fromdate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $fromdate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $fromdate = strtotime(date('Y-m-') . '1');
    }
    $_str = '';
    $_str = $this->input->post('todate');
    if (strlen($_str) > 0) {
        $_temp = explode('/', $_str);
        $todate = strtotime($_temp[2] . '-' . $_temp[0] . '-' . $_temp[1]);
    } else {
        $todate = strtotime(date('Y-m-t', $fromdate));
    }
    $page_data['income'] = $this->db->get_where('a8', array('date >= ' => date('Y-m-d', $fromdate), 'date <= ' => date('Y-m-d', $todate)))->result_array();
    $page_data['expense'] = $this->db->get_where('a5', array('date >= ' => date('Y-m-d', $fromdate), 'date <= ' => date('Y-m-d', $todate)))->result_array();
    $page_data['fromdate'] = $fromdate;
    $page_data['todate'] = $todate;
    $page_data['page_name'] = 'income_expense_report';
    $page_data['page_title'] = get_phrase('income') . '/' . get_phrase('expense_report') . ' (' . date('m/d/Y', $fromdate) . ' - ' . date('m/d/Y', $todate) . ')';
    $this->load->view('backend/index', $page_data);
}
function getbalance($a2_id) {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if (intval($a2_id) <= 0) {
        echo $this->crud_model->money(0);
        return;
    }

    $sql = "SELECT SUM(amount) as balance FROM a6 WHERE a6.a2_id = $a2_id";
    $val = $this->db->query($sql)->result_array()[0]['balance'];
    if (strlen($val) <= 0)
        $val = '0';

    echo $this->crud_model->money($val);
}
function igetbalance($a2_id) {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if (intval($a2_id) <= 0) {
        return $this->crud_model->money(0);
    }

    $sql = "SELECT SUM(amount) as balance FROM a6 WHERE a6.a2_id = $a2_id";
    $val = $this->db->query($sql)->result_array()[0]['balance'];
    if (strlen($val) <= 0)
        $val = '0';

    return $this->crud_model->money($val);
}
function income($timestamp) {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = $timestamp;
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'income';
    $page_data['page_title'] = get_phrase('incomes: ').date('M Y',$data['monthfrom']);
    $this->load->view('backend/index', $page_data);
}
function incomedaily($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = strtotime($this->input->post('datefrom'));
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'incomedaily';    
    $page_data['page_title'] = 'income_Date '. date('d M Y',$data['monthfrom']);
    $this->load->view('backend/index', $page_data);
}
function incomemonthly($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = strtotime($this->input->post('datefrom'));
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'incomemonthly';

    $page_data['page_title'] = 'monthly incomes '. date('M',$data['monthfrom']);
    $this->load->view('backend/index', $page_data);
}


function expensemonthly($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = strtotime($this->input->post('datefrom'));
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'expensemonthly';
    $page_data['page_title'] = 'monthly expense  ' . date('M',$data['monthfrom']);
    $this->load->view('backend/index', $page_data);
}
function expense_category($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $this->db->insert('expense_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }
    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $this->db->where('expense_category_id', $param2);
        $this->db->update('expense_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }
    if ($param1 == 'delete') {
        $this->db->where('expense_category_id', $param2);
        $this->db->delete('expense_category');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }

    $page_data['page_name'] = 'expense_category';
    $page_data['page_title'] = get_phrase('expense_category');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE DEPARTMENT******************* */
function department($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['comment'] = $this->input->post('comment');
        $this->db->insert('department', $data);
        $this->session->set_flashdata('flash_message', get_phrase('department_added_successfully'));
        redirect(base_url() . 'index.php?admin/department/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['comment'] = $this->input->post('comment');

        $this->db->where('department_id', $param3);
        $this->db->update('department', $data);
        $this->session->set_flashdata('flash_message', get_phrase('department_updated'));
        redirect(base_url() . 'index.php?admin/department/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('department', array(
            'department_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('department_id', $param2);
        $this->db->delete('department');
        $this->session->set_flashdata('flash_message', get_phrase('department_deleted'));
        redirect(base_url() . 'index.php?admin/department/', 'refresh');
    }
    $page_data['department'] = $this->db->get('department')->result_array();
    $page_data['page_name'] = 'department';
    $page_data['page_title'] = get_phrase('manage_departments');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE LIBRARY / BOOKS******************* */
function book_category($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $this->db->insert('book_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/book_category/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['name'] = $this->input->post('name');

        $this->db->where('book_category_id', $param3);
        $this->db->update('book_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/book_category/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('book_category', array(
            'book_category_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('book_category_id', $param2);
        $this->db->delete('book_category');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/book_category/', 'refresh');
    }
    $page_data['book_category'] = $this->db->get('book_category')->result_array();
    $page_data['page_name'] = 'book_category';
    $page_data['page_title'] = get_phrase('manage_book_category');
    $this->load->view('backend/index', $page_data);
}

function book_lease($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'lease') {
        $data['leasee_id'] = $this->input->post('student_id');
        $data['book_id'] = $this->input->post('book_id');
        $data['lease_date'] = date('Y-m-d');
        $data['lease_days'] = $this->input->post('days');
        $data['status'] = 0;
        $data['comment'] = 'N/A';

        $this->db->insert('book_lease', $data);

        $book = $this->db->get_where('book', array('book_id' => $data['book_id']))->row_array();

        $_int_avail = (int)$book['available'];
        $_int_lease_count = (int)$book['lease_count'];

        if ($_int_avail > 0) {
            $data2['available'] = $_int_avail - 1;
            $data2['lease_count'] = $_int_lease_count + 1;
            $this->db->where('book_id', $data['book_id']);
            $this->db->update('book', $data2);
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/book_lease', 'refresh');
    }
    if ($param1 == 'return') {
        $data['status'] = 1;

        $this->db->where('book_lease_id', $param3);
        $this->db->update('book_lease', $data);

        $book = $this->db->get_where('book', array('book_id' => $param2))->row_array();

        $_int_avail = (int)$book['available'];
        $_int_quant = (int)$book['quantity'];

        if ($_int_avail < $_int_quant) {
            $data2['available'] = $_int_avail + 1;
            $this->db->where('book_id', $param2);
            $this->db->update('book', $data2);
        }

        $this->session->set_flashdata('flash_message', get_phrase('book_returned_successfully'));
        redirect(base_url() . 'index.php?admin/book_lease', 'refresh');
    }

    $page_data['book_lease'] = $this->db->get('book_lease')->result_array();
    $page_data['page_name'] = 'book_lease';
    $page_data['page_title'] = get_phrase('issue_books');
    $this->load->view('backend/index', $page_data);
}

function book($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['isbn'] = $this->input->post('isbn');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['author'] = $this->input->post('author');
        $data['course_id'] = $this->input->post('course_id');
        $data['status'] = 'N/A';
        $data['publisher'] = $this->input->post('publisher');
        $data['quantity'] = $this->input->post('quantity');
        $data['available'] = $this->input->post('quantity');
        $data['lease_count'] = 0;
        $data['locked'] = $this->input->post('locked');
        $data['book_category_id'] = $this->input->post('book_category_id');

        $this->db->insert('book', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['author'] = $this->input->post('author');
        $data['course_id'] = $this->input->post('course_id');
        $data['status'] = 'N/A';
        $data['publisher'] = $this->input->post('publisher');
        $data['quantity'] = $this->input->post('quantity');
        $data['available'] = $this->input->post('quantity');
        $data['lease_count'] = 0;
        $data['locked'] = $this->input->post('locked');
        $data['book_category_id'] = $this->input->post('book_category_id');

        $this->db->where('book_id', $param2);
        $this->db->update('book', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('book', array(
            'book_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('book_id', $param2);
        $this->db->delete('book');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    }
    $page_data['books'] = $this->db->get('book')->result_array();
    $page_data['page_name'] = 'book';
    $page_data['page_title'] = get_phrase('manage_library_books');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE TRANSPORT / VEHICLES / ROUTES******************* */

function transport($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['route_name'] = $this->input->post('route_name');
        $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
        $data['description'] = $this->input->post('description');
        $data['route_fare'] = $this->input->post('route_fare');
        $this->db->insert('transport', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['route_name'] = $this->input->post('route_name');
        $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
        $data['description'] = $this->input->post('description');
        $data['route_fare'] = $this->input->post('route_fare');

        $this->db->where('transport_id', $param2);
        $this->db->update('transport', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('transport', array(
            'transport_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('transport_id', $param2);
        $this->db->delete('transport');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    }
    $page_data['transports'] = $this->db->get('transport')->result_array();
    $page_data['page_name'] = 'transport';
    $page_data['page_title'] = get_phrase('manage_transport');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE DORMITORY / HOSTELS / ROOMS ******************* */

function dormitory($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['number_of_room'] = $this->input->post('number_of_room');
        $data['description'] = $this->input->post('description');
        $this->db->insert('dormitory', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['number_of_room'] = $this->input->post('number_of_room');
        $data['description'] = $this->input->post('description');

        $this->db->where('dormitory_id', $param2);
        $this->db->update('dormitory', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('dormitory', array(
            'dormitory_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('dormitory_id', $param2);
        $this->db->delete('dormitory');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    }
    $page_data['dormitories'] = $this->db->get('dormitory')->result_array();
    $page_data['page_name'] = 'dormitory';
    $page_data['page_title'] = get_phrase('manage_dormitory');
    $this->load->view('backend/index', $page_data);
}

/* * *MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD* */

function noticeboard($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {
        $data['notice_title'] = $this->input->post('notice_title');
        $data['notice'] = $this->input->post('notice');
        $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
        $this->db->insert('noticeboard', $data);

        $check_sms_send = $this->input->post('check_sms');

        if ($check_sms_send == 1) {
            // sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $students = $this->db->get('student')->result_array();
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('from') . $system_name . '<br>' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }
        if ($check_sms_send == 2) {
            // sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }

        }
        if ($check_sms_send == 3) {
            // sms sending configurations
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }
        if ($check_sms_send == 4) {
            // sms sending configurations
            $students = $this->db->get('student')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['notice_title'] = $this->input->post('notice_title');
        $data['notice'] = $this->input->post('notice');
        $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
        $this->db->where('notice_id', $param2);
        $this->db->update('noticeboard', $data);

        $check_sms_send = $this->input->post('check_sms');

        if ($check_sms_send == 1) {
            // sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $students = $this->db->get('student')->result_array();
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }
        if ($check_sms_send == 2) {
            // sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }

        }
        if ($check_sms_send == 3) {
            // sms sending configurations
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }
        if ($check_sms_send == 4) {
            // sms sending configurations
            $students = $this->db->get('student')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
            'notice_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('notice_id', $param2);
        $this->db->delete('noticeboard');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    }
    $page_data['page_name'] = 'noticeboard';
    $page_data['page_title'] = get_phrase('manage_noticeboard');
    $page_data['notices'] = $this->db->get('noticeboard')->result_array();
    $this->load->view('backend/index', $page_data);
}
/* * *MANAGE SMS* */

function sms($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');       
    if ($param1 == 'tentative_student') {        

        $data['receiver'] =  $this->db->get_where('tentative_student', array('tentative_student_id' => $param2))->row()->contact_no;        
        $data['message'] = $this->input->post('message');
        $data['timestamp'] = strtotime( date('d M,Y'));
        $data['student_id'] = 'TS ,'.$param2;
        $date = date('d M,Y');
        $message = $data['message'] . ' ';
        $message .= $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
        $reciever_phone =  $data['receiver'] ;
        $reply = $this->sms_model->send_sms($message, $reciever_phone);    
        // echo '<pre>';
        // print_r($data);
        // die(); 
        $this->db->insert('sms', $data);
        // $course_id = $this->db->get_where('student', array('student_id' => $param2))->row()->course_id;
        $this->session->set_flashdata('flash_message'.$reply);
        redirect(base_url() . 'index.php?admin/sms/', 'refresh');
    }
  
    if ($param1 == 'student') {        

        $data['receiver'] =  $this->db->get_where('student', array('student_id' => $param2))->row()->phone;        
        $data['message'] = $this->input->post('message');
        $data['timestamp'] = strtotime( date('d M,Y'));
        $data['student_id'] = $param2;
        $date = date('d M,Y');
        $message = $data['message'] . ' ';
        $message .= $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
        $reciever_phone =  $data['receiver'] ;
        $reply = $this->sms_model->send_sms($message, $reciever_phone);     
        $this->db->insert('sms', $data);
        $course_id = $this->db->get_where('student', array('student_id' => $param2))->row()->course_id;
        $this->session->set_flashdata('flash_message'.$reply);
        redirect(base_url() . 'index.php?admin/sms/', 'refresh');
    }

    if ($param1 == 'teacher') {        
        $data['receiver'] =  $this->db->get_where('teacher', array('teacher_id' => $param2))->row()->phone;        
        $data['message'] = $this->input->post('message');
        $data['timestamp'] = strtotime( date('d M,Y'));
        $data['teacher_id'] = $param2;
        $date = date('d M,Y');
        $message = $data['message'] . ' ';
        $message .= $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
        $reciever_phone =  $data['receiver'] ;
        $reply = $this->sms_model->send_sms($message, $reciever_phone);     
        $this->db->insert('sms', $data);           
        $this->session->set_flashdata('flash_message', get_phrase('message_sent'));
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    }
    if ($param1 == 'parent') {        
        $data['receiver'] =  $this->db->get_where('parent', array('parent_id' => $param2))->row()->phone;        
        $data['message'] = $this->input->post('message');
        $data['timestamp'] = strtotime( date('d M,Y'));
        $data['parent_id'] = $param2;      
        $date = date('d M,Y');
        $message = $data['message'] . ' ';
        $message .= $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
        $reciever_phone =  $data['receiver'] ;
        $reply = $this->sms_model->send_sms($message, $reciever_phone);     
        $this->db->insert('sms', $data); 
        $this->session->set_flashdata('flash_message', get_phrase('message_sent'));
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('sms_id', $param2);
        $this->db->delete('sms');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/sms/', 'refresh');
    }
    $page_data['page_name'] = 'sms';
    $page_data['page_title'] = get_phrase('sms_list');  
    $this->db->order_by('timestamp','desc');
    $page_data['smss'] = $this->db->get('sms')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* private messaging */

function message($param1 = 'message_home', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'send_new') {
        $message_thread_code = $this->crud_model->send_new_private_message();
        $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
        redirect(base_url() . 'index.php?admin/message/message_read/' . $message_thread_code, 'refresh');
    }

    if ($param1 == 'send_reply') {
        $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
        $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
        redirect(base_url() . 'index.php?admin/message/message_read/' . $param2, 'refresh');
    }

    if ($param1 == 'message_read') {
        $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
        $this->crud_model->mark_thread_messages_read($param2);
    }

    $page_data['message_inner_page_name'] = $param1;
    $page_data['page_name'] = 'message';
    $page_data['page_title'] = get_phrase('private_messaging');
    $this->load->view('backend/index', $page_data);
}

/* * ***SITE/SYSTEM SETTINGS******** */

function system_settings($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'do_update') {

        $data['description'] = $this->input->post('system_name');
        $this->db->where('type', 'system_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_title');
        $this->db->where('type', 'system_title');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('address');
        $this->db->where('type', 'address');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('phone');
        $this->db->where('type', 'phone');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('paypal_email');
        $this->db->where('type', 'paypal_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('currency');
        $this->db->where('type', 'currency');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_email');
        $this->db->where('type', 'system_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_name');
        $this->db->where('type', 'system_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('language');
        $this->db->where('type', 'language');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('text_align');
        $this->db->where('type', 'text_align');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('id_card_validity');
        $this->db->where('type', 'id_card_validity');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('student_id_card_prefix');
        $this->db->where('type', 'student_id_card_prefix');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('employee_id_card_prefix');
        $this->db->where('type', 'employee_id_card_prefix');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('student_attendance_sms_message');
        $this->db->where('type', 'student_attendance_sms_message');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    if ($param1 == 'upload_logo') {
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    if ($param1 == 'change_skin') {
        $data['description'] = $param2;
        $this->db->where('type', 'skin_colour');
        $this->db->update('settings', $data);
        $this->session->set_flashdata('flash_message', get_phrase('theme_selected'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    $page_data['page_name'] = 'system_settings';
    $page_data['page_title'] = get_phrase('system_settings');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ***SMS SETTINGS******** */
function sms_subscribers_edit_list()
{
    if ($this->session->userdata('admin_login') != 1) {
        echo base_url();
        return;
        //redirect(base_url(), 'refresh');
    }

    $_set = $this->input->post('param0');
    $_parent_id = $this->input->post('param1');

    if ($_set == 'enable') {
        $data['subscriber_id'] = $_parent_id;
        $this->db->insert('sms_subscribers', $data);
    }

    if ($_set == 'disable') {
        $this->db->where('subscriber_id', $_parent_id);
        $this->db->delete('sms_subscribers');
    }

    echo 'ok';
}

function sms_subscribers($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'enable') {
        $data['subscriber_id'] = $param2;
        $this->db->insert('sms_subscribers',$data);
    }

    if ($param1 == 'disable') {
        $this->db->where('subscriber_id', $param2);
        $this->db->delete('sms_subscribers');
    }

    $_sql = 'SELECT parent_id, name, phone, student_name, sms_subscribers.subscriber_id as sub_id FROM parent INNER JOIN
    (SELECT name as student_name, parent_id as pid FROM student
    WHERE student.parent_id > 0 GROUP BY student.parent_id
) a on a.pid = parent.parent_id LEFT JOIN sms_subscribers ON sms_subscribers.subscriber_id = parent.parent_id';

$_query = $this->db->query($_sql);

$page_data['page_name'] = 'sms_subscribers';
$page_data['page_title'] = get_phrase('sms_subscribers');
$page_data['parent'] = $_query->result_array();
$this->load->view('backend/index', $page_data);
}

function sms_settings($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'robi') {

        $data['description'] = $this->input->post('robi_user');
        $this->db->where('type', 'robi_user_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('robi_password');
        $this->db->where('type', 'robi_password');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('robi_sender_phone_number');
        $this->db->where('type', 'robi_sender_phone_number');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'itsolutionbd') {

        $data['description'] = $this->input->post('itsolutionbd_user_name');
        $this->db->where('type', 'itsolutionbd_user_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('itsolutionbd_password');
        $this->db->where('type', 'itsolutionbd_password');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('itsolutionbd_sender_name');
        $this->db->where('type', 'itsolutionbd_sender_name');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'clickatell') {

        $data['description'] = $this->input->post('clickatell_user');
        $this->db->where('type', 'clickatell_user');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_password');
        $this->db->where('type', 'clickatell_password');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_api_id');
        $this->db->where('type', 'clickatell_api_id');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'twilio') {

        $data['description'] = $this->input->post('twilio_account_sid');
        $this->db->where('type', 'twilio_account_sid');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('twilio_auth_token');
        $this->db->where('type', 'twilio_auth_token');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('twilio_sender_phone_number');
        $this->db->where('type', 'twilio_sender_phone_number');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'active_service') {

        $data['description'] = $this->input->post('active_sms_service');
        $this->db->where('type', 'active_sms_service');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    $page_data['page_name'] = 'sms_settings';
    $page_data['page_title'] = get_phrase('sms_settings');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ***LANGUAGE SETTINGS******** */

function manage_language($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'edit_phrase') {
        $page_data['edit_profile'] = $param2;
    }
    if ($param1 == 'update_phrase') {
        $language = $param2;
        $total_phrase = $this->input->post('total_phrase');
        for ($i = 1; $i < $total_phrase; $i++) {
            //$data[$language]	=	$this->input->post('phrase').$i;
            $this->db->where('phrase_id', $i);
            $this->db->update('language', array($language => $this->input->post('phrase' . $i)));
        }
        redirect(base_url() . 'index.php?admin/manage_language/edit_phrase/' . $language, 'refresh');
    }
    if ($param1 == 'do_update') {
        $language = $this->input->post('language');
        $data[$language] = $this->input->post('phrase');
        $this->db->where('phrase_id', $param2);
        $this->db->update('language', $data);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'add_phrase') {
        $data['phrase'] = $this->input->post('phrase');
        $this->db->insert('language', $data);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'add_language') {
        $language = $this->input->post('language');
        $this->load->dbforge();
        $fields = array(
            $language => array(
                'type' => 'LONGTEXT'
            )
        );
        $this->dbforge->add_column('language', $fields);

        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'delete_language') {
        $language = $param2;
        $this->load->dbforge();
        $this->dbforge->drop_column('language', $language);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));

        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    $page_data['page_name'] = 'manage_language';
    $page_data['page_title'] = get_phrase('manage_language');
    //$page_data['language_phrases'] = $this->db->get('language')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ***BACKUP / RESTORE / DELETE DATA PAGE********* */

function backup_restore($operation = '', $type = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($operation == 'create') {
        $this->crud_model->create_backup($type);
    }
    if ($operation == 'restore') {
        $this->crud_model->restore_backup();
        $this->session->set_flashdata('backup_message', 'Backup Restored');
        redirect(base_url() . 'index.php?admin/backup_restore/', 'refresh');
    }
    if ($operation == 'delete') {
        $this->crud_model->truncate($type);
        $this->session->set_flashdata('backup_message', 'Data removed');
        redirect(base_url() . 'index.php?admin/backup_restore/', 'refresh');
    }

    $page_data['page_info'] = 'Create backup / restore from backup';
    $page_data['page_name'] = 'backup_restore';
    $page_data['page_title'] = get_phrase('manage_backup_restore');
    $this->load->view('backend/index', $page_data);
}

/* * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */

function manage_profile($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'update_profile_info') {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');

        $this->db->where('admin_id', $this->session->userdata('admin_id'));
        $this->db->update('admin', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/admin_image/' . $this->session->userdata('admin_id') . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
        redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
    }
    if ($param1 == 'change_password') {
        $data['password'] = $this->input->post('password');
        $data['new_password'] = $this->input->post('new_password');
        $data['confirm_new_password'] = $this->input->post('confirm_new_password');

        $current_password = $this->db->get_where('admin', array(
            'admin_id' => $this->session->userdata('admin_id')
        ))->row()->password;
        if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
            $this->db->where('admin_id', $this->session->userdata('admin_id'));
            $this->db->update('admin', array(
                'password' => $data['new_password']
            ));
            $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
        } else {
            $this->session->set_flashdata('flash_message', get_phrase('password_mismatch'));
        }
        redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
    }
    $page_data['page_name'] = 'manage_profile';
    $page_data['page_title'] = get_phrase('manage_profile');
    $page_data['edit_data'] = $this->db->get_where('admin', array(
        'admin_id' => $this->session->userdata('admin_id')
    ))->result_array();
    $this->load->view('backend/index', $page_data);
}

/***********input history data**************/

function history_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['history'] = $this->input->post('history');

        $this->db->insert('history', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/history_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['history'] = $this->input->post('history');

        $this->db->where('history_id', $param2);
        $this->db->update('history', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/history_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('history', array(
            'history_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('history_id', $param2);
        $this->db->delete('history');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/history_information/', 'refresh');
    }
    $page_data['page_name'] = 'history';
    $page_data['page_title'] = get_phrase('manage_history');
    $page_data['histories'] = $this->db->get('history')->result_array();
    $this->load->view('backend/index', $page_data);
}

/***********input at a glance data**************/

function ataglance_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['ataglance'] = $this->input->post('ataglance');

        $this->db->insert('ataglance', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/ataglance_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['ataglance'] = $this->input->post('ataglance');

        $this->db->where('ataglance_id', $param2);
        $this->db->update('ataglance', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/ataglance_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('ataglance', array(
            'ataglance_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('ataglance_id', $param2);
        $this->db->delete('ataglance');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/ataglance_information/', 'refresh');
    }
    $page_data['page_name'] = 'ataglance';
    $page_data['page_title'] = get_phrase('At a glance');
    $page_data['ataglances'] = $this->db->get('ataglance')->result_array();
    $this->load->view('backend/index', $page_data);
}
/* * **MANAGEMENT COMMITTEE**** */

function managementcommitte($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['position'] = $this->input->post('position');
        $data['proffession'] = $this->input->post('proffession');
        $data['address'] = $this->input->post('address');
        $data['email'] = $this->input->post('email');
        $this->db->insert('mgtcommitte', $data);
        $mgtcommitte_id = $this->db->insert_id();
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/mgtcommitte_image/' . $mgtcommitte_id. '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/managementcommitte/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['position'] = $this->input->post('position');
        $data['proffession'] = $this->input->post('proffession');
        $data['address'] = $this->input->post('address');
        $data['email'] = $this->input->post('email');
        $this->db->where('mgtcommitte_id', $param2);
        $this->db->update('mgtcommitte', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/mgtcommitte_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/managementcommitte/', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile'] = true;
        $page_data['current_mgtcommitte_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('mgtcommitte', array(
            'mgtcommitte_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('mgtcommitte_id', $param2);
        $this->db->delete('mgtcommitte');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/managementcommitte/', 'refresh');
    }
    $page_data['mgtcommitties'] = $this->db->get('mgtcommitte')->result_array();
    $page_data['page_name'] = 'mgtcommitte';
    $page_data['page_title'] = get_phrase('management_committe');
    $this->load->view('backend/index', $page_data);
}
/////////////principal//////////////
function principal($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['qualifications'] = $this->input->post('qualifications');
        $data['address'] = $this->input->post('address');
        $data['message'] = $this->input->post('message');
        $data['email'] = $this->input->post('email');
        $this->db->insert('principal', $data);
        $principal_id = $this->db->insert_id();
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/principal_image/' . $principal_id. '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/principal/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['qualifications'] = $this->input->post('qualifications');
        $data['address'] = $this->input->post('address');
        $data['message'] = $this->input->post('message');
        $data['email'] = $this->input->post('email');
        $this->db->where('principal_id', $param2);
        $this->db->update('principal', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/principal_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/principal/', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile'] = true;
        $page_data['current_principal_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('principal', array(
            'principal_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('principal_id', $param2);
        $this->db->delete('principal');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/principal/', 'refresh');
    }
    $page_data['principals'] = $this->db->get('principal')->result_array();
    $page_data['page_name'] = 'principal';
    $page_data['page_title'] = get_phrase('principal');
    $this->load->view('backend/index', $page_data);
}
/* * **MANAGE EMPLOYEES**** */

function employee($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
       // $data['birthday'] = $this->input->post('birthday');
        $data['post'] = $this->input->post('post');
        $data['address'] = $this->input->post('address');
       // $data['phone'] = $this->input->post('phone');
       // $data['email'] = $this->input->post('email');
       // $data['password'] = $this->input->post('password');
        $this->db->insert('employee', $data);
        $employee_id = $this->db->insert_id();
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/employee_image/' . $employee_id. '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        //$this->email_model->account_opening_email('employee', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'index.php?admin/employee/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['post'] = $this->input->post('post');
       // $data['sex'] = $this->input->post('sex');
        $data['address'] = $this->input->post('address');
      //  $data['phone'] = $this->input->post('phone');
       // $data['email'] = $this->input->post('email');

        $this->db->where('employee_id', $param2);
        $this->db->update('employee', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/employee_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/employee/', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile'] = true;
        $page_data['current_employee_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('employee', array(
            'employee_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('employee_id', $param2);
        $this->db->delete('employee');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/employee/', 'refresh');
    }
    $page_data['employees'] = $this->db->get('employee')->result_array();
    $page_data['page_name'] = 'employee';
    $page_data['page_title'] = get_phrase('manage_employee');
    $this->load->view('backend/index', $page_data);
}

///////////////BALANCE SHEET//////////////
function balancesheet($timestamp) {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = $timestamp;
    $page_data['page_name'] = 'balancesheet';
    $page_data['monthfrom'] =  $data['monthfrom'];    
    $page_data['page_title'] = get_phrase('Balance Sheet for: ').date('M, Y',$data['monthfrom']) ;
    $this->db->order_by('creation_timestamp', 'desc');
    $page_data['invoices'] = $this->db->get('invoice')->result_array();
    $this->load->view('backend/index', $page_data);
}
function balancesheetmonthly($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = strtotime($this->input->post('datefrom'));
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'balancesheetmonthly';
    $page_data['page_title'] = 'Balance Sheet for    '  . date('M, Y',$data['monthfrom']);
    $this->db->order_by('creation_timestamp', 'desc');
    $page_data['invoices'] = $this->db->get('invoice')->result_array();
    $this->load->view('backend/index', $page_data);
}
/*
 * Daily Balance Sheet 
 *  */
function balancesheetdaily($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    $data['monthfrom'] = strtotime($this->input->post('datefrom'));
    $page_data['monthfrom'] =  $data['monthfrom'];
    $page_data['page_name'] = 'balancesheetdaily';
    $page_data['page_title'] = 'Balance Sheet for    '  . date('d M, Y',$data['monthfrom']);
    $this->db->order_by('creation_timestamp', 'desc');
    $page_data['invoices'] = $this->db->get('invoice')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********INCOME CATEGORY***************/
function income_category($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $this->db->insert('income_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/income_category');
    }
    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $this->db->where('income_category_id', $param2);
        $this->db->update('income_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/income_category');
    }
    if ($param1 == 'delete') {
        $this->db->where('income_category_id', $param2);
        $this->db->delete('income_category');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/income_category');
    }

    $page_data['page_name'] = 'income_category';
    $page_data['page_title'] = get_phrase('income_category');
    $this->load->view('backend/index', $page_data);
}
///////////////////HOMEPAGETEXT NFORMATION/////////////////
function homepagetext($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {
        $data['homepage_details'] = $this->input->post('homepage_details');

        $this->db->insert('homepage_text', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/homepagetext/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['homepage_details'] = $this->input->post('homepage_details');
        $this->db->where('homepage_id', $param2);
        $this->db->update('homepage_text', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/homepagetext/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('homepage_text', array(
            'homepage_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('homepage_id', $param2);
        $this->db->delete('homepage_text');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/homepagetext/', 'refresh');
    }
    $page_data['page_name'] = 'homepagetext';
    $page_data['page_title'] = get_phrase('home_page_texts');
    $page_data['homepage'] = $this->db->get('homepage_text')->result_array();
    $this->load->view('backend/index', $page_data);
} 
/***********input at a mission data**************/

function mission_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['mission'] = $this->input->post('mission');

        $this->db->insert('mission', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/mission_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['mission'] = $this->input->post('mission');

        $this->db->where('mission_id', $param2);
        $this->db->update('mission', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/mission_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('mission', array(
            'mission_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('mission_id', $param2);
        $this->db->delete('mission');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/mission_information/', 'refresh');
    }
    $page_data['page_name'] = 'mission';
    $page_data['page_title'] = get_phrase('mission');
    $page_data['missions'] = $this->db->get('mission')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********input at a Exam Method data**************/

function exammethod_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['exammethod'] = $this->input->post('exammethod');

        $this->db->insert('exammethod', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/exammethod_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['exammethod'] = $this->input->post('exammethod');

        $this->db->where('exammethod_id', $param2);
        $this->db->update('exammethod', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/exammethod_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('exammethod', array(
            'exammethod_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('exammethod_id', $param2);
        $this->db->delete('exammethod');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/exammethod_information/', 'refresh');
    }
    $page_data['page_name'] = 'exammethod';
    $page_data['page_title'] = get_phrase('exammethod');
    $page_data['exammethods'] = $this->db->get('exammethod')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********input at a Special Aspects data**************/

function specialaspect_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['specialaspect'] = $this->input->post('specialaspect');

        $this->db->insert('specialaspect', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/specialaspect_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['specialaspect'] = $this->input->post('specialaspect');

        $this->db->where('specialaspect_id', $param2);
        $this->db->update('specialaspect', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/specialaspect_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('specialaspect', array(
            'specialaspect_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('specialaspect_id', $param2);
        $this->db->delete('specialaspect');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/specialaspect_information/', 'refresh');
    }
    $page_data['page_name'] = 'specialaspect';
    $page_data['page_title'] = get_phrase('specialaspect');
    $page_data['specialaspects'] = $this->db->get('specialaspect')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********input at a admission process data**************/

function admissionprocess_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['admissionprocess'] = $this->input->post('admissionprocess');

        $this->db->insert('admissionprocess', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/admissionprocess_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['admissionprocess'] = $this->input->post('admissionprocess');

        $this->db->where('admissionprocess_id', $param2);
        $this->db->update('admissionprocess', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/admissionprocess_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('admissionprocess', array(
            'admissionprocess_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('admissionprocess_id', $param2);
        $this->db->delete('admissionprocess');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/admissionprocess_information/', 'refresh');
    }
    $page_data['page_name'] = 'admissionprocess';
    $page_data['page_title'] = get_phrase('admissionprocess');
    $page_data['admissionprocesss'] = $this->db->get('admissionprocess')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********input at a fees data**************/

function fees_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['fees'] = $this->input->post('fees');

        $this->db->insert('fees', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/fees_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['fees'] = $this->input->post('fees');

        $this->db->where('fees_id', $param2);
        $this->db->update('fees', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/fees_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('fees', array(
            'fees_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('fees_id', $param2);
        $this->db->delete('fees');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/fees_information/', 'refresh');
    }
    $page_data['page_name'] = 'fees';
    $page_data['page_title'] = get_phrase('fees');
    $page_data['feess'] = $this->db->get('fees')->result_array();
    $this->load->view('backend/index', $page_data);
}
/***********input at a school time data**************/

function schooltime_information($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {

        $data['schooltime'] = $this->input->post('schooltime');

        $this->db->insert('schooltime', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/schooltime_information/', 'refresh');
    }
    if ($param1 == 'do_update') {

        $data['schooltime'] = $this->input->post('schooltime');

        $this->db->where('schooltime_id', $param2);
        $this->db->update('schooltime', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/schooltime_information/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('schooltime', array(
            'schooltime_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('schooltime_id', $param2);
        $this->db->delete('schooltime');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/schooltime_information/', 'refresh');
    }
    $page_data['page_name'] = 'schooltime';
    $page_data['page_title'] = get_phrase('schooltime');
    $page_data['schooltimes'] = $this->db->get('schooltime')->result_array();
    $this->load->view('backend/index', $page_data);
}
///////////////CONTACT////////////////////
function contact($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['contact_name'] = $this->input->post('contact_name');
        $data['contact_address'] = $this->input->post('contact_address');
        $data['contact_phone'] = $this->input->post('contact_phone');
        $data['contact_email'] = $this->input->post('contact_email');
        $this->db->insert('contact', $data);
        $contact_id = $this->db->insert_id();
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/contact/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['contact_name'] = $this->input->post('contact_name');
        $data['contact_address'] = $this->input->post('contact_address');
        $data['contact_phone'] = $this->input->post('contact_phone');
        $data['contact_email'] = $this->input->post('contact_email');

        $this->db->where('contact_id', $param2);
        $this->db->update('contact', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/contact/', 'refresh');
    }
    else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('contact', array(
            'contact_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('contact_id', $param2);
        $this->db->delete('contact');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/contact/', 'refresh');
    }
    $page_data['contact'] = $this->db->get('contact')->result_array();
    $page_data['page_name'] = 'contact';
    $page_data['page_title'] = get_phrase('contact_details');
    $this->load->view('backend/index', $page_data);
}
/////////////////////////////////////////////////////////////////////////////////////////////
function photogallery($param1 = '',  $param2 ='', $param3 ='' ) {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if($param1=='create'){
       $data['album_name'] = $this->input->post('album_name');
       $data['album_description'] = $this->input->post('album_description');                    
       $this->db->insert('album', $data);
       $album_id=  $this->db->insert_id();
       move_uploaded_file($_FILES['userfile']['tmp_name'],'uploads/album_admin_image/'.$album_id.'-'.$data['album_name'].'.jpg');
       redirect(base_url() . 'index.php?admin/photogallery/', 'refresh');
   }
   if($param1=='delete'){
       $this->db->where('album_id', $param2);
       $this->db->delete('image');
       $this->db->where('album_id', $param2);
       $this->db->delete('album');
       $this->session->set_flashdata('flash_message', get_phrase('album_deleted'));
       redirect(base_url() . 'index.php?admin/photogallery/', 'refresh');


   }
   if ($param1 == 'do_update') {
    $data['album_name'] = $this->input->post('album_name');
    $data['album_description'] = $this->input->post('album_description');
    $this->db->where('album_id', $param2);
    $this->db->update('album', $data);
    $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
    redirect(base_url() . 'index.php?admin/photogallery/', 'refresh');
} else if ($param1 == 'edit') {
    $page_data['edit_data'] = $this->db->get_where('album', array(
        'album_id' => $param2
    ))->result_array();
}
$page_data['albums']=$this->db->get('album')->result_array();
$page_data['page_name'] = 'photogallery';
$page_data['page_title'] = get_phrase('Gallery');
$this->load->view('backend/index', $page_data);
}
/////////////////////////////////////////////////////////////////////////////////////////////
function image($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'upload') {

        if(!empty($_FILES)){
            $data['album_id'] =  $param2;
            $data['image_name']=$_FILES['file']['name'];                                
            $this->db->insert('image',$data);
            $image_id=  $this->db->insert_id();
               // $name1 = $_FILES["file"]["name"];
               // $ext1 = substr($name1, strrpos($name1, '.'));                
            move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/album_admin_image/'.$param2.'-'.$image_id.'.jpg');                
        }
        redirect(base_url() . 'index.php?admin/image/', 'refresh');
    }
    if ($param1 == 'do_update') {
     $data['image_name'] = $this->input->post('image_name');
     $data['image_description'] = $this->input->post('image_description');
     $this->db->where('image_id', $param3);
     $this->db->update('image', $data);
     $this->session->set_flashdata('flash_message', get_phrase('image_edited'));
     redirect(base_url() . 'index.php?admin/image/view/'.$param2);
 } else if ($param1 == 'edit') {
     $page_data['edit_data'] = $this->db->get_where('image', array(
         'image_id' => $param3
     ))->result_array();
 }
 if ($param1 == 'delete') {
    $this->db->where('image_id', $param3);
    $this->db->delete('image');

    $this->session->set_flashdata('flash_message', get_phrase('image_deleted'));
    redirect(base_url() . 'index.php?admin/image/view/'.$param2);

}
if ($param1 == 'view') {
 $page_data['images'] =  $this->db->get_where('image', array(
    'album_id' => $param2
))->result_array();
 $page_data['current_album_id']= $param2;

}


$page_data['addimage'] =  $this->db->get_where('image', array(
    'album_id' => $param2
))->result_array();

$page_data['page_name'] = 'image';
$page_data['page_title'] = get_phrase('pictures');
$this->load->view('backend/index', $page_data,'refresh');
}

///////////////////////////////////////////////////////////////////////////////////////////////   
function sliderimage($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'upload') {

        if(!empty($_FILES)){

            $data['slider_image_name']=$_FILES['file']['name'];                                
            $this->db->insert('slider_image',$data);
            $image_id=  $this->db->insert_id();
               // $name1 = $_FILES["file"]["name"];
               // $ext1 = substr($name1, strrpos($name1, '.'));                
            move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/slider_admin_image/'.$image_id.'.jpg');                
        }
        redirect(base_url() . 'index.php?admin/sliderimage/', 'refresh');
    }

    if ($param1 == 'delete') {
        $this->db->where('slider_image_id', $param3);
        $image_id=  $param3;
        $this->db->delete('slider_image');
        unlink(base_url("uploads/slider_admin_image/".$image_id.'.jpg'));

        $this->session->set_flashdata('flash_message', get_phrase('image_deleted'));
        redirect(base_url() . 'index.php?admin/sliderimage/view/');

    }
    if ($param1 == 'view') {
     $page_data['images'] =  $this->db->get('slider_image')->result_array();  
 }


 $page_data['images'] =  $this->db->get('slider_image')->result_array();        
 $page_data['page_name'] = 'sliderimage';
 $page_data['page_title'] = get_phrase('slider_image');
 $this->load->view('backend/index', $page_data,'refresh');
}
/*********MANAGE FormL************/
function form($task = "", $form_id = "")
{
    if ($this->session->userdata('admin_login') != 1)
    {
        $this->session->set_userdata('last_page' , current_url());
        redirect(base_url(), 'refresh');
    }

    if ($task == "create")
    {
        $this->crud_model->save_form_info();
        $this->session->set_flashdata('flash_message' , get_phrase('form_info_saved_successfuly'));
        redirect(base_url() . 'index.php?admin/form' , 'refresh');
    }

    if ($task == "update")
    {
        $this->crud_model->update_form_info($form_id);
        $this->session->set_flashdata('flash_message' , get_phrase('form_info_updated_successfuly'));
        redirect(base_url() . 'index.php?admin/form' , 'refresh');
    }

    if ($task == "delete")
    {
        $this->crud_model->delete_form_info($form_id);
        redirect(base_url() . 'index.php?admin/form');
    }

    $data['form_info']    = $this->crud_model->select_form_info();
    $data['page_name']              = 'form';
    $data['page_title']             = get_phrase('Admission_form');
    $this->load->view('backend/index', $data);
}
function clear_id_cards_cache() {
    if ($this->session->userdata('admin_login') != 1)
    {
        $this->session->set_userdata('last_page' , current_url());
        redirect(base_url(), 'refresh');
    }

    $clear = $this->input->post('clear_cache');
    if ($clear == 'yes') {
        $files = glob('uploads/student_id_cards/*');
        foreach($files as $file) {
            if(is_file($file))
                unlink($file);
        }
        echo base_url() . 'index.php?admin/id_card';
    }
}
}